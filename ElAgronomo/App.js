import { Provider } from "react-redux";
import { store } from "./Redux/store/index.js";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { LogBox } from 'react-native';
// import 'react-native-gesture-handler';

import Login from "./Components/Home/Login";
import SingUp from "./Components/Home/SingUp";
import ForgotPass from "./Components/Home/ForgotPass";
import ForgotPass2 from "./Components/Home/ForgotPass2";
import { HomeScreenStack } from "./Components/Home/HomeStack.js";
import Empresa from "./Components/Configuration/Empresa.js"
import AddEmpresa from "./Components/Configuration/AddEmpresa.js"
import AddEmpresa2 from "./Components/Configuration/AddEmpresa2.js"
import Establecimientos from "./Components/Establecimientos/Establecimientos.js"
import AddEstablecimiento from "./Components/Establecimientos/AddEstablecimiento.js"
import AddEstablecimiento2 from "./Components/Establecimientos/AddEstablecimiento2.js"
import Almacenes from "./Components/Almacenes/Almacenes.js"
import AddAlmacen from "./Components/Almacenes/AddAlmacen.js"
import AddAlmacen2 from "./Components/Almacenes/AddAlmacen2.js"
import Insumos from "./Components/Insumos/Insumos.js"
import AddInsumo from "./Components/Insumos/AddInsumo.js"
import AddInsumo2 from "./Components/Insumos/AddInsumo2.js"
import Lotes from "./Components/Lotes/Lotes.js"
import AddLote from "./Components/Lotes/AddLote.js"
import AddLote2 from "./Components/Lotes/AddLote2.js"
import EstDetail from "./Components/Establecimientos/EstDetail.js"
import AlmDetail from "./Components/Almacenes/AlmDetail.js"
import LotesxEst from "./Components/Lotes/LotesxEst.js"
import LoteDetail from "./Components/Lotes/LoteDetail.js"
import LoteMap from "./Components/Lotes/LoteMap.js"
import InsxFam from "./Components/Insumos/InsxFam.js"
import InsDetail from "./Components/Insumos/InsDetail.js"
import Compras from "./Components/Home/Menu lateral/AdmStock/Compras.js"
import DetCom from "./Components/Home/Menu lateral/AdmStock/DetCom.js"
import EncabCom from "./Components/Home/Menu lateral/AdmStock/EncabCom.js"
import Ajustes from "./Components/Home/Menu lateral/AdmStock/Ajustes/Ajustes.js"
import EncabAju from "./Components/Home/Menu lateral/AdmStock/Ajustes/EncabAju.js"
import DetAju from "./Components/Home/Menu lateral/AdmStock/Ajustes/DetAju.js"
import Traslados from "./Components/Home/Menu lateral/AdmStock/Traslados/Traslados.js"
import DetTras from "./Components/Home/Menu lateral/AdmStock/Traslados/DetTras.js"
import EncabTras from "./Components/Home/Menu lateral/AdmStock/Traslados/EncabTras.js"
import EstMap from "./Components/Establecimientos/EstMap.js"
import AlmMap from "./Components/Almacenes/AlmMap.js"
import MarkerMap from "./Components/Establecimientos/MarkerMap.js"
import MovStock from "./Components/Home/Menu lateral/AdmStock/Stock/MovStock.js"
import MovIns from "./Components/Home/Menu lateral/AdmStock/Stock/MovIns.js";
import Existencias from "./Components/Home/Menu lateral/AdmStock/Stock/Existencias.js";
import PrintPdf from "./Components/Home/Menu lateral/AdmStock/Stock/PrintPdf.js"
import { createTablesIfNotExists } from "./db/controller.js";
import { useEffect } from "react";
import InitialSync from "./Components/sync/InitialSync.js";

LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you\'re using an old API with gesture components, check out new Gestures system!",
]);

const Stack = createStackNavigator();

//SQLite
const createTables = createTablesIfNotExists()

const App = () => {

  /* const prueba1 = async () => {

    const prueba = await global.SQLF.customSelectSQLite(`SELECT * from Prueba`, "Prueba")
    console.log(prueba)
  }

  const pruebaInsert = async () => {
    let insumo = [
      {
        name: "santiago",
        edad: 24
      }
    ]
    const prueba = await global.SQLF.InsertSQLite("Prueba", insumo)
    prueba1()
  }

  useEffect(() => {
    pruebaInsert()
  },[]) */

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          {/* <Stack.Screen
              name="Home"
              component={Home}
              options={{ headerShown: false }}
            /> */}
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              headerShown: false,
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="SingUp"
            component={SingUp}
            options={{
              headerShown: false,
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="HomeStack"
            component={HomeScreenStack}
            options={{
              headerShown: false,
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="ForgotPass"
            component={ForgotPass}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="ForgotPass2"
            component={ForgotPass2}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="Empresa"
            component={Empresa}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AddEmpresa"
            component={AddEmpresa}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AddEmpresa2"
            component={AddEmpresa2}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="Establecimientos"
            component={Establecimientos}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AddEstablecimiento"
            component={AddEstablecimiento}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AddEstablecimiento2"
            component={AddEstablecimiento2}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="Almacenes"
            component={Almacenes}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AddAlmacen"
            component={AddAlmacen}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AddAlmacen2"
            component={AddAlmacen2}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="Insumos"
            component={Insumos}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AddInsumo"
            component={AddInsumo}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AddInsumo2"
            component={AddInsumo2}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="Lotes"
            component={Lotes}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AddLote"
            component={AddLote}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AddLote2"
            component={AddLote2}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="EstDetail"
            component={EstDetail}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AlmDetail"
            component={AlmDetail}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="LotesxEst"
            component={LotesxEst}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="LoteDetail"
            component={LoteDetail}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="LoteMap"
            component={LoteMap}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="InsxFam"
            component={InsxFam}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="InsDetail"
            component={InsDetail}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="Compras"
            component={Compras}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="EncabCom"
            component={EncabCom}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="DetCom"
            component={DetCom}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="Ajustes"
            component={Ajustes}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="EncabAju"
            component={EncabAju}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="DetAju"
            component={DetAju}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="Traslados"
            component={Traslados}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="EncabTras"
            component={EncabTras}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="DetTras"
            component={DetTras}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="EstMap"
            component={EstMap}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="AlmMap"
            component={AlmMap}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="MarkerMap"
            component={MarkerMap}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="MovStock"
            component={MovStock}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="MovIns"
            component={MovIns}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="Existencias"
            component={Existencias}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="PrintPdf"
            component={PrintPdf}
            options={{
              gestureEnabled: false
            }}
          />
          <Stack.Screen
            name="InitialSync"
            component={InitialSync}
            options={{
              headerShown: false,
              gestureEnabled: false
            }}
          />


        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};
export default App;
