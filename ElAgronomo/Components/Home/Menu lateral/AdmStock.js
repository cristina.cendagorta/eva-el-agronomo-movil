import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect, useRef } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {MaterialCommunityIcons} from "@expo/vector-icons"
import { useDispatch, useSelector } from "react-redux";
import { getEstablecimiento } from "../../../Redux/actions";
import { getAlmacen } from "../../../Redux/actions";
import { useIsFocused } from "@react-navigation/native";
import { getExist } from "../../../Redux/actions";
import { getMov } from "../../../Redux/actions";
import { getDetalle } from "../../../Redux/actions";
const AdmStock = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  // ME TRAIGO LA EMPRESA
  const empresa1 = useSelector((store) => store.empresa);
  const isFocused = useIsFocused();
  
  useEffect(() => {
    if(isFocused){ 
      // AL INICIAR EL COMPONENTE YA ME TRAIGO, ALMACENES, ESTABLECIMIENTOS, EXISTENCIAS, MOVIMIENTOS Y DETALLES
      dispatch(getEstablecimiento(empresa1[0].id));
      dispatch(getAlmacen());
      dispatch(getExist())
      dispatch(getMov());
      dispatch(getDetalle());

  } 
  }, [,isFocused]);

  // ESTOS LOS SON LOS MENUES DISPONIBLES DE LA ADM DE STOCK.
    const foods = [
        {
          id: '1',
          name: 'Compras',
          ingredients: 'Adquisición de Insumos',
          icon: 'cart-plus',
          screen:'Compras',
          
        },
        {
          id: '2',
          name: 'Orden de Trabajo',
          ingredients: 'Entregas / Recepciones',
          icon: 'account-hard-hat'
        },
        {
          id: '3',
          name: 'Traslado Interno',
          ingredients: 'Movimiento entre                                                      almacenes/establecimientos',
          icon: 'dolly',
          screen:'Traslados',
        },
        {
            id: '4',
            name: 'Ajuste de Inventario',
            ingredients: 'Recuento de Stock',
            icon: 'cog',
            screen:'Ajustes',
          },
                  {
            id: '5',
            name: 'Existencias de Stock',
            ingredients: 'Visualizar el saldo de stock',
            icon: 'scale-balance',
            screen:'Existencias'
          },
          {
            id: '6',
            name: 'Movimientos de Insumos',
            ingredients: 'Reporte de ingresos/egresos',
            icon: 'sprinkler-variant',
            screen:'MovIns'
          },
          {
            id: '7',
            name: 'Movimientos',
            ingredients: 'Reporte de ingresos/egresos',
            icon: 'sync',
            screen:'MovStock'
          },

      ];

      
      const CartCard = ({item}) => {
        return (
          <TouchableOpacity style={style.cartCard}    onPress={() => 
            navigation.navigate(item.screen) // LO LLEVO HACIA UN COMPONENTE AL TOCAR EL BOTON CORRESPONDIENTE
        }>
            {/* <Image source={item.image} style={{height: 80, width: 80}} /> */}
            
            <View style={style.actionBtn}>
                <MaterialCommunityIcons name={item.icon} size={28} color={"green"} style={{marginTop:5}} /> 
                {/* <Icon name="add" size={25} color={"white"} /> */}
              </View>
            <View
              style={{
                height: 100,
                marginLeft:wp("-2%"),
                // paddingVertical: 20,
                alignItems:"center",
                justifyContent:"center",
                flex: 1,
              }}>
     
              <Text style={{fontWeight: 'bold', fontSize: 19}}>{item.name}</Text>
              <Text style={{fontSize: 13, color: "grey", textAlign:"center"}}>
                {item.ingredients}
              </Text>
            </View>
            <View style={{marginRight: 20, alignItems: 'center'}}>
              {/* <Text style={{fontWeight: 'bold', fontSize: 18}}>3</Text> */}
              <View style={style.actionBtn2}>
                <MaterialCommunityIcons name="chevron-right" size={25} color={"grey"}  />
                {/* <Icon name="add" size={25} color={"white"} /> */}
              </View>
            </View>
          </TouchableOpacity>
        );
      };
return (
    <SafeAreaView style={{backgroundColor: "white", flex: 1}}>
      <View style={style.header}>
        {/* <Icon name="arrow-back-ios" size={28} onPress={navigation.goBack} /> */}
        <Text style={{fontSize: 20, fontWeight: 'bold', color:"black"}}>Elige el tipo de movimiento</Text>
      </View>
      <FlatList // ACA MUESTRO TODA LA LISTA DE LAS OPCIONES
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 80}}
        data={foods}
        renderItem={({item}) => <CartCard item={item} />}
        ListFooterComponentStyle={{paddingHorizontal: 20, marginTop: 20}}
        ListFooterComponent={() => (
          <View>
            <View style={{marginHorizontal: 30}}>
              {/* <PrimaryButton title="CHECKOUT" /> */}
            </View>
          </View>
        )}
      />
    </SafeAreaView>
);

}

export default AdmStock;

const style = StyleSheet.create({
    header: {
      paddingVertical: 5,
      flexDirection: 'row',
      alignSelf: 'center',
      marginHorizontal: 20,
    },
    cartCard: {
      height: 100,
      elevation: 15,
      borderRadius: 10,
      backgroundColor: "white",
      marginVertical: 10,
      marginHorizontal: 20,
      paddingHorizontal: 10,
      flexDirection: 'row',
      alignItems: 'center',
    },
    actionBtn: {
      width: wp("10%"),
      height: wp("10%"),
    borderColor:"#f2f2f2",
      borderRadius: 30,
      borderWidth:hp("0.2%"),
    marginLeft:wp("3%"),
      flexDirection: 'row',
      justifyContent: 'center',
      alignContent: 'center',
    },
    actionBtn2: {
      marginRight:wp("-3%"),

      },
  });