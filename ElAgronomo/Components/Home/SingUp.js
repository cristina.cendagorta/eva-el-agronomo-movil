import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect, useRef } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  SafeAreaView,
  Modal,
  Button,
} from "react-native";
import {MaterialCommunityIcons} from "@expo/vector-icons"
import Ionicons from 'react-native-vector-icons/Ionicons';
import CheckBox from "expo-checkbox";
import { useDispatch, useSelector } from "react-redux";
import { registro } from "../../Redux/actions/index";

// ESTO ES PARA EL REGISTRO

const SignUp = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  // ESTA REF, ES PARA QUE AL APRETAR ENTER EN EL TECLADO, VAYA HACIA EL INPUT DE ABAJO
  const ref_input2 = useRef();
  const ref_input3 = useRef();
  const ref_input4 = useRef();

  // ESTADOS INICIALES

  const [reg, setReg] = useState({
    usuario: "",    
    mail: "",
    contraseña: "",
    contraseña2: "",
  });

  const [show, setShow] = useState(false);
  const [visible,setVisible] = useState(true);
  const [show2, setShow2] = useState(false);
  const [visible2,setVisible2] = useState(true);


  const [check, setCheck] = useState(false);

  const CheckboxChange = (e) => {
    setCheck(!check);
  };


  const handleSubmit = (e) => {
    e.preventDefault();
    reg.contraseña != reg.contraseña2 ? alert("la contraseña no coincide") : null
    const obj = {
      usuario: reg.usuario,
      mail: reg.mail,
      contraseña: reg.contraseña,

    };

dispatch(registro(obj)); // SE EJECUTA LA ACCION PARA HACER EL SINGUP
 console.log("Estoy enviado", obj);
    setReg({
        usuario: "",    
        mail: "",
        contraseña: "",
        contraseña2: "",
    });
navigation.navigate("Login") // LO LLEVAMOS HACIA EL LOGIN
    console.log(obj);
};

  //funciones para cambiar e.value de los inputs

  const handelChangeUser = (name) => {
    setReg({
      ...reg,
      usuario: name,
    });
  };

  const handelChangeMail = (name) => {
    setReg({
      ...reg,
      mail: name,
    });
  };
  const handelChangePass = (name) => {
    setReg({
      ...reg,
      contraseña: name,
    });
  };
  const handelChangePass2 = (name) => {
    setReg({
      ...reg,
      contraseña2: name,
    });
  };





  return ( // SE MUESTRA UN UNICO RENDER
    
      <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }}  behavior={Platform.OS === "ios" ? "padding" : "height"}>
    <View style={{ flex: 1,  backgroundColor: 'white'  }}>
      <View style={{marginTop:hp("-2%"),marginLeft:wp("0%"),marginBottom:hp("0%")}}>
        {/* <HeaderBar  screen={'null'} style={{color:"white"}}/> */}
        </View>
    <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffffff" }}
      showsVerticalScrollIndicator={false}
    >
      
      {/* Brand View */}
      <ImageBackground
        source={require("./logo.png")}
        resizeMode= "contain"
        style={{
            display:'flex',
            marginTop:  hp('-9%'),
            marginBottom: hp('-7%'),
            height: hp('60%') ,
            width: wp('50%') ,
          alignSelf: "center",
        }}
      >
      </ImageBackground>
      {/* Botton View */}
      <View style={styles.bottonView}>
        {/* Welcome View */}
        <View style={{ padding: 40, display: "flex", alignItems: "center" }}>
          <Text style={{ color: "#151f27", fontSize: 34,fontWeight: '600', marginTop: hp("-6%") }}>
            Ingresa a EA+
          </Text>
        </View>
        {/* inputs */}
        <View style={styles.FormView}>
          <TextInput
            name="nombre"
            value={reg.nombre}
            onChangeText={(name) => handelChangeUser(name)}
            placeholder="Usuario*"
            style={styles.TextInput}
            blurOnSubmit={false}
            // autoFocus={true}
            returnKeyType="next"
            onSubmitEditing={() => ref_input2.current.focus()}

          ></TextInput>

          <TextInput
            icon="mail"
            value={reg.mail}
            onChangeText={(name) => handelChangeMail(name)}
            name="mail"
            placeholder="Dirección de Mail*"
            style={styles.TextInput}
            returnKeyType="next"
            onSubmitEditing={() => ref_input3.current.focus()}
             ref={ref_input2}           
        
          ></TextInput>

          <TextInput
            value={reg.contraseña}
            onChangeText={(name) => handelChangePass(name)}
            name="contraseña"
            placeholder="Contraseña*"
            secureTextEntry={visible}
            style={styles.TextInput}
            ref={ref_input3}
            returnKeyType="next"
            onSubmitEditing={() => ref_input4.current.focus()}
            
          ></TextInput>
          <TouchableOpacity style={styles.btnEye} onPress={
            ()=>{
              setVisible(!visible)
              setShow(!show)
            }
          }>
            <Ionicons
           name={show === false ? 'eye-off-outline' : 'eye-outline'}
           size={hp("3.7%")}
           color={show === false ?"#344c40" : "#007d3c"}
            />
            </TouchableOpacity>

          <TextInput
            value={reg.contraseña2}
            onChangeText={(name) => handelChangePass2(name)}
            name="contraseña2"
            placeholder="Repetir Contraseña*"
            secureTextEntry={visible2}
            style={styles.TextInput}
            ref={ref_input4}
            
          ></TextInput>
             <TouchableOpacity style={styles.btnEye2} onPress={
            ()=>{
              setVisible2(!visible2)
              setShow2(!show2)
            }
          }>
            <Ionicons
            name={show2 === false ? 'eye-off-outline' : 'eye-outline'}
            size={hp("3.7%")}
            color={show2 === false ?"#344c40" : "#007d3c"}
            />
            </TouchableOpacity>
          <TouchableOpacity style={styles.Button} onPress={handleSubmit } >
            <Text style={styles.ButtonText} >
              Registrarme!
            </Text>
          </TouchableOpacity>         
         
        </View>        
      </View>
    </ScrollView>
    </View>
    </KeyboardAvoidingView>
    
  );
};

export default SignUp;

const styles = StyleSheet.create({
  brandView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  brandViewText: {
    color: "#FFC107",
    fontSize: 45,
    fontWeight: "bold",
    textTransform: "uppercase",
    // justifyContent:'flex-start'
  },
  bottonView: {
    flex: 1.5,
    backgroundColor: "white",
    bottom: 50,
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
  },
  FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: hp("-7%"),
  },
  TextInput: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.8%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: 20,
    color: "black",
    fontSize:hp("2.3%"),
    backgroundColor:"#eceef2"
  },
  Button: {
    width: "90%",
    color: "black",
    height: hp("8%"),
    backgroundColor: "#007d3c",
    borderRadius: 10,
    marginTop: hp("5%"),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  ButtonText: {
    fontWeight: "bold",
    fontSize: hp("3%"),
    color: "white",
  },
  SingUpText: {
    color: "#4632a1",
    fontSize: 20,
  },
  TextButton: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  preg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  pregunta: {
    color: "red",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    padding: 50,
    marginTop: 20,
    borderRadius: 20,
  },
  text: {
    // marginVertical: 20,
    fontSize: 22,
    color: "white",
    fontWeight: "bold",
  },
  TouchableOpacity: {
    backgroundColor: "black",
    alignSelf: "stretch",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    
  },
  checkbox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  checkboxx: {
    marginTop: 15,
  },
  imgPerfil: {
    width: wp("100%"),
    height: hp('30%'),
    // borderRadius: 100,
    borderColor: "#ff1c49",
    borderWidth: wp('0.6%'),
    marginTop: hp('6%'),
    backgroundColor:"#e1e1e1"
  },
  imgAdd: {
    width: wp("20%"),
    height: wp("20%"),
    marginLeft: wp("75%"),
    marginTop: hp('-11%'),
    borderWidth: 4,
    borderColor: "#D5D5D5",
    borderRadius: 50,
  },
  btnEye: {
    position:"absolute",
    alignSelf:"flex-end",
    marginTop:hp("28%"),
    paddingRight:wp("8%")
  },
  btnEye2: {
    position:"absolute",
    alignSelf:"flex-end",
    marginTop:hp("39%"),
    paddingRight:wp("8%")
  }
});