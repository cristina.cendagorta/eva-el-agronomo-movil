import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView,
    Modal,
    Button,
    ActivityIndicator,
    FlatList
  } from "react-native";
  import { useNavigation } from "@react-navigation/core";
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import empresa from "../../Components/Configuration/empresa.png"
import { getInsumo } from "../../Redux/actions";
 import { useIsFocused } from "@react-navigation/native";
 import { useDispatch, useSelector } from "react-redux";
 import React, { useState, useEffect } from "react";
import { getFamilia } from "../../Redux/actions";
import { ListItem, Icon } from '@rneui/themed'

const Insumos = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  // me traigo insumos y familias
  const insumos = useSelector((store) => store.insumo);
  const familias = useSelector((store) => store.familias);
  const isFocused = useIsFocused();
  useEffect(() => {
    if(isFocused){ 
      setTimeout(function(){
        setLoading(true)
          }, 1500);
          // me traigo insumos y familias
    dispatch(getInsumo());
    dispatch(getFamilia());

  } 
  }, [,isFocused]);
  const [isLoading, setLoading] = useState(false);


  {if(isLoading === false || insumos === null){     // cargando
    return(
  <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
  <ActivityIndicator size="large" color="#007d3c" />
</View>
)
}
}

{
  if(isLoading === true && insumos!= null && insumos.length === 0  ){ // si no hay insumos
        return(
          <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
      
      <Image
      source={empresa}
      resizeMode= "contain"
      style={{
          display:'flex',
          marginTop:  hp('-8%'),
        height: hp('70%') ,
        width: wp('80%') ,
        alignSelf: "center",
      }}
    >
    </Image>
    
      <View style={{alignItems:"center"}}>
      <Text style={{
        color:"#182E44",
        fontSize:hp("2.6%"),
        fontWeight:"bold",
        marginTop:hp("-15%")}}>
        Agrega toda la información de tus insumos
      </Text>
    </View>
    <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
          <TouchableOpacity onPress={() => navigation.navigate("AddInsumo")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
            <View style={{ alignItems: 'center'}}>
              <Text
              
                style={{  
                  fontSize: hp("2.8%"),
                  color:"white",
                  marginTop:hp("2%")                       
                  
                }}>
                + Agregar
              </Text>
            </View>
          </TouchableOpacity>
          </View>
    </SafeAreaView>
  )
}
}

if(isLoading === true && insumos.length > 0 ){ // si hay insumos cargados

  return(
    <SafeAreaView style={{marginTop:3}}>
    <FlatList
           data={familias} // muestro las familias
           keyExtractor={ (item) => item.id }
           renderItem={({item, index}) =>{
             return (
               <TouchableOpacity
                 onPress={() => 
                   navigation.navigate('InsxFam', item) // me voy al detalle por familia
               }>
               <ListItem>
               <Icon name='business' />
               
         <ListItem.Content>
           <ListItem.Title>{item.detalle_familia}</ListItem.Title>
           {/* <ListItem.Subtitle>{item.abreviatura}</ListItem.Subtitle> */}
         </ListItem.Content>
         <ListItem.Chevron color="black" />
       </ListItem>
             </TouchableOpacity>
             )
           }}
           // <ListItemEst item ={item} />}
           ItemSeparatorComponent= { () =>       <View style={{ marginBottom: 10, borderColor:'#00000020', borderWidth:0.5}}></View>}
           ListHeaderComponent = { () => <Text style={{fontWeight:"bold", fontSize:18,marginBottom:20, textAlign:"center"}}>¿A qué familia pertenece el Insumo?</Text>}
           // <ListItem item = {item} />}
               />
                 <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
        <TouchableOpacity onPress={() => navigation.navigate("AddInsumo")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
          <View style={{ alignItems: 'center'}}>
            <Text
            
              style={{  
                fontSize: hp("2.8%"),
                color:"white",
                marginTop:hp("2%")                       
                
              }}>
              + Agregar
            </Text>
          </View>
        </TouchableOpacity>
        </View>
   </SafeAreaView>


)


}


};

export default Insumos;