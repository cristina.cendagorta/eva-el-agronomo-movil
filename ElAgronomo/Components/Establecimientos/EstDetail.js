import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView,
    Modal,
    Button,
    ActivityIndicator,
    FlatList,
  } from "react-native";
  import React, { useState, useEffect, useRef,useMemo } from "react";
  import { useNavigation } from "@react-navigation/core";
  import { useDispatch, useSelector } from "react-redux";
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const EstDetail = (item) => {
  const navigation = useNavigation();
console.log("item",item.route.params)
const geo = JSON.parse(item.route.params.geoposicion) // ACA LO QUE VENIA COMO STRING, LO PASO A OBJETO
console.log("geo",geo)
//MUESTRO TODA LA INFORMACIÓN QUE ME TRAIGO POR PARAMS
  return(

    <ScrollView style={{backgroundColor:"white"}}>
            <Text style={{
        marginLeft:wp("3%"),
      color:"grey",
      fontSize:hp("4%"),
      fontWeight:"bold",
      marginBottom:hp("5%")
      }}>
    Detalle del Establecimiento
    </Text>
        
        <View>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Nombre
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.nombre} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Abreviatura
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.abreviatura} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Establecimiento
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.detalle_tipo_establecimiento} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Dirección
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.direccion} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Localidad
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.localidad} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Provincia
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.provincia} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Zona
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.detalle_zona} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Latitud
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {geo != null ?geo[0].latitude : null} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey", 
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%") 
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Longitud
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {geo != null ?geo[0].longitude : null} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Observaciones
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.observaciones} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Contacto
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.contacto} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  {item.route.params.geoposicion != null ?
  <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("2%"), marginBottom:hp("2%")}} >
    <TouchableOpacity onPress={() => navigation.navigate("MarkerMap", item)} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
      <View style={{ alignItems: 'center'}}>
        <Text
        
          style={{  
            fontSize: hp("2.8%"),
            color:"white",
            marginTop:hp("2%")                       
            
          }}>
          Ver en el Mapa
        </Text>
      </View>
    </TouchableOpacity>
    </View>
    : null
  }

{/* <FlatList
 data={data}
 keyExtractor={({ id }, index) => id}
 renderItem={({ item }) => (
//    <ItemDetail item={item}/>   

            <TouchableOpacity
            //   onPress={() => 
            //     navigation.navigate('EstDetail', item)
            // }
            >
            <ListItem>
            <Icon name='business' />
            
      <ListItem.Content>
        <ListItem.Title>{item.nombre}</ListItem.Title>
        <ListItem.Subtitle>{item.abreviatura}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron color="black" />
    </ListItem>
          </TouchableOpacity>

  )}
/> */}
 {/* <FlatList
        data={item}
        keyExtractor={ (item) => item.id }
        renderItem={({item, index}) =>{
          return (
            <TouchableOpacity
              onPress={() => 
                navigation.navigate('EstDetail', item)
            }>
            <ListItem>
            <Icon name='business' />
            
      <ListItem.Content>
        <ListItem.Title>{item.nombre}</ListItem.Title>
        <ListItem.Subtitle>{item.abreviatura}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron color="black" />
    </ListItem>
          </TouchableOpacity>
          )
        }}
        // <ListItemEst item ={item} />}
        ItemSeparatorComponent= { () =>       <View style={{ marginBottom: 10, borderColor:'#00000020', borderWidth:0.5}}></View>}
        ListHeaderComponent = { () => <Text style={{fontWeight:"bold", fontSize:18,marginBottom:10,}}>Establecimientos</Text>}
        // <ListItem item = {item} />}
            /> */}
</ScrollView>
)
}

const styles = StyleSheet.create({
    container: {
      margin: 4,
      padding: 30
    }
  })


export default EstDetail;