import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as ImagePicker from "expo-image-picker";
import { Ionicons } from "@expo/vector-icons";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  Modal,
  Button,
  KeyboardAvoidingView
} from "react-native";
import DatePicker from 'react-native-datepicker'
import { LogBox } from 'react-native';
import CountryPicker from 'react-native-country-picker-modal'
import Select2 from "react-native-select-two"
// import HeaderBar from "../Utils/HeaderBar";
// import SimpleModal1 from "./../Alerts/SingUp/SimpleModalok";
// import SimpleModal2 from "../Alerts/SingUp/SimpleModalok2.js";
// import SimpleModal3 from "../Alerts/SingUp//SimpleModalname.js";
// import SimpleModal4 from "../Alerts/SingUp//SimpleModalLastname.js";
// import SimpleModal5 from "../Alerts/SingUp//SimpleModalmail.js";
// import SimpleModal6 from "../Alerts/SingUp//SimpleModalpass.js";
// import SimpleModal7 from "../Alerts/SingUp//SimpleModalphone.js";
// import SimpleModal8 from "../Alerts/SingUp//SimpleModalterms.js";
// import SimpleModal9 from "../Alerts/SingUp//SimpleModalrole.js";
// import SimpleModal40 from "../Alerts/SingUp//SimpleModalsamemail.js"; 


import CheckBox from "expo-checkbox";

// import CheckBox from "@react-native-community/checkbox";
// import { ModalPicker } from "./ModalPicker";
import { useDispatch, useSelector } from "react-redux";
// import { adminregister } from "../../Redux/actions/index";
const AddEmpresa = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  // ME TRAIGO LOS SELECTS DE IVA
  const iva = useSelector((store) => store.iva)

  useEffect(() => {
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
}, [])

if(iva != null){
  console.log("iva",iva)
}

//MAPEO LA INFORMACION PARA PONERLO EN UN SELECT
var ivas = iva?.map( item => { 
  return { name: item.detalle_cond_iva, id:item.id }; 
});

  // ALERTAS:

  // REGISTRO USUARIO OK
//   const [isModalVisible1, setisModalVisible1] = useState(false);
//   const [chooseData1, setchooseData1] = useState();

//   const changeModalVisible1 = (bool) => {
//     setisModalVisible1(bool);
//   };

//   const setData1 = (data) => {
//     setchooseData1(data);
//   };

//   // REGISTRO TRANSPORTISTA OK
//   const [isModalVisible2, setisModalVisible2] = useState(false);
//   const [chooseData2, setchooseData2] = useState();

//   const changeModalVisible2 = (bool) => {
//     setisModalVisible2(bool);
//   };

//   const setData2 = (data) => {
//     setchooseData2(data);
//   };


//   // NOMBRE MAL INGRESADO
//   const [isModalVisible3, setisModalVisible3] = useState(false);
//   const [chooseData3, setchooseData3] = useState();

//   const changeModalVisible3 = (bool) => {
//     setisModalVisible3(bool);
//   };

//   const setData3 = (data) => {
//     setchooseData3(data);
//   };

//   // APELLIDO MAL INGRESADO

//   const [isModalVisible4, setisModalVisible4] = useState(false);
//   const [chooseData4, setchooseData4] = useState();

//   const changeModalVisible4 = (bool) => {
//     setisModalVisible4(bool);
//   };

//   const setData4 = (data) => {
//     setchooseData4(data);
//   };

//   //MAIL MAL INGRESADO
//   const [isModalVisible5, setisModalVisible5] = useState(false);
//   const [chooseData5, setchooseData5] = useState();

//   const changeModalVisible5 = (bool) => {
//     setisModalVisible5(bool);
//   };

//   const setData5 = (data) => {
//     setchooseData5(data);
//   };

//   // CONTRASEÑA MAL INGRESADA

//   const [isModalVisible6, setisModalVisible6] = useState(false);
//   const [chooseData6, setchooseData6] = useState();

//   const changeModalVisible6 = (bool) => {
//     setisModalVisible6(bool);
//   };

//   const setData6 = (data) => {
//     setchooseData6(data);
//   };
//   // TELEFONO MAL INGRESADO

//   const [isModalVisible7, setisModalVisible7] = useState(false);
//   const [chooseData7, setchooseData7] = useState();

//   const changeModalVisible7 = (bool) => {
//     setisModalVisible7(bool);
//   };

//   const setData7 = (data) => {
//     setchooseData7(data);
//   };
//   // NO ACEPTA TERMINOS
//   const [isModalVisible8, setisModalVisible8] = useState(false);
//   const [chooseData8, setchooseData8] = useState();

//   const changeModalVisible8 = (bool) => {
//     setisModalVisible8(bool);
//   };

//   const setData8 = (data) => {
//     setchooseData8(data);
//   };

//   // NO ELIJE ROL
//   const [isModalVisible9, setisModalVisible9] = useState(false);
//   const [chooseData9, setchooseData9] = useState();

//   const changeModalVisible9 = (bool) => {
//     setisModalVisible9(bool);
//   };

//   const setData9 = (data) => {
//     setchooseData9(data);
//   };

//   // MAIL REPETIDO
//   const [isModalVisible40, setisModalVisible40] = useState(false);
//   const [chooseData40, setchooseData40] = useState();

//   const changeModalVisible40 = (bool) => {
//     setisModalVisible40(bool);
//   };

//   const setData40 = (data) => {
//     setchooseData40(data);
//   };

  


  // useEffect(()=>{
  //   //console.log('aqui esta la respuestaaaa:',respuesta);
  //   if(respuesta?.role === true){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileUser");
  //     changeModalVisible1(true)
  //   }if(respuesta?.role === false){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileCarrier");
  //     changeModalVisible2(true)
  //   }if(respuesta?.role === 1){
  //     // alert('El mail ingresado ya se encuentra en uso!')
  //     changeModalVisible40(true)
  //   }
  // },[respuesta]);


// GENERO ESTADOS LOCALES

  const [reg, setReg] = useState({
    razon: "",
    pais: "",
    cuit: "",
    iva: "",
  });

  const [place, setPlace] = useState({
    iva: "Condición de Iva",
  });

  const [check, setCheck] = useState(false);

  const CheckboxChange = (e) => {
    setCheck(!check);
  };

  // const ChangeInput = (e) => {
  //   setReg({
  //     // y sino es  generos y platforms, directamente pongo lo que escribo en el input
  //     ...reg,
  //     [e.target.name]: e.target.value,
  //   });
  // };

  const handleSubmit = (e) => {
    e.preventDefault();
    // en un objeto pongo lo que tengo en el estado inicial
    // let rolex = undefined;
    // if (chooseData === "◉ Usuario") {
    //   rolex = true;
    // } if(chooseData === "◉ Transportista") {
    //   rolex = false;
    // }
    //LLENO EL OBJETO A ENVIAR AL BACK
    const obj = {
      razon_social: reg.razon,
      direccion_pais: reg.pais,
      cuit: Number(reg.cuit),
      cond_iva_id: Number(reg.iva),
    };

    // validaciones 
//     if (!obj.name ) {
//       changeModalVisible3(true)
//       return
//   }
//   if (!obj.lastName) {
//       // alert("Por favor escribe el Apellido correctamente!")
//       changeModalVisible4(true)
//       return
//   }
//   if (!obj.eMail.includes('.com') || !obj.eMail.includes('@')  ) {
//     changeModalVisible5(true)
//     // alert("Por favor escribe un correo electrónico válido!")
//     return
// } if (!obj.password) {
//   changeModalVisible6(true)
//   // alert("Por favor escribe una Contraseña válida!")
//   return
// }
// if (!obj.phone) {
//   changeModalVisible7(true)
//       // alert("Por favor escribe un número de telefono válido!")
//       return
//   }
//   if (!obj.business) {
//     changeModalVisible8(true)
//     // alert("Debes aceptar los términos para poder registrarte!")
//     return
//   }if (!obj.secret) {
//     changeModalVisible9(true)
//   // alert("Por favor elije un Rol!")
//   return
// }



// dispatch(adminregister(obj));
 console.log("Estoy enviado", obj);

navigation.navigate("AddEmpresa2",obj) //EJECUTO LA ACCION
setReg({
  razon: "",
  pais: "",
  cuit: "",
  iva: "",

});

setPlace({
  iva: "Condición de Iva",
});

};

  //funciones para cambiar e.value de los inputs

  const handelChangeRazon = (name) => {
    setReg({
      ...reg,
      razon: name,
    });
  };
  const handelChangePais = (name) => {
    setReg({
      ...reg,
      pais: name,
    });
  };
  const handelChangeCuit = (name) => {
    setReg({
      ...reg,
      cuit: name,
    });
  };

  const handelChangeIva = (name) => {
    console.log(name)
    setReg({
      ...reg,
      iva: name,
    });

    const filtrado = iva?.filter((f) => f.id === name)
    console.log("filtra",filtrado)
    setPlace({
      ...place,
      iva: filtrado[0].detalle_cond_iva,
    });
  };
  
   ////--> IMAGE PICKER <-- ////
   const [selectedImage, setSelectedImage] = useState(null);

   let openImagePickerAsync = async () => {
     let permissionResult =
       await ImagePicker.requestMediaLibraryPermissionsAsync();
 
     if (permissionResult.granted === false) {
       alert("Se requiere el permiso para acceder a la cámara");
       return;
     }
 
     //Si es true va a venir a pickerResult
     const pickerResult = await ImagePicker.launchImageLibraryAsync({
       mediaTypes: ImagePicker.MediaTypeOptions.Images,
       allowsEditing: true,
       aspect: [4, 3],
       quality: 1,
     });
 
     if (pickerResult.cancelled !== true) {
       let newFile = {
         uri: pickerResult.uri,
         type: `logi/${pickerResult.uri.split(".")[1]}`,
         name: `logi.${pickerResult.uri.split(".")[1]}`,
       };
       handleUpload(newFile);
     }
   };
 
   const handleUpload = (image) => {
     const data = new FormData();
     data.append("file", image);
     data.append("upload_preset", "logiexpress");
     data.append("cloud_name", "elialvarez");
 
     fetch("https://api.cloudinary.com/v1_1/elialvarez/image/upload", {
       method: "post",
       body: data,
     })
       .then((res) => res.json())
       .then((data) => {
         //console.log(data)
         setSelectedImage(data.url);
       });
   };




  return (
    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : null}
    keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
    <View style={{ flex: 1,  backgroundColor: 'white'  }}>
      {/* <View style={{marginTop:hp("-2%"),marginLeft:wp("0%"),marginBottom:hp("0%")}}>
        <HeaderBar  screen={'null'} style={{color:"white"}}/>
        </View> */}
    <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffffff" }}
      showsVerticalScrollIndicator={false}
    >
      
      {/* Brand View */}
      {/* <ImageBackground
        source={require("../Home/logo.png")}
        resizeMode= "contain"
        style={{
            display:'flex',
            marginTop:  hp('-18%'),
          height: hp('60%') ,
          width: wp('110%') ,
          alignSelf: "center",
        }}
      >
      </ImageBackground> */}
      {/* Botton View */}
      <View style={styles.bottonView}>
        {/* Welcome View */}
        <View style={{ padding: 40, display: "flex", alignItems: "center" }}>
          <Text style={{ color: "#151f27", fontSize: hp("4.7%"),fontWeight: '600', marginTop: hp("3%") }}>
            Datos de tu empresa
          </Text>
          <View style={{ alignItems: "center" }}>
          <Image
          resizeMode="cover"
            source={{
              uri:
                selectedImage !== null
                  ? selectedImage
                  : 
                  "https://memoriamanuscrita.bnp.gob.pe/img/default-user.jpg",
            
                  // "https://girbaud.vteximg.com.br/arquivos/ids/190690-500-500/Gorra-Para-Hombre-Marithe-Francois-Girbaud1217.jpg?v=637732022965400000",
            }}
            style={styles.imgPerfil}
          />
          <View>
            <Text style={{fontSize:hp("3%"), color:"#8a9096"}}>
              Subir logo de la empresa
            </Text>
          </View>

          <View style={styles.add}>
            <TouchableWithoutFeedback onPress={openImagePickerAsync}>
              <Image
                source={require("./add-image.png")}
                style={styles.imgAdd}
              />
            </TouchableWithoutFeedback>

            </View>
            </View>
        </View>
        {/* inputs */}
        <View style={styles.FormView}>
          <TextInput
            name="razon"
            value={reg.razon}
            onChangeText={(name) => handelChangeRazon(name)}
            placeholder="Razón Social*"
            style={styles.TextInput}
          ></TextInput>
    
  
          <TextInput
            value={reg.pais}
            onChangeText={(name) => handelChangePais(name)}
            name="pais"
            placeholder="País*"
            style={styles.TextInput}
            editable={false}
          ></TextInput>
          <View style={{marginTop:hp("-5.5%"), marginLeft:wp("78%")}}>
          <CountryPicker
        placeholder={"▼"}
       withFlag
      //  withFlagButton
       withAlphaFilter
       translation="spa"
      
        // visible
        onSelect={country => {handelChangePais(country.name)}}
    
      />
      </View>
          {reg.pais === "Argentina"
          ?
          <View style={{flex:1,width:hp("70%"), marginLeft:wp("18%")}}>
          <TextInput
          keyboardType="phone-pad"
          value={reg.cuit}
          onChangeText={(name) => handelChangeCuit(name)}          
          name="cuit"
          placeholder="Cuit*"
          style={styles.TextInput2}
        ></TextInput>
        
        <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Seleccionar Condición de Iva"
          title={place.iva}
          data={ivas != null ? ivas : null}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeIva(name[0])}
        />
</View>
        
          
      
      : null
    }
          <TouchableOpacity style={styles.Button} onPress={handleSubmit} >
            <Text style={styles.ButtonText} >
              Siguiente
            </Text>
            {/* <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible1}
                  nRequestClose={() => changeModalVisible1(false)}
                >
                  <SimpleModal1
                    changeModalVisible1={changeModalVisible1}
                    setData1={setData1}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible2}
                  nRequestClose={() => changeModalVisible2(false)}
                >
                  <SimpleModal2
                    changeModalVisible2={changeModalVisible2}
                    setData2={setData2}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible3}
                  nRequestClose={() => changeModalVisible3(false)}
                >
                  <SimpleModal3
                    changeModalVisible3={changeModalVisible3}
                    setData3={setData3}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible4}
                  nRequestClose={() => changeModalVisible4(false)}
                >
                  <SimpleModal4
                    changeModalVisible4={changeModalVisible4}
                    setData4={setData4}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible5}
                  nRequestClose={() => changeModalVisible5(false)}
                >
                  <SimpleModal5
                    changeModalVisible5={changeModalVisible5}
                    setData5={setData5}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible6}
                  nRequestClose={() => changeModalVisible6(false)}
                >
                  <SimpleModal6
                    changeModalVisible6={changeModalVisible6}
                    setData6={setData6}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible7}
                  nRequestClose={() => changeModalVisible7(false)}
                >
                  <SimpleModal7
                    changeModalVisible7={changeModalVisible7}
                    setData7={setData7}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible8}
                  nRequestClose={() => changeModalVisible8(false)}
                >
                  <SimpleModal8
                    changeModalVisible8={changeModalVisible8}
                    setData8={setData8}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible9}
                  nRequestClose={() => changeModalVisible9(false)}
                >
                  <SimpleModal9
                    changeModalVisible9={changeModalVisible9}
                    setData9={setData9}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible40}
                  nRequestClose={() => changeModalVisible40(false)}
                >
                  <SimpleModal40
                    changeModalVisible40={changeModalVisible40}
                    setData40={setData40}
                  />
                  
                  </Modal> */}
          </TouchableOpacity>
          
         
        </View>

        
      </View>
    </ScrollView>
    </View>
    </KeyboardAvoidingView>
  );
};

export default AddEmpresa;

const styles = StyleSheet.create({
  brandView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  brandViewText: {
    color: "#FFC107",
    fontSize: 45,
    fontWeight: "bold",
    textTransform: "uppercase",
    // justifyContent:'flex-start'
  },
  bottonView: {
    flex: 1.5,
    backgroundColor: "white",
    bottom: 50,
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
  },
  FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: -20,
  },
  TextInput: {
    width: wp("90%"),
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("2.9%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  TextInput2: {
    width: wp("90%"),
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("5%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  Button: {
    width: "90%",
    color: "black",
    height: 52,
    backgroundColor: "rgb(24,116,28)",
    borderRadius: 10,
    marginTop: hp("5%"),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  ButtonText: {
    // fontWeight: "bold",
    fontSize: hp("3%"),
    color: "white",
  },
  SingUpText: {
    color: "#4632a1",
    fontSize: 20,
  },
  TextButton: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  preg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  pregunta: {
    color: "red",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    padding: 50,
    marginTop: 20,
    borderRadius: 20,
  },
  text: {
    // marginVertical: 20,
    fontSize: 22,
    color: "white",
    fontWeight: "bold",
  },
  TouchableOpacity: {
    backgroundColor: "black",
    alignSelf: "stretch",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    
  },
  checkbox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  checkboxx: {
    marginTop: 15,
  },
  imgPerfil: {
    width: hp('25%'),
    height: hp('25%'),
    borderRadius: hp("12,5%"),
    borderColor: "rgb(159,209,162)",
    borderWidth: wp('0.8%'),
    marginTop: hp('3%'),
    backgroundColor:"#e1e1e1",
  },
  imgAdd: {
    width: hp("8%"),
    height: hp("8%"),
    marginLeft: wp("35%"),
    marginTop: hp('-13%'),
    borderWidth: hp('0.5%'),
    borderColor: "rgb(189,191,198)",
    borderRadius: 50,
  },
});