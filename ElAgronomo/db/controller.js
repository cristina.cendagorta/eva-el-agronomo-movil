import * as SQLite from 'expo-sqlite';
import { tablesWithIDField } from './models/model';

global.db = SQLite.openDatabase('AGRONOMO.DB');

const ExecuteQuery = (sql, params = []) => new Promise((resolve, reject) => {

    //SQL Query a ejecutar
    //params Parametros

    global.db.transaction((trans) => {
        trans.executeSql(sql, params, (trans, results) => {
            resolve(results);
        },
            (error) => {
                reject(error);
            });
    });
});


// =========== SQLFunctions ===========
const createTables = async (table, datatypes) => {

    //table Nombre de la tabla
    //dataTypes Nombres y tipos de datos

    let types = Object.entries(datatypes);
    let query = "CREATE TABLE IF NOT EXISTS " + table + " ("
    query += "id_sql INTEGER PRIMARY KEY NOT NULL,"

    for (var i = 0; i < types.length; i++) {
        let cname = types[i][0]
        if (cname == "or") {
            cname = "[or]"
        }
        query += cname + " " + (types[i][1] != "object" && types[i][1] != "boolean" ? types[i][1] : "text") + " "
        if (i != types.length - 1) {
            query = query + ",";
        }
    }

    query = query.slice(0, -1)
    query += ");"

    let resp = await ExecuteQuery(query, []);
    return resp;
};

const customSelectSQLite = async (sqlQuery, table, campoKey = null) => {

    //sqlQuery query
    //tabla nombre de tabla
    //campoKey nombre de la key por la cual queremos filtrar, si ponemos id nos va a retornar {id1: {data}, id2: {data}};

    if (campoKey !== null) {

        let select = await ExecuteQuery(sqlQuery, []);
        const dataTypes = tablesWithIDField[table];
        let rows = await select.rows;
        let rowsRes = {}
        for (let i = 0; i < rows.length; i++) {

            let item = rows.item(i);

            for (let j in item) {
                if (item[j] && (dataTypes[j] === "object" || dataTypes[j] === "boolean")) {
                    item[j] = JSON.parse(item[j])
                }
            }
            rowsRes[item[campoKey]] = item;
        }

        return rowsRes;

    } else {

        let select = await ExecuteQuery(sqlQuery, []);
        const dataTypes = tablesWithIDField[table];
        let rows = await select.rows;
        let rowsRes = [];

        for (let i = 0; i < rows.length; i++) {

            let item = rows.item(i);

            for (let j in item) {
                if (item[j] && (dataTypes[j] === "object" || dataTypes[j] === "boolean")) {
                    item[j] = JSON.parse(item[j])
                }
            }
            rowsRes.push(item);

        }

        return rowsRes;
    }

};

const customUpdateSQLite = async (table, record, where, whereValues) => {
    let query = 'UPDATE ' + table + ' SET ';

    const dataTypes = Object.entries(tablesWithIDField[table])

    for (let i = 0; i < dataTypes.length; i++) {

        let data = record[dataTypes[i][0]] //value
        let cname = dataTypes[i][0] //campo
        if (cname == "or") {
            cname = "[or]"
        }
        if (data === undefined || data === null) {
            data = " " + cname + " = " + "null"
        }
        else if (dataTypes[i][1] == "object" || dataTypes[i][1] == "boolean") {
            data = " " + cname + " = " + "'" + JSON.stringify(data) + "'"
        }
        else if (dataTypes[i][1] == "text") {
            data = " " + cname + " = '" + data + "'"
        }
        else if (dataTypes[i][1] == "real") {
            data = " " + cname + " = " + data + ""
        }
        query += data;
        if (i != dataTypes.length - 1) {
            query = query + ",";
        }
    }

    query += ' WHERE ' + where;

    let updateQuery = await ExecuteQuery(query, whereValues);

    return updateQuery;
};

const InsertSQLite = async (table, arrData) => {

    //tabla = nombre de la tabla sqlite
    //arrData = [{ "id": 0, "first_name": "Santiago", "last_name": "Sarmiento" }, { "id": 1, "first_name": "Maria", "last_name": "Furlan"}....]

    if (arrData.length === 0) {
        return false;
    }

    const dataTypes = tablesWithIDField[table]; //accedemos al tipado de los campos de la tabla
    const arrCampos = Object.keys(dataTypes);

    for (let i in arrCampos) {
        if (arrCampos[i] === 'or') {
            arrCampos[i] = '[or]';
        }
    }
    const campos = arrCampos.join();

    let query = `INSERT INTO ${table} (${campos}) VALUES `;

    for (let i = 0; i < arrData.length; ++i) {

        query = query + "(";
        let record = arrData[i];

        for (let j = 0; j < arrCampos.length; ++j) {

            let orderValue = arrCampos[j];
            let type = dataTypes[orderValue];

            let value = record[orderValue] === undefined ?
                null
                :
                type === 'object' || type === 'boolean' ?
                    `'${JSON.stringify(record[orderValue])}'`
                    :
                    type === 'text' ?
                        `'${record[orderValue]}'`
                        :
                        record[orderValue]
                ;

            query += value;
            if (j != arrCampos.length - 1) {
                query = query + ",";
            }

        }

        query += ")";
        if (i != arrData.length - 1) {
            query = query + ",";
        }

    }

    query = query + ";"; //fin de la query

    let multipleInsert = await ExecuteQuery(query, []);

    return multipleInsert;

};

const DeleteTable = async (table) => {
    let deleteQuery = await ExecuteQuery('DELETE FROM ' + table + ' ', []);
    return deleteQuery;
};

const createTablesIfNotExists = () => {
    Object.entries(tablesWithIDField).map((table) => {
        createTables(table[0], table[1])
    })

    return true;
}

const UpdateLastSync = async (tabla, error = 0, errMsg = null) => {
    if (error !== 0) {
        let queryError = `UPDATE SyncInfo SET error = ?, errMsg = ? WHERE k = ?`;
        const updateError = await ExecuteQuery(queryError, [error, errMsg, tabla]);
    } else {
        let query = `UPDATE SyncInfo SET t = ?, error = ?, errMsg = ? WHERE k = ?`;
        let timeInMs = new Date().getTime();
        const update = await ExecuteQuery(query, [timeInMs, error, errMsg, tabla]);
    }
};


const SQLFunctions = {
    ExecuteQuery,
    customSelectSQLite,
    customUpdateSQLite,
    InsertSQLite,
    createTables,
    DeleteTable,
    UpdateLastSync
}

global.SQLF = SQLFunctions

export { createTablesIfNotExists }