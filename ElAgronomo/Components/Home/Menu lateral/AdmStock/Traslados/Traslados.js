import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, SafeAreaView, ActivityIndicator, Image } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import _ from "lodash"
import { useDispatch, useSelector } from "react-redux";
import { useIsFocused } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/core";
import empresa from "../../../../Configuration/empresa.png"
import { getCom } from '../../../../../Redux/actions';
import { getExistIns } from '../../../../../Redux/actions';

export default function Traslados(id2) {

  const dispatch = useDispatch();
  const navigation = useNavigation();
  // me traigo toda la info
  const encabezado = useSelector((store) => store.encabcompra);
  const detalle = useSelector((store) => store.detcompra);
  const getcompra = useSelector((store) => store.getcompra);
  const almacenes = useSelector((store) => store.almacen);
  const ins = useSelector((store) => store.getexistins);
  const isFocused = useIsFocused();
  useEffect(() => {


    if(isFocused){ 
      console.log("encabbbb",encabezado)
console.log("od",id2)
      if(encabezado != null){
        // si hay encabezado, me traigo los movimientos y existencias
        dispatch(getCom(encabezado.id))
        dispatch(getExistIns(encabezado.origen_almacen_id))
      }
        
      
      setTimeout(function(){
        setLoading(true)
          }, 1500);

  } 
  }, [,isFocused]);
  const [isLoading, setLoading] = useState(false);

  var hola = null;
  var hola2 = null;

  console.log("arroz",ins)

  console.log("det2",almacenes)
  if(encabezado != null){
    // filtro almacenes segun id
hola = almacenes.filter((f) => f.id === encabezado.origen_almacen_id)
hola2 = almacenes.filter((f) => f.id === encabezado.destino_almacen_id)
  }

  console.log("holaa",hola)
  console.log("det",detalle)
  if(getcompra != null || getcompra?.length > 0){

     var comprasss = getcompra?.map( item => { 
    return { nombre: item.insumo, precio:item.precio_unitario, cantidad:item.cantidad, unidad:item.unidad }; 
  });

  }
  //defino columnas

  const [ columns, setColumns ] = useState([
    "nombre",
    "cantidad",
    "unidad",
    "precio",
    
  ])
    const [ columns2, setColumns2 ] = useState([
    "Fecha",
    "Almacen Origen",
    "Almacén Destino",
    "N° Mov"

  ])
  const [ direction, setDirection ] = useState(null)
  const [ selectedColumn, setSelectedColumn ] = useState(null)
  const [ encab, setEncab ] = useState([
    {fecha:"2022-03-03",
    orden:"10asdb",
  almacen:1,
nro:222},
])
  const [ pets, setPets ] = useState([
    {nombre:"tierra",
    cantidad:10,
  unidad:"KILOGRAMOS",
  precio:30},
  {nombre:"agua",
  cantidad:10,
unidad:"LITROS",
precio:1},
{nombre:"maní",
cantidad:20,
unidad:"KILOGRAMOS",
precio:1000},
{nombre:"fertilizante",
cantidad:1,
unidad:"ONZA",
precio:30},

    
  ])
  if(pets.includes(detalle)){

  }else{
    if(detalle != null){
    pets.push(detalle)
  }
  }


 
 

  const sortTable = (column) => {
    const newDirection = direction === "desc" ? "asc" : "desc" 
    const sortedData = _.orderBy(pets, [column],[newDirection])
    setSelectedColumn(column)
    setDirection(newDirection)
    setPets(sortedData)
  }
  const tableHeader = () => (
    <View style={styles.tableHeader}>
      {
        columns.map((column, index) => {
          {
            return (
              <TouchableOpacity 
                key={index}
                style={styles.columnHeader} 
                onPress={()=> sortTable(column)}>
                <Text style={styles.columnHeaderTxt}>{column + " "} 
                  { selectedColumn === column && <MaterialCommunityIcons 
                      name={direction === "desc" ? "arrow-down-drop-circle" : "arrow-up-drop-circle"} 
                    />
                  }
                </Text>
              </TouchableOpacity>
            )
          }
        })
      }
    </View>
  )
  const tableHeader2 = () => (
    <View style={styles.tableHeader2}>
      {
        columns2.map((column, index) => {
          {
            return (
              <TouchableOpacity 
                key={index}
                style={styles.columnHeader2} 
                >
                <Text style={styles.columnHeaderTxt2}>{column + " "} 
                  { selectedColumn === column && <MaterialCommunityIcons 
                      name={direction === "desc" ? "arrow-down-drop-circle" : "arrow-up-drop-circle"} 
                    />
                  }
                </Text>
              </TouchableOpacity>
            )
          }
        })
      }
    </View>
  )


  {if(isLoading === false ){     
    return(
  <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
  <ActivityIndicator size="large" color="#007d3c" />
</View>
)
}
}
if(isLoading === true && (encabezado === null || encabezado === undefined || encabezado.tipo_movimiento_id != 3 )   ){
  console.log("get",getcompra)
  // navigation.navigate("EncabTras")
  return(
    <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>

<Image
source={empresa}
resizeMode= "contain"
style={{
    display:'flex',
    marginTop:  hp('-8%'),
  height: hp('70%') ,
  width: wp('80%') ,
  alignSelf: "center",
}}
>
</Image>

<View style={{alignItems:"center"}}>
<Text style={{
  color:"#182E44",
  fontSize:hp("2.6%"),
  fontWeight:"bold",
  marginTop:hp("-15%")}}>
  Ingresa un Traslado
</Text>
</View>
<View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
    <TouchableOpacity onPress={() => navigation.navigate("EncabTras")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
      <View style={{ alignItems: 'center'}}>
        <Text
        
          style={{  
            fontSize: hp("2.8%"),
            color:"white",
            marginTop:hp("2%")                       
            
          }}>
          + Agregar
        </Text>
      </View>
    </TouchableOpacity>
    </View>
</SafeAreaView>


)

}

{
  if(isLoading === true &&  encabezado.tipo_movimiento_id === 3  ){


  return (
        <SafeAreaView style={{backgroundColor: "white", flex: 1}}>
    <View style={styles.container2}>
      <FlatList 
        data={[encabezado]} // muestro en tabla el encabezado
        style={{width:"100%"}}
        keyExtractor={(item, index) => index+""}
        ListHeaderComponent={tableHeader2}
        stickyHeaderIndices={[0]}
        renderItem={({item, index})=> {
          return (
            <View>

            
            <View style={{...styles.tableRow, backgroundColor: index % 2 == 1 ? "#F0FBFC" : "white"}}>
              <Text style={{...styles.columnRowTxt2, fontWeight:"bold", flex:1}} numberOfLines={1}>{item.fecha_movimiento}</Text>
              <Text style={{...styles.columnRowTxt2, flex:1}}numberOfLines={1}>{hola[0].nombre}</Text>
              <Text style={{...styles.columnRowTxt2, flex:1}}numberOfLines={1}>{hola2[0].nombre}</Text>
              <Text style={{...styles.columnRowTxt2, flex:1}}numberOfLines={1}>{item.nro_movimiento}</Text>
          
            </View>
                <View style={{backgroundColor:"lightgrey", height:hp("0.2%")}}>

                </View>
                </View>
          )
        }}
      />
      <StatusBar style="auto" />
    </View>
    
    <View style={styles.container}>
      <FlatList 
        data={comprasss} // muestro el detalle de los traslados
        style={{width:"100%"}}
        keyExtractor={(item, index) => index+""}
        ListHeaderComponent={tableHeader}
        stickyHeaderIndices={[0]}
        renderItem={({item, index})=> {
          return (
            <View style={{...styles.tableRow, backgroundColor: index % 2 == 1 ? "#F0FBFC" : "white"}}>
              <Text style={{...styles.columnRowTxt, fontWeight:"bold", flex:1}} numberOfLines={1}>{item.nombre}</Text>
              <Text style={{...styles.columnRowTxt, flex:1}}numberOfLines={1}>{item.cantidad}</Text>
              <Text style={{...styles.columnRowTxt, flex:1}}numberOfLines={1}>{item.unidad}</Text>
              <Text style={{...styles.columnRowTxt, flex:1}}numberOfLines={1}>${item.precio}</Text>
            </View>
          )
        }}
      />
      <StatusBar style="auto" />
    </View>
    <View style={{alignSelf:"center", justifyContent:"center", backgroundColor:"black", width:wp("100%") }} >
        <TouchableOpacity onPress={() => navigation.navigate("DetTras",encabezado.id)} style={{ backgroundColor:"green", width:wp("10%"), height:wp("10%"),marginTop:hp("-1%"), borderRadius:wp("5%"), alignSelf:"center"}}>
          <View style={{ alignItems: 'center'}}>
            <Text
            
              style={{  
                fontSize: hp("4.5%"),
                color:"white",
                // marginTop:hp("2%")           
                textAlignVertical:"center"            
                
              }}>
              +
            </Text>
          </View>
        </TouchableOpacity>
        </View>
    </SafeAreaView>
  );
}
}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop:0
  },
  container2: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom:hp("-40%"),
  },
  tableHeader: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: "#37C2D0",
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    height: 50
  },
  tableHeader2: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: "orange",
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    height: 50
  },
  tableRow: {
    flexDirection: "row",
    height: 40,
    alignItems:"center",
  },
  columnHeader: {
    width: "25%",
    justifyContent: "center",
    alignItems:"center"
  },
  columnHeaderTxt: {
    color: "white",
    fontWeight: "bold",
  },
  columnRowTxt: {
    
    width:"25%",
    textAlign:"center",
    
  },
  columnHeader2: {
    width: "25%",
    justifyContent: "center",
    alignItems:"center"
  },
  columnHeaderTxt2: {
    color: "white",
    fontWeight: "bold",
  },
  columnRowTxt2: {
    
    width:"25%",
    textAlign:"center",
    
  }
});


