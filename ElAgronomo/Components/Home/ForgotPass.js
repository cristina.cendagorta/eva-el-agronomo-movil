import React, { useState, useEffect, useRef,useMemo } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { useNavigation } from "@react-navigation/core";
import {
  Text,
  ScrollView,
  Image,
  Dimensions,
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Modal
} from "react-native";
import {MaterialCommunityIcons} from "@expo/vector-icons"
import { useDispatch, useSelector } from "react-redux";
import { forgotmail } from "../../Redux/actions";
import SimpleModal5 from './../Alerts/Login/SimpleModalmail';

// ESTE ES POR SI OLVIDASTE LA CONTRASEÑA
const ForgotPass = () => {
    const dispatch = useDispatch();
    const navigation = useNavigation();

    const [reg, setReg] = useState({   
        mail: "",
      });

           //MAIL MAL INGRESADO
           const [isModalVisible5, setisModalVisible5] = useState(false);
           const [chooseData5, setchooseData5] = useState();
         
           const changeModalVisible5 = (bool) => {
             setisModalVisible5(bool);
           };
         
           const setData5 = (data) => {
             setchooseData5(data);
           };

      const handleSubmit = (e) => {
        e.preventDefault();
        const obj = {
          mail: reg.mail,    
        };

        if(!obj.mail || !obj.mail.includes("@") ){
          changeModalVisible5(true)
          return;
        }
    
    dispatch(forgotmail(obj));
     console.log("Estoy enviado", obj);
        setReg({ 
            mail: "",
        });
    navigation.navigate("ForgotPass2",reg.mail)
    };

    const handelChangeMail = (name) => {
        setReg({
          ...reg,
          mail: name,
        });
      };

    return(
        <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
        <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : null}
      keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
      <ScrollView
        style={{ flex: 1, backgroundColor: "#ffffffff" }}
        showsVerticalScrollIndicator={false}
      >
                  {/* <View> */}
              
               <Image
               source={require("./mail2.png")}
               resizeMode= "contain"
               style={styles.img}></Image>
        {/* </View> */}
          <View>
              <Text style={styles.texto}>¿Olvidaste tu contraseña?</Text>
        </View>
        <View style={styles.view2}>
              <Text style={styles.texto2}>¡No te preocupes! Ingresa tu correo electrónico, y te enviaremos un código de verificación de 6 dígitos. </Text>
        </View>
        <View>
        <TextInput
            icon="mail"
            value={reg.mail}
            onChangeText={(name) => handelChangeMail(name)}
            name="mail"
            placeholder="Dirección de Mail*"
            style={styles.TextInput}
            returnKeyType="next"
         
        
          ></TextInput>
        </View>


            <TouchableOpacity style={styles.Button} onPress={handleSubmit}>
                <Text style={styles.ButtonText} >
                  Enviar
                </Text>
                <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible5}
                  nRequestClose={() => changeModalVisible5(false)}
                >
                  <SimpleModal5
                    changeModalVisible5={changeModalVisible5}
                    setData5={setData5}
                  />                  
                  </Modal>
              </TouchableOpacity>  
        </ScrollView> 
        </KeyboardAvoidingView>
        </SafeAreaView>
        );
}
export default ForgotPass;

const styles = StyleSheet.create({
    img:{
        display:'flex',
        marginTop:  hp('-2%'),
        marginBottom: hp('-9%'),
      height: hp('50%') ,
      width: wp('60%') ,
      alignSelf: "center",
    },
    texto:{
        fontSize:hp("4%"),
        fontWeight:"bold",
        textAlign:"center",
        marginTop:hp("3%"),
        color:"rgb(0,140,207)"
    },
    texto2:{
        fontSize:hp("2.8%"),
        textAlign:"center",
        color:"grey"
    },
    view2:{
        width:wp("90%"), 
        alignSelf:"center", 
        marginTop:hp("1%")
    },
    newpass:{
        marginTop:hp("-22%")
    },
    otp:{
        width:wp("90%"),
        alignSelf:"center",
        marginTop:hp("-20%")
    },
    otpin:{
        borderRadius:hp("1%"),
        backgroundColor:"rgb(189,191,198)",
        color:"black",
        fontSize:hp("3.5%"),
        fontWeight:"bold"
    },
    inputs:{
        marginTop:hp("3.5%"),
        alignSelf:"center",
        width:wp("100%")
    },
    TextInput: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#eceef2",
        height: hp("7.8%"),
        borderRadius: 10,
        paddingLeft: hp("2.5%"),
        marginTop: hp("3%"),
        color: "black",
        fontSize:hp("2.3%"),
        backgroundColor:"#eceef2",
        marginLeft:wp("5%")
      },
      btnEye: {
        position:"absolute",
        alignSelf:"flex-end",
        marginTop:hp("5%"),
        paddingRight:wp("8%")
      },
      btnEye2: {
        position:"absolute",
        alignSelf:"flex-end",
        marginTop:hp("16%"),
        paddingRight:wp("8%")
      },
      Button: {
        width: "90%",
        color: "black",
        height: hp("8%"),
        backgroundColor: "rgb(0,140,207)",
        borderRadius: 10,
        marginTop: hp("8%"),
        display: "flex",
        justifyContent: "center",
        alignSelf: "center",
        shadowOpacity: 80,
        elevation: 15,
      },
      ButtonText: {
        alignSelf:"center",
        fontWeight: "bold",
        fontSize: hp("3.3%"),
        color: "white",
      },
})