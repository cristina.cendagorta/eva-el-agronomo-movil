import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, SafeAreaView, ActivityIndicator, Image, TextInput, Switch, } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import _ from "lodash"
import { useDispatch, useSelector } from "react-redux";
import { useIsFocused } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/core";
import empresa from "../../../../Configuration/empresa.png"
import { BottomSheet } from 'react-native-sheet';
import Select2 from "react-native-select-two"

export default function Existencias() { // este componente muestra las existencias

  const dispatch = useDispatch();
  const navigation = useNavigation();
  // me traigo encabezado, detalle, establecimientos, almacenes, y existencias
  const encabezado = useSelector((store) => store.encabtras);
  const detalle = useSelector((store) => store.dettras);
  const isFocused = useIsFocused();
  const bottomSheet = useRef(null); // estos son para los menu de filtrado y de busqueda
  const bottomSheet2 = useRef(null);
  const establecimientos = useSelector((store) => store.establecimiento);
  const almacenes = useSelector((store) => store.almacen);
  const existencias = useSelector((store) => store.getexist);
  useEffect(() => {
    if(isFocused){ 
      setTimeout(function(){
        setLoading(true)
          }, 1500);

  } 
  }, [,isFocused]);
  const [isLoading, setLoading] = useState(false);
  //establecimiento para el select
  var ests = establecimientos?.map( item => { 
    return { name: item.nombre, id:item.id }; 
  });

  console.log("exist",existencias)

  if(existencias != null){
    // existencias para poner en las tablas
    var exts = existencias?.map( item => { 
      return { Insumo: item.insumo, Almacen:item.almacen, Cantidad:item.cantidad, Unidad:item.unidad, Precio:item.precio_unitario }; 
    });

  }
//almacenes para el select
  var alms = almacenes?.map( item => { 
    return { name: item.nombre, id:item.id }; 
  });


  console.log("det",encabezado)

  //defino columnas

  const [ columns, setColumns ] = useState([
    "Insumo",
    "Almacen",
    "Cantidad",
    "Unidad",
    "Total",
    
    
  ])

  //defino estados locales

  const [reg, setReg] = useState({
    buscar: "",
    est:"",
    alm:"",

  });


  const [place, setPlace] = useState({
    est: "Establecimiento",
    alm:"Almacén",

  });

  //todo esto es para el filtrado y la busqueda

  const [isEnabled, setIsEnabled] = useState(false);
const toggleSwitch = () => {
  setIsEnabled(previousState => !previousState)
  console.log("hola");

  const pets4 = pets2 != null ? pets2 : pets
  pets2 != null ? setPets2(null) : null
  console.log("petsss",pets4)
const result = pets4.filter((o) => o.Cantidad < 100)
console.log("result",result,"hola")
      setPets3(result)

bottomSheet.current.hide()
setIsEnabled(false)
}
  const [ direction, setDirection ] = useState(null)
  const [ selectedColumn, setSelectedColumn ] = useState(null)
  const [ pets, setPets ] = useState(exts)
// {
//   Insumo:"Glifosato",
//   Almacen: "El Silo",
//   Cantidad:500,
//   Unidad: "Metros Cubicos",
//   Total:100500,

// },
// {
//     Insumo:"Semillas",
//     Almacen: "El Monte",
//     Cantidad:50,
//     Unidad: "Kilogramos",
//     Total:5000,
//     establecimiento_id:3
  
//   },
//   {
//     Insumo:"Nafta",
//     Almacen: "El Silo",
//     Cantidad:500,
//     Unidad: "Galon",
//     Total:75000,
  
//   },
//   {
//     Insumo:"Agua Mineral",
//     Almacen: "El Silo",
//     Cantidad:8070,
//     Unidad: "Litros",
//     Total:8070,
  
//   },
//   {
//     Insumo:"Clavos",
//     Almacen: "El Silo",
//     Cantidad:80,
//     Unidad: "Kilogramos",
//     Total:1070,
  
//   },

    
//   ])
  if(pets.includes(detalle)){

  }else{
    if(detalle != null){
    pets.push(detalle)
  }
  }


  var [ pets2, setPets2 ] = useState(null)
  var [ pets3, setPets3] = useState(null)

  const handelChangeBuscar = (name) => {
    setReg({
      ...reg,
      buscar: name.toLowerCase(),
    });
  };
console.log("pets2",pets2)
  const handlesubmit= () =>{
    // esto es la busqueda
    // setPets2(pets.filter((f) => f.Object.values(reg.buscar)))
    const pets4 = pets3 != null ? pets3 : pets
    pets3 != null ? setPets3(null) : null
    console.log("pets4", pets4)
    const result = pets4.filter(o => 
      Object.keys(o).some(k => 
          o[k].toString().toLowerCase().indexOf(reg.buscar) !== -1));
console.log("result",result)
          setPets2(result)
    console.log(pets,"pets2")
    bottomSheet2.current.hide()
    setReg({buscar:""})
    
  }

  const handelChangeEst = (name) => {
    setReg({
      ...reg,
      est: name,
    });

    // const filtrado6 = establecimientos?.filter((f) => f.id === name)
    // setPlace({
    //   ...place,
    //  est: filtrado6[0].nombre,
    // });
    setPlace({
      ...place,
     est: "Establecimiento",
    });

    const pets4 = pets2 != null ? pets2 : pets
    pets2 != null ? setPets2(null) : null
    console.log("petsss",pets4)
  const result = pets4.filter((o) => o.establecimiento_id === 3)
console.log("result",result,"hola")
        setPets3(result)
 
  bottomSheet.current.hide()
  //   handelChangeFilter(place.est)
  };

  const clean = () => {
    setPets2(null)
    setPets3(null)
}
  const handelChangeAlm = (name) => {
    console.log("alm",name)
    setReg({
      ...reg,
      alm: name,
    });

    const filtrado6 = alms?.filter((f) => f.id === name)
    setPlace({
      ...place,
     alm: "Almacén",
    });
    // handelChangeFilter(place.alm)

    const pets4 = pets2 != null ? pets2 : pets
    pets2 != null ? setPets2(null) : null
    const result = pets4.filter((o) => o.Almacen === filtrado6[0].name)
        setPets3(result)
 
  bottomSheet.current.hide()

  };
 

  const sortTable = (column) => {
    const newDirection = direction === "desc" ? "asc" : "desc" 
    const sortedData = _.orderBy(pets2 != null ? pets2 : pets3 != null ? pets3 : pets, [column],[newDirection])
    setSelectedColumn(column)
    setDirection(newDirection)
    pets2 != null ? setPets2(sortedData) : pets3 != null ? setPets3(sortedData): setPets(sortedData)
  }
  const tableHeader = () => (
    <View style={styles.tableHeader}>
      {
        columns.map((column, index) => {
          {
            return (
              <TouchableOpacity 
                key={index}
                style={styles.columnHeader} 
                onPress={()=> sortTable(column)}>
                <Text style={styles.columnHeaderTxt}>{column + " "} 
                  { selectedColumn === column && <MaterialCommunityIcons 
                      name={direction === "desc" ? "arrow-down-drop-circle" : "arrow-up-drop-circle"} 
                    />
                  }
                </Text>
              </TouchableOpacity>
            )
          }
        })
      }
    </View>
  )
  const tableHeader2 = () => (
    <View style={styles.tableHeader2}>
      {
        columns2.map((column, index) => {
          {
            return (
              <TouchableOpacity 
                key={index}
                style={styles.columnHeader2} 
                >
                <Text style={styles.columnHeaderTxt2}>{column + " "} 
                  { selectedColumn === column && <MaterialCommunityIcons 
                      name={direction === "desc" ? "arrow-down-drop-circle" : "arrow-up-drop-circle"} 
                    />
                  }
                </Text>
              </TouchableOpacity>
            )
          }
        })
      }
    </View>
  )

//   const tableHeader3 = () => (
//     <View style={styles.tableHeader2}>
//       {
//         columns3.map((column, index) => {
//           {
//             return (
//               <TouchableOpacity 
//                 key={index}
//                 style={styles.columnHeader2} 
//                 >
//                 <Text style={styles.columnHeaderTxt2}>{column + " "} 
//                   { selectedColumn === column && <MaterialCommunityIcons 
//                       name={direction === "desc" ? "arrow-down-drop-circle" : "arrow-up-drop-circle"} 
//                     />
//                   }
//                 </Text>
//               </TouchableOpacity>
//             )
//           }
//         })
//       }
//     </View>
//   )

  {if(isLoading === false ){     //cargando
    return(
  <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
  <ActivityIndicator size="large" color="#007d3c" />
</View>
)
}
}

{
  if(isLoading === true  ){


  return (
        <SafeAreaView style={{backgroundColor: "white", flex: 1}}>
            <BottomSheet
        // borderRadius={hp("20%")}
        draggable={true}
        hasDraggableIcon
        ref={bottomSheet}
        colorScheme="dark"
        height={hp("40%")}>
        <View style={{ marginBottom:hp("3%")}}>
          <Text style={{ fontWeight:"bold",fontSize:23, color:"white", alignSelf:"center"}}>Filtros</Text>
        </View>
        <View style={styles.FormView}>


        <Select2
          isSelectSingle
          style={styles.TextInput2}
          colorTheme="green"
          popupTitle="Almacén"
          title={place.alm}
          data={alms}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeAlm(name[0])}
        />
        <Select2
          isSelectSingle
          style={styles.TextInput2}
          colorTheme="green"
          popupTitle="Establecimiento"
          title={place.est}
          data={ests}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeEst(name[0])}
        />
                <View style={styles.switch}>
        <Text style={{marginTop:hp("2.2%"), marginRight:wp("8%"), fontSize:hp("2.5%"), color:"lightgrey"}}>
            Mostrar insumos con bajo stock
          </Text>
    
      <Switch
      style={{ transform: [{ scaleX: wp("0.3%") }, { scaleY: wp("0.3%") }] }}
        trackColor={{ false: "tomato", true: "#37C2D0" }}
        thumbColor={isEnabled ? "white" : "white"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isEnabled}
      />
      
    </View>

        {/* <View style={{marginLeft:wp("3%")}} >
        <TouchableOpacity  style={{paddingVertical: 15}} onPress>
          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: wp("3%")}}>
            <MaterialCommunityIcons name="office-building" size={hp("3.5%")} color={"black"} />
            <Text
              style={{  
                fontSize: hp("2.7%"),
                color:"black",
                // fontFamily: 'Roboto-Medium',
                marginLeft: wp("6%"),
              }}>
              Empresa
            </Text>
          </View>
        </TouchableOpacity>
        </View>
        <View style={{height:hp("0.1%"), backgroundColor:"lightgrey", width:wp("90%"), marginLeft: wp("15%"),}}>
        </View> */}
 
          
        {/* <View style={{marginLeft:wp("3%")}}>
        <TouchableOpacity onPress={() => {}} style={{paddingVertical: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: wp("3%")}}>
            <MaterialCommunityIcons name="account-multiple" size={hp("3.5%")} color={"black"} />
            <Text
              style={{  
                fontSize: hp("2.7%"),
                color:"black",
                // fontFamily: 'Roboto-Medium',
                marginLeft: wp("6%"),
              }}>
              Usuarios
            </Text>
          </View>
        </TouchableOpacity>
        </View> */}
        </View>
      </BottomSheet>
      <BottomSheet
        // borderRadius={hp("20%")}
        draggable={true}
        hasDraggableIcon
        ref={bottomSheet2}
        colorScheme="dark"
        // backdropBackgroundColor="white"
        // colorScheme="dark"
        height={hp("30%")}>
        <View style={{ marginBottom:hp("0%")}}>
          <Text style={{fontWeight:"bold", fontSize:23, color:"white", alignSelf:"center"}}>Búsqueda</Text>
        </View>
        <View style={{marginLeft:wp("3%")}} >
        <TouchableOpacity  style={{paddingVertical: 15}} onPress>
          <View style={{flexDirection: 'row', alignItems: 'center', alignSelf:"center"}}>
          
            <TextInput
            value={reg.buscar}
            onChangeText={(name) => handelChangeBuscar(name)}
            name="buscar"
            placeholder="Ingrese lo que desee buscar"
            style={styles.TextInput}
            
          ></TextInput>
            {/* <Text
              style={{  
                fontSize: hp("2.7%"),
                color:"black",
                // fontFamily: 'Roboto-Medium',
                marginLeft: wp("6%"),
              }}>
              Empresa
            </Text> */}
          </View>
        </TouchableOpacity>
        </View>


        <TouchableOpacity style={styles.Button} onPress={handlesubmit}>
        <View style={{flexDirection:"row", justifyContent:"center"}}>
        <MaterialCommunityIcons name="magnify" size={hp("3.6%")} color={"white"} />
            <Text style={styles.ButtonText} >
              Buscar
            </Text>
            </View>
          </TouchableOpacity>
        
      </BottomSheet>
          <Text style={{fontWeight:'bold', alignSelf:"center", fontSize:hp("3%")}}numberOfLines={1}>Existencias de Stock</Text>
    {/* <View style={styles.container2}>
      <FlatList 
        data={[pets]}
        style={{width:"100%"}}
        keyExtractor={(item, index) => index+""}
        ListHeaderComponent={tableHeader2}
        stickyHeaderIndices={[0]}
        renderItem={({item, index})=> {
          return (
            <View>

            
            <View style={{...styles.tableRow, backgroundColor: index % 2 == 1 ? "#F0FBFC" : "white"}}>
              <Text style={{...styles.columnRowTxt2, fontWeight:"bold", flex:1}} numberOfLines={1}>{item.nombre}</Text>
              <Text style={{...styles.columnRowTxt2, flex:1}}numberOfLines={1}>{item.cantidad}</Text>
              <Text style={{...styles.columnRowTxt2, flex:1}}numberOfLines={1}>{item.unidad}</Text>
          
            </View>
                <View style={{backgroundColor:"lightgrey", height:hp("0.2%")}}>

                </View>
                </View>
          )
        }}
      />
      <StatusBar style="auto" />
    </View> */}
    {/* <View style={styles.container2}>
      <FlatList 
        data={[encabezado]}
        style={{width:"100%"}}
        keyExtractor={(item, index) => index+""}
        ListHeaderComponent={tableHeader3}
        stickyHeaderIndices={[0]}
        renderItem={({item, index})=> {
          return (
            <View>

            
            <View style={{...styles.tableRow, backgroundColor: index % 2 == 1 ? "#F0FBFC" : "white"}}>
              <Text style={{...styles.columnRowTxt2, fontWeight:"bold", flex:1}} numberOfLines={1}>{item.movimiento}</Text>
              <Text style={{...styles.columnRowTxt2, flex:1}}numberOfLines={1}>{item.almacen}</Text>
              <Text style={{...styles.columnRowTxt2, flex:1}}numberOfLines={1}>{item.almacen2}</Text>
          
            </View>
                <View style={{backgroundColor:"lightgrey", height:hp("0.2%")}}>

                </View>
                </View>
          )
        }}
      />
      <StatusBar style="auto" />
    </View> */}
    
    <View style={{backgroundColor:"#f9f9f9", height:hp("12%"), flexDirection:"row", width:wp("100%"),alignItems:"center",justifyContent:"space-evenly", marginTop:hp("2%")}}>
    <TouchableOpacity onPress={() => clean()} style={{ backgroundColor:"white", width:wp("30%"), height:hp("7%"), borderRadius:wp("5%"), borderWidth:wp("0.2%"),borderColor:"#f1f1f1"}}>
    <View style={{  flexDirection:"row", justifyContent:"center"}}>
      <MaterialCommunityIcons name="table" size={hp("3.5%")} style={{marginTop:hp("1.6%")}} color={"black"} />
        <Text
        
          style={{  
            fontSize: hp("2.8%"),
            color:"black",
            marginTop:hp("1.5%")                       
            
          }}>
          Todos
        </Text>
      </View>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => bottomSheet2.current.show()} style={{ backgroundColor:"white", width:wp("30%"), height:hp("7%"), borderRadius:wp("5%"), borderWidth:wp("0.2%"),borderColor:"#f1f1f1"}}>
      <View style={{  flexDirection:"row", justifyContent:"center"}}>
      <MaterialCommunityIcons name="magnify" size={hp("3.5%")} style={{marginTop:hp("1.6%")}} color={"black"} />
        <Text
        
          style={{  
            fontSize: hp("2.8%"),
            color:"black",
            marginTop:hp("1.5%")                       
            
          }}>
          Buscar
        </Text>
      </View>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => bottomSheet.current.show()} style={{ backgroundColor:"white", width:wp("30%"), height:hp("7%"), borderRadius:wp("5%"), borderWidth:wp("0.2%"),borderColor:"#f1f1f1"}}>
    <View style={{  flexDirection:"row", justifyContent:"center"}}>
      <MaterialCommunityIcons name="filter-variant" size={hp("3.5%")} style={{marginTop:hp("1.6%")}} color={"black"} />
        <Text
        
          style={{  
            fontSize: hp("2.8%"),
            color:"black",
            marginTop:hp("1.5%")                       
            
          }}>
          Filtrar
        </Text>
      </View>
    </TouchableOpacity>

    </View>
    
    <View style={styles.container}>
      
      <FlatList 
        data={pets2 != null ? pets2 : pets3 != null ? pets3 : pets}
        style={{width:"100%"}}
        keyExtractor={(item, index) => index+""}
        ListHeaderComponent={tableHeader}
        stickyHeaderIndices={[0]}
        renderItem={({item, index})=> {
          return (
            <View style={{...styles.tableRow, backgroundColor: item.Cantidad < 100 ? "#ffadad" : index % 2 == 1 ? "#F0FBFC" : "white"}}>
              <Text style={{...styles.columnRowTxt, fontWeight:"bold", flex:1}} numberOfLines={1}>{item.Insumo}</Text>
              <Text style={{...styles.columnRowTxt, flex:1}}numberOfLines={1}>{item.Almacen}</Text>
              <Text style={{...styles.columnRowTxt,flex:1}}numberOfLines={1}>{item.Cantidad}</Text>
              <Text style={{...styles.columnRowTxt, flex:1}}numberOfLines={1}>{item.Unidad}</Text>
              <Text style={{...styles.columnRowTxt, fontWeight:"bold",flex:1}}numberOfLines={1}>${item.Precio*item.Cantidad}</Text>
            </View>
          )
        }}
      />
      <StatusBar style="auto" />
    </View>
    </SafeAreaView>
  );
}
}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:hp("3%")
  },
  container2: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom:hp("-37%"),
  },
  tableHeader: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: "#37C2D0",
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    height: 50
  },
  tableHeader2: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: "orange",
    // borderTopEndRadius: 10,
    // borderTopStartRadius: 10,
    height: hp("7%")
  },
  tableRow: {
    flexDirection: "row",
    height: 40,
    alignItems:"center",
  },
  columnHeader: {
    width: "20%",
    justifyContent: "center",
    alignItems:"center"
  },
  columnHeaderTxt: {
    color: "white",
    fontWeight: "bold",
  },
  columnRowTxt: {
    
    width:"20%",
    textAlign:"center",
    
  },
  columnHeader2: {
    width: "20%",
    justifyContent: "center",
    alignItems:"center"
  },
  columnHeaderTxt2: {
    color: "white",
    fontWeight: "bold",
  },
  columnRowTxt2: {
    
    width:"20%",
    textAlign:"center",
    
  },
  switch:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop:hp("1%")

},
  TextInput: {
    width: wp("90%"),
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    textAlign:"center",
    // marginLeft: wp("5%"),
    // marginBottom:hp("3%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
    TextInput2: {
    width: wp("90%"),
    borderWidth: 2,
    borderColor: "#37C2D0",
    height: hp("3.7%"),
    borderRadius: 10,
    textAlign:"center",
    // marginLeft: wp("5%"),
    marginBottom:hp("2%"),
    color: "black",
    backgroundColor:"lightgrey",
    fontSize:hp("2%"),
  },
  Button: {
    width: "60%",
    color: "white",
    height: hp("7%"),
    backgroundColor: "rgba(40,203,53,255)",
    borderRadius: 10,
    // marginTop: hp("5%"),
    display: "flex",
    justifyContent: "center",
    alignSelf: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  ButtonText: {
    // fontWeight: "bold",
    alignSelf:"center",
    fontSize: hp("2.6%"),
    color: "white",
  },
    FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: hp("-1%"),
  },
});