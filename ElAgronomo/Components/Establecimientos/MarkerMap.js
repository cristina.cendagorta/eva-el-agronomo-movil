import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import MapView, {
  MAP_TYPES,
  Polygon,
  ProviderPropType,
  Marker,
} from 'react-native-maps';
import {MaterialCommunityIcons} from "@expo/vector-icons"
const { width, height } = Dimensions.get('window');
import { newLote } from '../../Redux/actions';
import { connect } from "react-redux";
const ASPECT_RATIO = width / height;
const LATITUDE = -34.61315;
const LONGITUDE = -58.37723;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;

class MarkerMap extends React.Component {
  constructor(props) {
    super(props);
    const hola = JSON.parse(this.props.route.params.route.params.geoposicion)
    console.log("asd0",hola[0])
    this.state = {
      showsUserLocation: true,
      followsUserLocation : true,
      region: {
          
        latitude: hola[0].latitude,
        longitude: hola[0].longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      nombre:this.props.route.params.route.params.nombre,
      editar:false,
      geo: hola[0],
      polygons: [],
      editing:{
        coordinates : hola
      },
      creatingHole: false,
      area: 0,
    };
    console.log("editing",this.state.geo);
  }

  

  render() {
    const mapOptions = {
      scrollEnabled: true,
    };

    if (this.state.editing) {
      mapOptions.scrollEnabled = true;
    }

    return (
      <View style={styles.container}>
        
        
        <MapView
          provider={this.props.provider}
          style={styles.map}
          mapType={MAP_TYPES.HYBRID}
          initialRegion={this.state.region}

          {...mapOptions}
        >
          
              
           
              <Marker
              coordinate={this.state.geo}
              pinColor='purple'
              title={this.state.nombre}
             //  id={polygon.id}
             //  centerOffset={{ x: -58, y: 100 }}
           // anchor={{ x: 0.5, y: 0.5 }}
              >
              {/* <Image
              source={require('./pin.png')}
              style={{width: hp("3%"), height: hp("3%")}}
              resizeMode="cover"
            /> */}
            
             </Marker>
          
          

        </MapView>
  
      </View>
    );
  }
}

MarkerMap.propTypes = {
  provider: ProviderPropType,
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    // paddingHorizontal: 18,
    height:hp("10%"),
    paddingVertical: 12,
    borderRadius: hp("2%"),
    // flexDirection: 'row',
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: wp("30%"),
    paddingHorizontal: 12,
    alignItems: 'center',
    textAlignVertical:"center",
    marginHorizontal: 10,
    backgroundColor:"rgba(0,0,0,0.8)",
    // flexDirection: 'row',
    
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

export default connect(null, {newLote}) (MarkerMap);
