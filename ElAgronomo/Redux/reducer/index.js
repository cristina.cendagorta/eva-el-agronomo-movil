import axios from "axios";

//ACA ME GUARDO LOS GETS, Y LAS RESPUESTAS DE LOS POSTS

const initialState = {
    //hago un estado inicial
    responseLog : null,
    responseEmp : null,
    empresa: null,
    responseEst : null,
    establecimiento: null,
    responseAlm : null,
    almacen: null,
    responseIns : null,
    insumo: null,
    responseLote : null,
    lote: null,
    iva:null,
    rubemp: null,
    tipest:null,
    zonas:null,
    tipalm:null,
    monedas:null,
    unidades:null,
    familias: null,
    subfamilias:null,
    tareas:null,
    rubros:null,
    erogaciones:null,
    encabcompra:null,
    detcompra:null,
    encabajuste:null,
    detajuste:null,
    encabtras:null,
    dettras:null,
    getcompra:null,
    getexist:null,
    getexistins:null,
    movimientos:null,
    getdetalle:null,

  };



  export default function rootReducer(state = initialState, action) {
    switch (action.type) {
  
      case "LOGEO":
        return {
          ...state,
          responseLog: action.payload, //en registrarusuario meteme el action.payload
          // token: action.token
        };
        case "NEWEMP":
          return {
            ...state,
            responseEmp: action.payload, //en registrarusuario meteme el action.payload
            // token: action.token
          };
          case "GET_EMPRESA":
            return {
              ...state,
              empresa: action.payload
            }
            case "GET_DETALLE":
              return {
                ...state,
                getdetalle: action.payload
              }
            case "NEWEST":
              return {
                ...state,
                responseEst: action.payload, //en registrarusuario meteme el action.payload
                // token: action.token
              };
            case "GET_ESTABLECIMIENTO":
              return {
                ...state,
                establecimiento: action.payload
              }
              case "NEWALM":
                return {
                  ...state,
                  responseAlm: action.payload, //en registrarusuario meteme el action.payload
                  // token: action.token
                };
              case "GET_ALMACEN":
                return {
                  ...state,
                  almacen: action.payload
                }
                case "NEWINS":
                  return {
                    ...state,
                    responseIns: action.payload, //en registrarusuario meteme el action.payload
                    // token: action.token
                  };
                case "GET_INSUMO":
                  return {
                    ...state,
                    insumo: action.payload
                  }
                  case "NEWLOTE":
                    return {
                      ...state,
                      responseLote: action.payload, //en registrarusuario meteme el action.payload
                      // token: action.token
                    };
                  case "GET_LOTE":
                    return {
                      ...state,
                      lote: action.payload
                    }
                    case "GET_IVA":
                      return {
                        ...state,
                       iva: action.payload
                      }
                    case "GET_RUBEMP":
                      return {
                        ...state,
                       rubemp: action.payload
                      }
                      case "GET_MON":
                        return {
                          ...state,
                         monedas: action.payload
                        }
                        case "GET_TIPEST":
                          return {
                            ...state,
                           tipest: action.payload
                          }
                        case "GET_ZONAS":
                          return {
                            ...state,
                           zonas: action.payload
                          }
                          case "GET_TIPALM":
                            return {
                              ...state,
                             tipalm: action.payload
                            }
                    case "GET_UNIDAD":
                      return {
                        ...state,
                       unidades: action.payload
                      }
                    case "GET_FAMILIA":
                      return {
                        ...state,
                       familias: action.payload
                      }
                      case "GET_SUBFAMILIA":
                        return {
                          ...state,
                         subfamilias: action.payload
                        }
                        case "GET_TAREA":
                          return {
                            ...state,
                           tareas: action.payload
                          }
                          case "GET_RUBRO":
                            return {
                              ...state,
                             rubros: action.payload
                            }
                            case "GET_EROGACION":
                              return {
                                ...state,
                               erogaciones: action.payload
                              }
                              case "ADD_ENCABCOMPRA":
                                return {
                                  ...state,
                                 encabcompra: action.payload
                                }
                                case "GET_COMPRA":
                                  return {
                                    ...state,
                                   getcompra: action.payload
                                  }
                                case "ADD_DETCOMPRA":
                                  return {
                                    ...state,
                                   detcompra: action.payload
                                  }
                                  case "ADD_ENCABAJUSTE":
                                    return {
                                      // ...state,
                                     encabajuste: action.payload
                                    }
                                    case "ADD_DETAJUSTE":
                                      return {
                                        ...state,
                                       detajuste: action.payload
                                      }
                                      case "ADD_ENCABTRAS":
                                        return {
                                          // ...state,
                                         encabtras: action.payload
                                        }
                                        case "ADD_DETTRAS":
                                          return {
                                            ...state,
                                           dettras: action.payload
                                          }
                                          case "GET_EXIST":
                                            return {
                                              ...state,
                                             getexist: action.payload
                                            }
                                            case "GET_EXISTINS":
                                              return {
                                                ...state,
                                               getexistins: action.payload
                                              }
                                            case "GET_MOVS":
                                              return {
                                                ...state,
                                               movimientos: action.payload
                                              }
      default:
        return state;
    }
  }