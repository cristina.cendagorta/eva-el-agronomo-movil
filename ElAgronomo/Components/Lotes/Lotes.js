import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView,
    Modal,
    Button,
    ActivityIndicator,
    FlatList,
  } from "react-native";
  import { useNavigation } from "@react-navigation/core";
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import empresas from "../../Components/Configuration/empresa.png"
import { getLote } from "../../Redux/actions";
import { useIsFocused } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useEffect } from "react";
import { getEstablecimiento } from "../../Redux/actions";
import { ListItem, Icon } from '@rneui/themed'

const Lotes = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  // me traigo lotes, empresas y estavlecimientos
  const empresa = useSelector((store) => store.empresa);
  const establecimientos = useSelector((store) => store.establecimiento);
  const lotes = useSelector((store) => store.lote);
  const isFocused = useIsFocused();
  useEffect(() => {
    if(isFocused){ 
      //ejecuto acciones para traer todo
    dispatch(getLote());
    dispatch(getEstablecimiento(empresa[0].id));
    setTimeout(function(){
      setLoading(true)
        }, 1500);
  } 
  }, [,isFocused]);
  console.log("se ejecuta?",establecimientos)
  const [isLoading, setLoading] = useState(false);



  {if(isLoading === false || lotes === null){      // loading
    return(
  <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
  <ActivityIndicator size="large" color="#007d3c" />
</View>
)
}
} 
if(isLoading === true && lotes!= null && lotes.length === 0  ){ // si no hay lotes agregados
      return(
        <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
    
    <Image
    source={empresas}
    resizeMode= "contain"
    style={{
        display:'flex',
        marginTop:  hp('-8%'),
      height: hp('70%') ,
      width: wp('80%') ,
      alignSelf: "center",
    }}
  >
  </Image>
  
    <View style={{alignItems:"center"}}>
    <Text style={{
      color:"#182E44",
      fontSize:hp("2.6%"),
      fontWeight:"bold",
      marginTop:hp("-15%")}}>
      Agrega toda la información de tus Lotes
    </Text>
  </View>
  <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
        <TouchableOpacity onPress={() => navigation.navigate("AddLote2",establecimientos)} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
          <View style={{ alignItems: 'center'}}>
            <Text
            
              style={{  
                fontSize: hp("2.8%"),
                color:"white",
                marginTop:hp("2%")                       
                
              }}>
              + Agregar
            </Text>
          </View>
        </TouchableOpacity>
        </View>
  </SafeAreaView>


  )

}

if(isLoading === true && lotes.length > 0 ){ // si hay lotes agregados

  return(
    <SafeAreaView style={{marginTop:30}}>
    <FlatList
           data={establecimientos} // muestro establecimientos
           keyExtractor={ (item) => item.id }
           renderItem={({item, index}) =>{
             return (
               <TouchableOpacity
                 onPress={() => 
                   navigation.navigate('LotesxEst', item.id) // y me voy hacia los lotes pertenecientes a ese establecimientos
               }>
               <ListItem>
               <Icon name='business' />
               
         <ListItem.Content>
           <ListItem.Title>{item.nombre}</ListItem.Title>
           <ListItem.Subtitle>{item.abreviatura}</ListItem.Subtitle>
         </ListItem.Content>
         <ListItem.Chevron color="black" />
       </ListItem>
             </TouchableOpacity>
             )
           }}
           // <ListItemEst item ={item} />}
           ItemSeparatorComponent= { () =>       <View style={{ marginBottom: 10, borderColor:'#00000020', borderWidth:0.5}}></View>}
           ListHeaderComponent = { () => <Text style={{fontWeight:"bold", fontSize:18,marginBottom:20, textAlign:"center"}}>Elige el establecimiento de tu lote</Text>}
           // <ListItem item = {item} />}
               />
                 <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
        <TouchableOpacity onPress={() => navigation.navigate("AddLote2")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
          <View style={{ alignItems: 'center'}}>
            <Text
            
              style={{  
                fontSize: hp("2.8%"),
                color:"white",
                marginTop:hp("2%")                       
                
              }}>
              + Agregar
            </Text>
          </View>
        </TouchableOpacity>
        </View>
   </SafeAreaView>
)


}
};

export default Lotes;