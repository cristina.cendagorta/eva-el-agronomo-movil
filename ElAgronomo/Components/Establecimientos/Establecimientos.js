import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView,
    Modal,
    Button,
    ActivityIndicator,
    FlatList,
  } from "react-native";
  import React, { useState, useEffect, useRef,useMemo } from "react";
  import { useNavigation } from "@react-navigation/core";
  import { useDispatch, useSelector } from "react-redux";
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import empresa from "../../Components/Configuration/empresa.png"
 import { useIsFocused } from "@react-navigation/native";
 import { getEstablecimiento } from "../../Redux/actions";
// import Icon from 'react-native-vector-icons/Ionicons'
import { ListItem, Icon } from '@rneui/themed'
import { getTipEst } from "../../Redux/actions";
import { getZonas } from "../../Redux/actions";
  

const Establecimientos = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  // ME TRAIGO EMPRESA, Y ESTABLECIMIENTOS
  const empresa1 = useSelector((store) => store.empresa);
  const establecimientos = useSelector((store) => store.establecimiento);
  const isFocused = useIsFocused();
  useEffect(() => {
    if(isFocused){ 
      // EJECUTO LA ACCION, PARA TRAER, ZONAS, TIPO DE ESTABLECIMIENTOS Y LOS ESTABLECIMIENTOS PERTENECIENTES AL ID DE LA EMPRESA
      dispatch(getZonas())
      dispatch(getTipEst())
    dispatch(getEstablecimiento(empresa1[0].id));
    setTimeout(function(){ 
      setLoading(true) 
        }, 1000);
  } 
  }, [,isFocused]);
  // console.log("se ejecuta?",establecimientos)
  const [isLoading, setLoading] = useState(false);





      console.log("empresa",empresa1)

      
// SI EL CARGANDO ESTA EN FALSE, O ESTABLECIMIENTOS EN NULL, MUESTRO EL CARGANDO
  {if(isLoading === false || establecimientos === null){     
    return(
  <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
  <ActivityIndicator size="large" color="#007d3c" />
</View>
)
}
} // SI EL CARGANDO ESTA EN TRUE, PERO HAY ESTABLECIMIENTOS MUESTRO IMAGEN PRINCIPAL
if(establecimientos != null && isLoading === true){
  if(establecimientos.length === 0 ){
  
  return(
    <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>

<Image
source={empresa}
resizeMode= "contain"
style={{
    display:'flex',
    marginTop:  hp('-8%'),
  height: hp('70%') ,
  width: wp('80%') ,
  alignSelf: "center",
}}
>
</Image>

<View style={{alignItems:"center"}}>
<Text style={{
  color:"#182E44",
  fontSize:hp("2.6%"),
  fontWeight:"bold",
  marginTop:hp("-15%")}}>
  Agrega toda la información de tu establecimiento
</Text>
</View>
<View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
    <TouchableOpacity onPress={() => navigation.navigate("AddEstablecimiento")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
      <View style={{ alignItems: 'center'}}>
        <Text
        
          style={{  
            fontSize: hp("2.8%"),
            color:"white",
            marginTop:hp("2%")                       
            
          }}>
          + Agregar
        </Text>
      </View>
    </TouchableOpacity>
    </View>
</SafeAreaView>

)
}
} // SI HAY UN ESTABLECIMIENTO Y DEJO DE CARGAR, MUESTRO TODOS LOS ESTABLECIMIENTOS
if(establecimientos.length > 0 && isLoading === true){
  return(

    <SafeAreaView style={{marginTop:30}}>
 <FlatList // MUESTRO UNA LISTA DE DATOS
        data={establecimientos} // LA INFO VIENE DESDE ESTABLECIMIENTOS
        keyExtractor={ (item) => item.id }
        renderItem={({item, index}) =>{
          return (
            <TouchableOpacity
              onPress={() => 
                navigation.navigate('EstDetail', item)
            }>
            <ListItem>
            <Icon name='business' />
             {/* ACA DIGO, QUE VOY A MOSTRAR */}
      <ListItem.Content>
        <ListItem.Title>{item.nombre}</ListItem.Title>
        <ListItem.Subtitle>{item.abreviatura}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron color="black" />
    </ListItem>
          </TouchableOpacity>
          )
        }}
        // <ListItemEst item ={item} />}
        ItemSeparatorComponent= { () =>       <View style={{ marginBottom: 10, borderColor:'#00000020', borderWidth:0.5}}></View>}
        ListHeaderComponent = { () => <Text style={{fontWeight:"bold", fontSize:18,marginBottom:10, marginLeft:10}}>Establecimientos</Text>}
        // <ListItem item = {item} />}
            />
            <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
    <TouchableOpacity onPress={() => navigation.navigate("AddEstablecimiento")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
      <View style={{ alignItems: 'center'}}>
        <Text
        
          style={{  
            fontSize: hp("2.8%"),
            color:"white",
            marginTop:hp("2%")                       
            
          }}>
          + Agregar
        </Text>
      </View>
    </TouchableOpacity>
    </View>
</SafeAreaView>

//       <View style={{ fontSize:10}}>
//  <DataTable
//     // data={[ 
//     //     { name: 'Muhammad Rafeh', age: 21, gender: 'male' },
//     //     { name: 'Muhammad Akif', age: 22, gender: 'male' },
//     //     { name: 'Muhammad Umar', age: 21, gender: 'male' },
//     //     { name: 'Amna Shakeel', age: 22, gender: 'female' },
//     //     { name: 'Muhammad Ammar', age: 20, gender: 'male' },
//     //     { name: 'Muhammad Moiz', age: 13, gender: 'male' }
//     // ]} // list of objects
// style={{fontSize:22}}
//     data={establecimientos.map((n)=> n)}
//     colNames={['nombre', 'abreviatura', 'direccion', 'pais', 'Edición']} //List of Strings
//     colSettings={[{ name: 'nombre', type: COL_TYPES.STRING }, { name: 'abreviatura', type: COL_TYPES.STRING }, {name: 'direccion', type: COL_TYPES.STRING},{name: 'pais', type: COL_TYPES.STRING},{name: 'Edición', type: COL_TYPES.CHECK_BOX}]}//List of Objects
//     noOfPages={3} //number
//     backgroundColor={'lightgrey'} //Table Background Color
//     onRowSelect={(row) => {console.log('ROW => ',row)}}
    
// />
//       </View>
   
// //     <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
// //           <View style={{alignItems:"center", marginTop:hp("20%")}}>
// //     <Text style={{
// //       color:"#182E44",
// //       fontSize:hp("4%"),
// //       fontWeight:"bold",
// //       marginBottom:hp("5%")
// //       }}>
// //     Nombre: {establecimientos[0].nombre} 
// //     </Text>
// //   </View>
// //   <View style={{alignItems:"center"}}>
// //     <Text style={{
// //       color:"#182E44",
// //       fontSize:hp("4%"),
// //       fontWeight:"bold",
// //       marginBottom:hp("5%")
// //       }}>
// //      Abreviatura:{establecimientos[0].abreviatura} 
// //     </Text>
// //   </View>

// //   <View style={{alignItems:"center"}}>
// //     <Text style={{
// //       color:"#182E44",
// //       fontSize:hp("4%"),
// //       fontWeight:"bold",
// //       marginBottom:hp("5%")
// //       }}>
// //      Pais:{establecimientos[0].pais} 
// //     </Text>
// //   </View>

// //   <View style={{alignItems:"center"}}>
// //     <Text style={{
// //       color:"#182E44",
// //       fontSize:hp("4%"),
// //       fontWeight:"bold",
// //       marginBottom:hp("5%")
// //       }}>
// //      Latitud y Longitud:{establecimientos[0].latitud}°,{establecimientos[0].longitud}°
// //     </Text>
// //   </View>

// // </SafeAreaView>
)

};
}


const styles = StyleSheet.create({
  container: {
    margin: 4,
    padding: 30
  }
})
export default Establecimientos;