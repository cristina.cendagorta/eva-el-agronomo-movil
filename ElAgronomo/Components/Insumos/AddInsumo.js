import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as ImagePicker from "expo-image-picker";
import { Ionicons } from "@expo/vector-icons";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  Modal,
  Button,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import DatePicker from 'react-native-datepicker'
import { LogBox } from 'react-native';
import CountryPicker from 'react-native-country-picker-modal'
import Select2 from "react-native-select-two"
// import HeaderBar from "../Utils/HeaderBar";
// import SimpleModal1 from "./../Alerts/SingUp/SimpleModalok";
// import SimpleModal2 from "../Alerts/SingUp/SimpleModalok2.js";
// import SimpleModal3 from "../Alerts/SingUp//SimpleModalname.js";
// import SimpleModal4 from "../Alerts/SingUp//SimpleModalLastname.js";
// import SimpleModal5 from "../Alerts/SingUp//SimpleModalmail.js";
// import SimpleModal6 from "../Alerts/SingUp//SimpleModalpass.js";
// import SimpleModal7 from "../Alerts/SingUp//SimpleModalphone.js";
// import SimpleModal8 from "../Alerts/SingUp//SimpleModalterms.js";
// import SimpleModal9 from "../Alerts/SingUp//SimpleModalrole.js";
// import SimpleModal40 from "../Alerts/SingUp//SimpleModalsamemail.js"; 


// import { ModalPicker } from "./ModalPicker";
import { useDispatch, useSelector } from "react-redux";
import { useIsFocused } from "@react-navigation/native";

import { getUnidad } from "../../Redux/actions";
import { getFamilia } from "../../Redux/actions";
import { getSubfamilia } from "../../Redux/actions";
import { getTarea } from "../../Redux/actions";
import { getRubro } from "../../Redux/actions";
import { getErogacion } from "../../Redux/actions";

const AddInsumo = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  // me traigo todo lo que necesito para agregar insumos, desde el back
  const unidades = useSelector((store) => store.unidades);
  const familias = useSelector((store) => store.familias);
  const subfamilias = useSelector((store) => store.subfamilias);
  const tareas = useSelector((store) => store.tareas);
  const rubros = useSelector((store) => store.rubros);
  const erogaciones = useSelector((store) => store.erogaciones);
  const isFocused = useIsFocused();

  useEffect(() => {
    if(isFocused){ 
      //ejecuto acciones para traer selects desde el back
    dispatch(getUnidad());
    dispatch(getFamilia());
    dispatch(getSubfamilia());
    dispatch(getTarea());
    dispatch(getRubro());
    dispatch(getErogacion());
  } 
  }, [,isFocused]);
  const [isLoading, setLoading] = useState(false);
  setTimeout(function(){
setLoading(true)
  }, 1500);
  // const respuesta = useSelector((store) => store.responseReg)

  //mapeo para poder usar la info en los selects

  var unidadess = unidades?.map( item => { 
    return { name: item.detalle_unidad, id:item.id }; 
  });
  var familiass = familias?.map( item => { 
    return { name: item.detalle_familia, id:item.id }; 
  });
  var subfamiliass = subfamilias?.map( item => { 
    return { name: item.detalle_subfamilia, id:item.id }; 
  });
  var tareass = tareas?.map( item => { 
    return { name: item.detalle_tarea, id:item.id }; 
  });
  var rubross = rubros?.map( item => { 
    return { name: item.detalle_rubro_insumo, id:item.id }; 
  });
  var erogacioness = erogaciones?.map( item => { 
    return { name: item.nombre_tipo_erogacion, id:item.id }; 
  });



  useEffect(() => {
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
}, [])



  // ALERTAS:

  // REGISTRO USUARIO OK
//   const [isModalVisible1, setisModalVisible1] = useState(false);
//   const [chooseData1, setchooseData1] = useState();

//   const changeModalVisible1 = (bool) => {
//     setisModalVisible1(bool);
//   };

//   const setData1 = (data) => {
//     setchooseData1(data);
//   };

//   // REGISTRO TRANSPORTISTA OK
//   const [isModalVisible2, setisModalVisible2] = useState(false);
//   const [chooseData2, setchooseData2] = useState();

//   const changeModalVisible2 = (bool) => {
//     setisModalVisible2(bool);
//   };

//   const setData2 = (data) => {
//     setchooseData2(data);
//   };


//   // NOMBRE MAL INGRESADO
//   const [isModalVisible3, setisModalVisible3] = useState(false);
//   const [chooseData3, setchooseData3] = useState();

//   const changeModalVisible3 = (bool) => {
//     setisModalVisible3(bool);
//   };

//   const setData3 = (data) => {
//     setchooseData3(data);
//   };

//   // APELLIDO MAL INGRESADO

//   const [isModalVisible4, setisModalVisible4] = useState(false);
//   const [chooseData4, setchooseData4] = useState();

//   const changeModalVisible4 = (bool) => {
//     setisModalVisible4(bool);
//   };

//   const setData4 = (data) => {
//     setchooseData4(data);
//   };

//   //MAIL MAL INGRESADO
//   const [isModalVisible5, setisModalVisible5] = useState(false);
//   const [chooseData5, setchooseData5] = useState();

//   const changeModalVisible5 = (bool) => {
//     setisModalVisible5(bool);
//   };

//   const setData5 = (data) => {
//     setchooseData5(data);
//   };

//   // CONTRASEÑA MAL INGRESADA

//   const [isModalVisible6, setisModalVisible6] = useState(false);
//   const [chooseData6, setchooseData6] = useState();

//   const changeModalVisible6 = (bool) => {
//     setisModalVisible6(bool);
//   };

//   const setData6 = (data) => {
//     setchooseData6(data);
//   };
//   // TELEFONO MAL INGRESADO

//   const [isModalVisible7, setisModalVisible7] = useState(false);
//   const [chooseData7, setchooseData7] = useState();

//   const changeModalVisible7 = (bool) => {
//     setisModalVisible7(bool);
//   };

//   const setData7 = (data) => {
//     setchooseData7(data);
//   };
//   // NO ACEPTA TERMINOS
//   const [isModalVisible8, setisModalVisible8] = useState(false);
//   const [chooseData8, setchooseData8] = useState();

//   const changeModalVisible8 = (bool) => {
//     setisModalVisible8(bool);
//   };

//   const setData8 = (data) => {
//     setchooseData8(data);
//   };

//   // NO ELIJE ROL
//   const [isModalVisible9, setisModalVisible9] = useState(false);
//   const [chooseData9, setchooseData9] = useState();

//   const changeModalVisible9 = (bool) => {
//     setisModalVisible9(bool);
//   };

//   const setData9 = (data) => {
//     setchooseData9(data);
//   };

//   // MAIL REPETIDO
//   const [isModalVisible40, setisModalVisible40] = useState(false);
//   const [chooseData40, setchooseData40] = useState();

//   const changeModalVisible40 = (bool) => {
//     setisModalVisible40(bool);
//   };

//   const setData40 = (data) => {
//     setchooseData40(data);
//   };

  


  // useEffect(()=>{
  //   //console.log('aqui esta la respuestaaaa:',respuesta);
  //   if(respuesta?.role === true){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileUser");
  //     changeModalVisible1(true)
  //   }if(respuesta?.role === false){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileCarrier");
  //     changeModalVisible2(true)
  //   }if(respuesta?.role === 1){
  //     // alert('El mail ingresado ya se encuentra en uso!')
  //     changeModalVisible40(true)
  //   }
  // },[respuesta]);

//genero estados locales


  const [reg, setReg] = useState({
    nombre: "",
    abre: "",
    cod: "",
    uni: "",
    fam: "",
    sub: "",
    tar: "",
    rub: "",
    ero: "",
  });
  const [place, setPlace] = useState({
    uni: "Unidad",
    fam: "Familia",
    sub: "Subfamilia",
    tar: "Tarea",
    rub: "Rubro",
    ero: "Tipo de Erogación",
  });

  console.log("place",place)


  const [check, setCheck] = useState(false);

  const CheckboxChange = (e) => {
    setCheck(!check);
  };

  // const ChangeInput = (e) => {
  //   setReg({
  //     // y sino es  generos y platforms, directamente pongo lo que escribo en el input
  //     ...reg,
  //     [e.target.name]: e.target.value,
  //   });
  // };

  const handleSubmit = (e) => {
    e.preventDefault();
    // en un objeto pongo lo que tengo en el estado inicial
    // let rolex = undefined;
    // if (chooseData === "◉ Usuario") {
    //   rolex = true;
    // } if(chooseData === "◉ Transportista") {
    //   rolex = false;
    // }
    const obj = { // genero objeto para crear
        nombre: reg.nombre,
        abreviatura: reg.abre,
        codigo_externo: reg.cod,
        unidad_id: Number(reg.uni),
        familia_id: Number(reg.fam),
        subfamilia_id: Number(reg.sub),
        tarea_id: Number(reg.tar),
        rubro_insumo_id: Number(reg.rub),
        tipo_erogacion_id: Number(reg.ero)
        
    };

    // validaciones 
//     if (!obj.name ) {
//       changeModalVisible3(true)
//       return
//   }
//   if (!obj.lastName) {
//       // alert("Por favor escribe el Apellido correctamente!")
//       changeModalVisible4(true)
//       return
//   }
//   if (!obj.eMail.includes('.com') || !obj.eMail.includes('@')  ) {
//     changeModalVisible5(true)
//     // alert("Por favor escribe un correo electrónico válido!")
//     return
// } if (!obj.password) {
//   changeModalVisible6(true)
//   // alert("Por favor escribe una Contraseña válida!")
//   return
// }
// if (!obj.phone) {
//   changeModalVisible7(true)
//       // alert("Por favor escribe un número de telefono válido!")
//       return
//   }
//   if (!obj.business) {
//     changeModalVisible8(true)
//     // alert("Debes aceptar los términos para poder registrarte!")
//     return
//   }if (!obj.secret) {
//     changeModalVisible9(true)
//   // alert("Por favor elije un Rol!")
//   return
// }



// dispatch(adminregister(obj));
 console.log("Estoy enviado", obj);

navigation.navigate("AddInsumo2",obj) // me voy a el otro componente y le paso el objeto
setReg({
  nombre: "",
  abre: "",
  cod: "",
  uni: "",
  fam: "",
  sub: "",
  tar: "",
  rub: "",
  ero: "",

});

setPlace({
  uni: "Unidad",
  fam: "Familia",
  sub: "Subfamilia",
  tar: "Tarea",
  rub: "Rubro",
  ero: "Tipo de Erogación",
});

};

  //funciones para cambiar e.value de los inputs

  const handelChangeNombre = (name) => {
    setReg({
      ...reg,
      nombre: name,
    });
  };
  const handelChangeAbre = (name) => {
    setReg({
      ...reg,
      abre: name,
    });
  };
  const handelChangeCod = (name) => {
    setReg({
      ...reg,
      cod: name,
    });
  };
  const handelChangeUni = (name) => {
    console.log(name)
    setReg({
      ...reg,
      uni: name,
    });

    const filtrado = unidades?.filter((f) => f.id === name)
    console.log("filtra",filtrado)
    setPlace({
      ...place,
      uni: filtrado[0].detalle_unidad,
    });
  };
  const handelChangeFam = (name) => {
    setReg({
      ...reg,
      fam: name,
    });

    const filtrado2 = familias?.filter((f) => f.id === name)
    setPlace({
      ...place,
      fam: filtrado2[0].detalle_familia,
    });
  };
  const handelChangeSub = (name) => {
    setReg({
      ...reg,
      sub: name,
    });

    const filtrado3 = subfamilias?.filter((f) => f.id === name)
    setPlace({
      ...place,
      sub: filtrado3[0].detalle_subfamilia,
    });
  };
  const handelChangeTar = (name) => {
    setReg({
      ...reg,
      tar: name,
    });

    const filtrado4 = tareas?.filter((f) => f.id === name)
    setPlace({
      ...place,
      tar: filtrado4[0].detalle_tarea,
    });
  };
  const handelChangeRub = (name) => {
    setReg({
      ...reg,
      rub: name,
    });

    const filtrado5 = rubros?.filter((f) => f.id === name)
    setPlace({
      ...place,
      rub: filtrado5[0].detalle_rubro_insumo,
    });
  };
  const handelChangeEro = (name) => {
    setReg({
      ...reg,
      ero: name,
    });

    const filtrado6 = erogaciones?.filter((f) => f.id === name)
    setPlace({
      ...place,
     ero: filtrado6[0].nombre_tipo_erogacion,
    });
  };
  
   ////--> IMAGE PICKER <-- ////
   const [selectedImage, setSelectedImage] = useState(null);

   let openImagePickerAsync = async () => {
     let permissionResult =
       await ImagePicker.requestMediaLibraryPermissionsAsync();
 
     if (permissionResult.granted === false) {
       alert("Se requiere el permiso para acceder a la cámara");
       return;
     }
 
     //Si es true va a venir a pickerResult
     const pickerResult = await ImagePicker.launchImageLibraryAsync({
       mediaTypes: ImagePicker.MediaTypeOptions.Images,
       allowsEditing: true,
       aspect: [4, 3],
       quality: 1,
     });
 
     if (pickerResult.cancelled !== true) {
       let newFile = {
         uri: pickerResult.uri,
         type: `logi/${pickerResult.uri.split(".")[1]}`,
         name: `logi.${pickerResult.uri.split(".")[1]}`,
       };
       handleUpload(newFile);
     }
   };
 
   const handleUpload = (image) => {
     const data = new FormData();
     data.append("file", image);
     data.append("upload_preset", "logiexpress");
     data.append("cloud_name", "elialvarez");
 
     fetch("https://api.cloudinary.com/v1_1/elialvarez/image/upload", {
       method: "post",
       body: data,
     })
       .then((res) => res.json())
       .then((data) => {
         //console.log(data)
         setSelectedImage(data.url);
       });
   };

{ if (isLoading === false || unidades === null){
  return (
  <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
  <ActivityIndicator size="large" color="#007d3c" />
</View>
)
}
}

{ if(isLoading === true && unidades !== null){


  return (
    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : null}
    keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
    <View style={{ flex: 1,  backgroundColor: 'white'  }}>
      {/* <View style={{marginTop:hp("-2%"),marginLeft:wp("0%"),marginBottom:hp("0%")}}>
        <HeaderBar  screen={'null'} style={{color:"white"}}/>
        </View> */}
    <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffffff" }}
      showsVerticalScrollIndicator={false}
    >
      
      {/* Brand View */}
      {/* <ImageBackground
        source={require("../Home/logo.png")}
        resizeMode= "contain"
        style={{
            display:'flex',
            marginTop:  hp('-18%'),
          height: hp('60%') ,
          width: wp('110%') ,
          alignSelf: "center",
        }}
      >
      </ImageBackground> */}
      {/* Botton View */}
      <View style={styles.bottonView}>
        {/* Welcome View */}
        <View style={{ padding: 40, display: "flex", alignItems: "center" }}>
          <Text style={{ color: "#151f27", fontSize: hp("4.7%"),fontWeight: '600', marginTop: hp("3%") }}>
            Datos de tu Insumo
          </Text>
        </View>
        {/* inputs */}
        <View style={styles.FormView}>
          <TextInput
            name="nombre"
            value={reg.nombre}
            onChangeText={(name) => handelChangeNombre(name)}
            placeholder="Nombre*"
            style={styles.TextInput}
            autoCapitalize='characters'
          ></TextInput>
    
  
          <TextInput
            value={reg.abre}
            onChangeText={(name) => handelChangeAbre(name)}
            name="abreviatura"
            placeholder="Abreviatura"
            style={styles.TextInput}
            autoCapitalize='characters'
            
          ></TextInput>
              <TextInput
            value={reg.cod}
            onChangeText={(name) => handelChangeCod(name)}
            name="codigoexterno"
            placeholder="Código Externo"
            style={styles.TextInput}
            autoCapitalize='characters'
            
          ></TextInput>
          
                   <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Seleccionar Unidad"
          title={place.uni}
          data={unidadess != null ? unidadess : null}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeUni(name[0])}
        />
             <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Seleccionar Familia"
          title={place.fam}
          data={familiass}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeFam(name[0])}
        />
                    <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Seleccionar Subfamilia"
          title={place.sub}
          data={subfamiliass}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeSub(name[0])}
        />
                            <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Seleccionar Tarea"
          title={place.tar}
          data={tareass}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeTar(name[0])}
        />
                            <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Seleccionar Rubro"
          title={place.rub}
          data={rubross}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeRub(name[0])}
        />
                                   <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Seleccionar Tipo de Erogación"
          title={place.ero}
          data={erogacioness}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeEro(name[0])}
        />
        

{/* <View style={{width:100, height:30}}> */}
 
      {/* </View> */}
        
          
          <TouchableOpacity style={styles.Button} onPress={handleSubmit} >
            <Text style={styles.ButtonText} >
              Siguiente
            </Text>
            {/* <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible1}
                  nRequestClose={() => changeModalVisible1(false)}
                >
                  <SimpleModal1
                    changeModalVisible1={changeModalVisible1}
                    setData1={setData1}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible2}
                  nRequestClose={() => changeModalVisible2(false)}
                >
                  <SimpleModal2
                    changeModalVisible2={changeModalVisible2}
                    setData2={setData2}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible3}
                  nRequestClose={() => changeModalVisible3(false)}
                >
                  <SimpleModal3
                    changeModalVisible3={changeModalVisible3}
                    setData3={setData3}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible4}
                  nRequestClose={() => changeModalVisible4(false)}
                >
                  <SimpleModal4
                    changeModalVisible4={changeModalVisible4}
                    setData4={setData4}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible5}
                  nRequestClose={() => changeModalVisible5(false)}
                >
                  <SimpleModal5
                    changeModalVisible5={changeModalVisible5}
                    setData5={setData5}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible6}
                  nRequestClose={() => changeModalVisible6(false)}
                >
                  <SimpleModal6
                    changeModalVisible6={changeModalVisible6}
                    setData6={setData6}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible7}
                  nRequestClose={() => changeModalVisible7(false)}
                >
                  <SimpleModal7
                    changeModalVisible7={changeModalVisible7}
                    setData7={setData7}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible8}
                  nRequestClose={() => changeModalVisible8(false)}
                >
                  <SimpleModal8
                    changeModalVisible8={changeModalVisible8}
                    setData8={setData8}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible9}
                  nRequestClose={() => changeModalVisible9(false)}
                >
                  <SimpleModal9
                    changeModalVisible9={changeModalVisible9}
                    setData9={setData9}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible40}
                  nRequestClose={() => changeModalVisible40(false)}
                >
                  <SimpleModal40
                    changeModalVisible40={changeModalVisible40}
                    setData40={setData40}
                  />
                  
                  </Modal> */}
          </TouchableOpacity>
          
         
        </View>

        
      </View>
    </ScrollView>
    </View>
    </KeyboardAvoidingView>
  );
}
}
};

export default AddInsumo;

const styles = StyleSheet.create({
  brandView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  brandViewText: {
    color: "#FFC107",
    fontSize: 45,
    fontWeight: "bold",
    textTransform: "uppercase",
    // justifyContent:'flex-start'
  },
  bottonView: {
    flex: 1.5,
    backgroundColor: "white",
    bottom: 50,
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
  },
  FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: -20,
  },
  TextInput: {
    width: wp("90%"),
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("2.9%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  TextInput2: {
    width: wp("90%"),
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("5%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  Button: {
    width: "90%",
    color: "black",
    height: 52,
    backgroundColor: "rgb(24,116,28)",
    borderRadius: 10,
    marginTop: hp("5%"),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  ButtonText: {
    // fontWeight: "bold",
    fontSize: hp("3%"),
    color: "white",
  },
  SingUpText: {
    color: "#4632a1",
    fontSize: 20,
  },
  TextButton: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  preg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  pregunta: {
    color: "red",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    padding: 50,
    marginTop: 20,
    borderRadius: 20,
  },
  text: {
    // marginVertical: 20,
    fontSize: 22,
    color: "white",
    fontWeight: "bold",
  },
  TouchableOpacity: {
    backgroundColor: "black",
    alignSelf: "stretch",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    
  },
  checkbox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  checkboxx: {
    marginTop: 15,
  },
  imgPerfil: {
    width: hp('25%'),
    height: hp('25%'),
    borderRadius: hp("12,5%"),
    borderColor: "rgb(0,140,207)",
    borderWidth: wp('0.8%'),
    marginTop: hp('3%'),
    backgroundColor:"#e1e1e1",
  },
  imgAdd: {
    width: wp("15%"),
    height: wp("15%"),
    marginLeft: wp("35%"),
    marginTop: hp('-13%'),
    borderWidth: hp('0.5%'),
    borderColor: "rgb(189,191,198)",
    borderRadius: 50,
  },
});