import React, { useRef } from "react";
import { Button, Modal, SafeAreaView, StyleSheet, Text, View, ImageBackground, TouchableOpacity, Animated  } from "react-native";
import {createDrawerNavigator, CreateDrawerNavigator,
DrawerContentScrollView,
DrawerItemList} from "@react-navigation/drawer";
import { StatusBar } from "expo-status-bar";
import { useNavigation } from "@react-navigation/core";
import {MaterialCommunityIcons} from "@expo/vector-icons"
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as SecureStore from "expo-secure-store";
// import BottomSheet from "react-native-gesture-bottom-sheet";
import { BottomSheet } from 'react-native-sheet';
import AdmStock from "./Menu lateral/AdmStock";
import { getEmpresa } from "../../Redux/actions";
import { useDispatch, useSelector } from "react-redux";

//ESTE SERIA COMO EL COMPONENTE PRINCIPAL, ES DONDE PUEDO ELEGIR A QUE PANTALLA MOVERME
function MyModal({isVisible, onClick}){
    return (

 
        <Modal
        visible={isVisible}
        animationType="slide"
        presentationStyle="pageSheet"
        transparent={false}
        >
        <View style={{
          flex:1,
          backgroundColor:"#000000AA",
          justifyContent:"flex-end"
        }}
        >
            <View style={{
          backgroundColor:"#FFFFFF",
          width: "100%",
          borderTopRightRadius:10,
          borderTopLeftRadius:10,
          paddingHorizontal:10,
          maxHeight:100
        }}
        >
          <View style={{alignItems:"center"}}>
          <Text style={{
            color:"#182E44",
            fontSize:20,
            fontWeight:"500",
            margin:15,
            marginBottom:30}}>
            HOLA
          </Text>

        </View>
        </View> 

        </View>
       </Modal>
       
    );
}

  //FUNCION PARA GUARDAR LA INFO EN EL STORE, KEY = token , VALUE=el string del token
  async function save(key, value) {
    //FUNCION PARA GUARDAR LA INFO EN EL STORE, KEY = token , VALUE=el string del token
    try{
    await SecureStore.setItemAsync(key, value);
    } catch(error){
      console.log('error', error.response)
    }
  }

function HomeScreen({ navigation}) {
    const [showModal, setShowModal] = React.useState(false);
    const bottomSheet = useRef(null);
    const dispatch = useDispatch();
    React.useLayoutEffect(() =>{

    dispatch(getEmpresa()) // ACCIONO PARA TRAER LAS EMPRESAS
        navigation.setOptions({
          
            headerRight: () => (
           //ESTE ES LA CONFIGURACION
                <MaterialCommunityIcons
                style={{marginTop:hp("0%"), marginRight:wp("3%")}}
                name={'cog'}
                size={26}
                color={"rgb(0,102,172)"}
                onPress={() => bottomSheet.current.show()}
                />
                
            ),
        });
    }, [navigation]);

    const navegar = () =>{
      bottomSheet.current.hide()
      setTimeout(function(){
        navigation.navigate("Empresa")
      }, 500);
      
    }

    return (
        <SafeAreaView style={styles.container}>
        <BottomSheet // ESTE ES EL MODAL QUE SALE DESDE ABAJO, CON LA CONFIGURACION DE EMPRESA Y DE USUARIOSA
        borderRadius={hp("2.2%")}
        draggable={true}
        hasDraggableIcon
        ref={bottomSheet}
        height={hp("30%")}>
        <View style={{marginLeft:wp("3%"), marginBottom:hp("3%")}}>
          <Text style={{fontWeight:"bold", fontSize:23}}>Configuración</Text>
        </View>
        <View style={{marginLeft:wp("3%")}} >
        <TouchableOpacity  style={{paddingVertical: 15}} onPress={navegar}>
          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: wp("3%")}}>
            <MaterialCommunityIcons name="office-building" size={hp("3.5%")} color={"black"} />
            <Text
              style={{  
                fontSize: hp("2.7%"),
                color:"black",
                // fontFamily: 'Roboto-Medium',
                marginLeft: wp("6%"),
              }}>
              Empresa
            </Text>
          </View>
        </TouchableOpacity>
        </View>
        <View style={{height:hp("0.1%"), backgroundColor:"lightgrey", width:wp("90%"), marginLeft: wp("15%"),}}>
        </View>
        <View style={{marginLeft:wp("3%")}}>
        <TouchableOpacity onPress={() => {}} style={{paddingVertical: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: wp("3%")}}>
            <MaterialCommunityIcons name="account-multiple" size={hp("3.5%")} color={"black"} />
            <Text
              style={{  
                fontSize: hp("2.7%"),
                color:"black",
                // fontFamily: 'Roboto-Medium',
                marginLeft: wp("6%"),
              }}>
              Usuarios
            </Text>
          </View>
        </TouchableOpacity>
        </View>
      </BottomSheet>
          
         <ImageBackground 
         source={require("./home1.png")}
         resizeMode="contain"
              style={{
                marginTop:hp("-10%"),
                display:'flex',
              height: hp('90%') ,
              width: wp('90%') ,
              alignSelf: "center",
            }}
            >
            </ImageBackground>
            <ImageBackground 
         source={require("./home2.png")}
         resizeMode="contain"
              style={{
                marginTop:hp("-50%"),
                display:'flex',
              height: hp('90%') ,
              width: wp('90%') ,
              alignSelf: "center",
            }}
            >
            </ImageBackground>
         {/* <Text>Acá se encuentra el Home</Text> */}
         <StatusBar style="auto"/>
         {/* <Button
         title="next page"
         onPress={()=> navigation.navigate("Detail")} 
         ></Button> */}
         </SafeAreaView>
 
    );
}

function ProfileScreen(){
    return (
        <View style={styles.container}>
        <Text>Esta es la pagina de Perfil</Text>
        <StatusBar style="auto"/>
        </View>
    );
}

// function HelpScreen(){
//     return (
//         <View style={styles.container}>
//         <Text>Esta es la pagina de Ayuda</Text>
//         <StatusBar style="auto"/>
//         </View>
//     );
// }


//ACA ESTAN LAS DEMAS OPCIONES DEL COSTADO
function CustomDrawerContent(props) {
    const navigation = useNavigation();
    return (
        <>
        <View style={{flex:1,}}>
        <DrawerContentScrollView {...props}>
        {/* <View style={styles.drawerHeader}>
        <Text>HEADER</Text>
        </View> */}
        <View style={{flex:1}}>
        <DrawerItemList {...props} />
        </View>
        </DrawerContentScrollView>
        <View style={{marginBottom:hp("10%")}}>
        <View style={{height:hp("0.2%"), backgroundColor:"lightgrey"}}>
        </View>
        <Text style={{fontSize:hp("2.3%"), marginTop:hp("2.4%"), marginLeft:hp("2.6%"), color:"grey", fontWeight:"bold"}}>Configuración</Text>
        <TouchableOpacity onPress={()=>navigation.navigate("Establecimientos")} style={{paddingVertical: 15}} >
          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 20}} >
            <MaterialCommunityIcons name="barn" size={22} color={"black"} />
            <Text
            
              style={{  
                fontSize: 14,
                fontWeight:"bold",
                color:"black",
                // fontFamily: 'Roboto-Medium',
                marginLeft: 10,
              }}>
              Establecimientos
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {navigation.navigate("Almacenes")}} style={{paddingVertical: 5}}>
          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 20}}>
            <MaterialCommunityIcons name="silo" size={22} color={"black"} style={{marginLeft: 2}} />
            <Text
              style={{  
                fontSize: 14,
                fontWeight:"bold",
                color:"black",
                // fontFamily: 'Roboto-Medium',
                marginLeft: 8,
              }}>
              Almacenes
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {navigation.navigate("Insumos")}} style={{marginTop: hp("2%")}}>
          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 20}}>
            <MaterialCommunityIcons name="peanut-outline" size={22} color={"black"} />
            <Text
              style={{  
                fontSize: 14,
                fontWeight:"bold",
                color:"black",
                // fontFamily: 'Roboto-Medium',
                marginLeft: 10,
              }}>
              Insumos
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {navigation.navigate("Lotes")}} style={{marginTop: hp("3%")}}>
          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 20}}>
            <MaterialCommunityIcons name="sprout" size={22} color={"black"} />
            <Text
              style={{  
                fontSize: 14,
                fontWeight:"bold",
                color:"black",
                // fontFamily: 'Roboto-Medium',
                marginLeft: 10,
              }}>
              Lotes
            </Text>
          </View>
        </TouchableOpacity>
        </View>
        </View>
        <View style={{backgroundColor:"rgb(107,190,59)", height:hp("5%")}}>
        
        <TouchableOpacity
        style={{}}
        // Backgroundcolor={"#007d3c"}
        //  title="Cerrar Sesión"
         onPress={()=> {
          
         
            //  props.navigation.closeDrawer();             
             
             save("tokenn", "false");
             
              navigation.navigate("Login")
            
         }}
         >
             <Text
              style={{  
                // height:hp("5%"),
                fontSize: 14,
                fontWeight:"bold",
                color:"white",
                alignSelf:"center",
                // textAlignVertical:"center"
                // fontFamily: 'Roboto-Medium',
                marginTop: hp("1.2%"),
              }}>
              Cerrar Sesión
            </Text>
         </TouchableOpacity>
        </View>
        </>
    )
}

const DrawerStack = createDrawerNavigator();
export function DrawerScreenStack() {
    return (        
        <DrawerStack.Navigator 
        initialRouteName="Home"
        screenOptions={{
            headerTitleStyle:{
                // fontWeight: "bold",
                width:wp("65%"),
                // marginLeft:wp("2%")
                // textAlign:"center"
            },
            drawerPosition: 'left',
            drawerActiveBackgroundColor:"rgb(107,190,59)",
            drawerActiveTintColor:"white",
            drawerType: 'front', 
            headerTintColor:"rgb(0,102,172)", //violeta
            swipeEdgeWidth: 200, 
            headerShown: true, 
            overlayColor: '#00000050', 
            drawerStyle: {
              backgroundColor: '#dee0e8',
              width: wp("80%")
            },
             drawerLabelStyle:{
                marginLeft:wp("-6%"),
                fontSize:14,
                fontWeight:"bold"
            },
            swipeEnabled: true, 
          }}
        drawerContent={(props) => <CustomDrawerContent {...props} />}
        // screenOptions={{
        //     drawerActiveBackgroundColor:"#007d3c",
        //     drawerActiveTintColor:"white",
        //     drawerLabelStyle:{
        //         marginLeft:wp("-6%"),
        //         fontSize:14
        //     }
        // }}
        >
            {/* <DrawerStack.Group> */}

            {/* ACA DEFINO LOS MENUS DISPONIBLESS */}

          <DrawerStack.Screen 
          name="Home" 
          component={HomeScreen} 
          options={{
            title: 'Home',
            drawerIcon: ({focused, size}) => (
                <MaterialCommunityIcons
                name={'home'}
                size={23}
                color={"black"}
                />
            ),
         }}
         
          />
          <DrawerStack.Screen name="Administración de Stock" component={AdmStock} 
           options={{
            title: 'Administración de Stock',
            drawerIcon: ({focused, size}) => (
                <MaterialCommunityIcons
                name={'chart-bar'}
                size={23}
                color={"black"}
                />
            ),
         }}
         />
          <DrawerStack.Screen name="Planificación" component={ProfileScreen} 
          style={{}}
              options={{
                title: 'Planificación y Presupuestos',
                drawerIcon: ({focused, size}) => (
                    <MaterialCommunityIcons
                    name={'credit-card'}
                    size={23}
                    color={"black"}
                    />
                ),
             }}
          />
          <DrawerStack.Screen name="Modelo Agrícola" component={AdmStock} 
              options={{
                title: 'Órdenes de Trabajo',
                drawerIcon: ({focused, size}) => (
                    <MaterialCommunityIcons
                    name={'tractor'}
                    size={23}
                    color={"black"}
                    />
                ),
             }}
          />       
                {/* </DrawerStack.Group> */}
        </DrawerStack.Navigator>
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"#fff",
        alignItems:"center",
        justifyContent:"center",
    },
    "modal-container": {
        flex:1,
        alignItems:"center",
        borderRadius:18,
        height:hp("50%")
    },
    drawerHeader:{
        height:100,
        backgroundColor:"#F1F1F1",
        margin:10,
        marginTop:0,
        marginBottom:8,
        borderRadius:8,
    }
});