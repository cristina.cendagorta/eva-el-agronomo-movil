import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import MapView, {
  MAP_TYPES,
  Polygon,
  ProviderPropType,
  Marker,
} from 'react-native-maps';
import {MaterialCommunityIcons} from "@expo/vector-icons"
const { width, height } = Dimensions.get('window');
import { newAlmacen } from '../../Redux/actions';
import { connect } from "react-redux";
const ASPECT_RATIO = width / height;
const LATITUDE = -34.61315;
const LONGITUDE = -58.37723;
const LATITUDE_DELTA = 0.0922;
const anchor = { x: 0.02, y: 0.02 }
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;

class AlmMap extends React.Component {
  constructor(props) {
    super(props);
//ESTADO INICIAL
    this.state = {
      showsUserLocation: true,
      followsUserLocation : true,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      editar:false,
      polygons: [],
      polygon:{
        id:1,

      },
      editing: null,
      creatingHole: false,
    };
    console.log("editing",this.props);
    
  }

  

  finish2() {


    const obj = this.props.route.params
 
    obj.geoposicion = JSON.stringify(this.state.editing.coordinates) //DEMTRO DEL OBJ QUE YA COMPLETE ANTES, AGREGO LAS COORDENADAS COMO UN STRING
    console.log("objjj",obj)
    this.props.newAlmacen(obj); // HAGO EL POST DE CREAR ALMACEN
    
    this.props.navigation.navigate("Almacenes") 

    
  }

  createHole() {
    const { editing, creatingHole } = this.state;
    if (!creatingHole) {
      this.setState({
        creatingHole: true,
        editing: {
          ...editing,
          holes: [...editing.holes, []],
        },
      });
    } else {
      const holes = [...editing.holes];
      if (holes[holes.length - 1].length === 0) {
        holes.pop();
        this.setState({
          editing: {
            ...editing,
            holes,
          },
        });
      }
      this.setState({ creatingHole: false });
    }
  }
  
  onPress(e) {
    if(this.state.editar === true && this.state.editing === null){ //CONDICION DE SI APRETAR EL EDITAR
    const { editing, creatingHole } = this.state;
    if (!editing) {
      this.setState({
        editing: {
          coordinates: [e.nativeEvent.coordinate],
        },
      });
    } else if (!creatingHole) {
      this.setState({
        editing: {
          ...editing,
          coordinates: [...editing.coordinates, e.nativeEvent.coordinate],
        },
      });
    } else {
      const holes = [...editing.holes];
      holes[holes.length - 1] = [
        ...holes[holes.length - 1],
        e.nativeEvent.coordinate,
      ];
      this.setState({
        editing: {
          ...editing,
          coordinates: [...editing.coordinates],
        },
      });
    }
  }
  }
  

  render() {
    const mapOptions = {
      scrollEnabled: true,
    };

    if (this.state.editing) {
      mapOptions.scrollEnabled = false;
      mapOptions.onPanDrag = e => this.onPress(e);
    }

    return (
      <View style={styles.container}>
        
        
        <MapView
          showsUserLocation={this.state.showsUserLocation}
          followsUserLocation={this.state.followsUserLocation}
          provider={this.props.provider}
          style={styles.map}
          mapType={MAP_TYPES.HYBRID}
          initialRegion={this.state.region}
          onPress={e => this.onPress(e)}
          {...mapOptions}
        >
          {this.state.polygons.map(polygon => (
           // MUESTRO LOS POLIGONOS
            <Polygon
              key={this.state.polygon.id++}
              coordinates={polygon.coordinates}
              holes={polygon.holes}
              strokeColor="#F00"
              fillColor="rgba(255,0,0,0.5)"
              strokeWidth={1}
            />
          ))}
          {this.state.editing && (
     
            <Polygon
              key={this.state.polygon.id++}
              coordinates={this.state.editing.coordinates}
              holes={this.state.editing.holes}
              strokeColor="rgba(121,170,242,255)"
              fillColor="rgba(121,170,242,0.4)"
              strokeWidth={hp("0.5%")}
            />
          )}
          {
          this.state.editar === true &&  this.state.editing != null?
        //  console.log("hola", this.state.editing)
           this.state.editing.coordinates.map(polygon => (
       <Marker
       coordinate={polygon}
       pinColor='purple'
      //  id={polygon.id}
      //  centerOffset={{ x: -58, y: 100 }}
    // anchor={{ x: 0.5, y: 0.5 }}
       >
       {/* <Image
       source={require('./pin.png')}
       style={{width: hp("3%"), height: hp("3%")}}
       resizeMode="cover"
     /> */}
     
      </Marker>
       

)
)
: null
}
        </MapView>
        <View style={{height:hp("8%"), backgroundColor:"white", marginBottom:hp("60%"), width:wp("90%"), borderRadius:hp("1%"), flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
        <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='map-marker' size={hp("3.5%")} style={{marginRight:wp("3%")}}/>
                    <Text style={{color:"black", fontWeight:"bold", }}>
                Marcá el Almacén a agregar
              </Text>

        </View>
        {this.state.editing != null ?
              <View style={{height:hp("8%"), backgroundColor:"rgba(255,255,255,0.85)", marginTop:hp("-8%"), width:wp("90%"), borderRadius:hp("1%"), flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
              <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='map-marker' size={hp("3.5%")} style={{marginRight:wp("3%")}}/>
                          <Text style={{color:"black", fontWeight:"bold", }}>
                     Latitud: {(this.state.editing.coordinates[0].latitude).toFixed(4)}, Longitud: {(this.state.editing.coordinates[0].longitude).toFixed(4)}
                    </Text>
      
              </View>
              : null
        
      }
  
        <View style={styles.buttonContainer}>
          {
          
          this.state.editar === false
          
          ?
          
            <TouchableOpacity
              onPress={() => this.setState({editar:true})}
              style={[styles.bubble, styles.button]}
            >

              <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='map-marker' size={hp("3.5%")}/>
                    <Text style={{color:"white", fontWeight:"bold", }}>
                Marcar
              </Text>
       
            </TouchableOpacity>
             :       <TouchableOpacity
                    onPress={() => this.setState({editing:null,editar:false,})}
                    style={[styles.bubble, styles.button]}
                  >
                    <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='delete' size={hp("3.5%")}/>
                    <Text style={{color:"white", fontWeight:"bold", }}>
                    
                      Limpiar
                    </Text>
                  </TouchableOpacity>

                  
          }
          {this.state.editar === true &&  (
            
            <TouchableOpacity
            onPress={() => this.finish2()}
            style={[styles.bubble, styles.button]}
          >
            <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='check-decagram' size={hp("3.5%")}/>
            <Text style={{color:"white", fontWeight:"bold", }}>Finalizar
            </Text>
          </TouchableOpacity>
          )
        }
  
        </View>
      </View>
    );
  }
}

AlmMap.propTypes = {
  provider: ProviderPropType,
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    // paddingHorizontal: 18,
    height:hp("10%"),
    paddingVertical: 12,
    borderRadius: hp("2%"),
    // flexDirection: 'row',
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: wp("30%"),
    paddingHorizontal: 12,
    alignItems: 'center',
    textAlignVertical:"center",
    marginHorizontal: 10,
    backgroundColor:"rgba(0,0,0,0.8)",
    // flexDirection: 'row',
    
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

export default connect(null, {newAlmacen}) (AlmMap);
