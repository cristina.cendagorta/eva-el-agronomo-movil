import React from "react";
import {View, Text, StyleSheet} from 'react-native'

const ListItemEst = ({item}) => {

    const {nombre, abreviatura, id} = item
    return (
          <View style={styles.container}>
    <Text>{nombre} - {abreviatura}</Text>
    
  </View>
    )
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#f1f2f1',
        borderRadius:10,
        // marginBottom: 10,
        padding: 10
    }

})

export default ListItemEst;