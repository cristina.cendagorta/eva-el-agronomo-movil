import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as ImagePicker from "expo-image-picker";
import { Ionicons } from "@expo/vector-icons";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  Modal,
  Button,
  KeyboardAvoidingView
} from "react-native";
import { LogBox } from 'react-native';
// import HeaderBar from "../Utils/HeaderBar";
// import SimpleModal1 from "./../Alerts/SingUp/SimpleModalok";
// import SimpleModal2 from "../Alerts/SingUp/SimpleModalok2.js";
// import SimpleModal3 from "../Alerts/SingUp//SimpleModalname.js";
// import SimpleModal4 from "../Alerts/SingUp//SimpleModalLastname.js";
// import SimpleModal5 from "../Alerts/SingUp//SimpleModalmail.js";
// import SimpleModal6 from "../Alerts/SingUp//SimpleModalpass.js";
// import SimpleModal7 from "../Alerts/SingUp//SimpleModalphone.js";
// import SimpleModal8 from "../Alerts/SingUp//SimpleModalterms.js";
// import SimpleModal9 from "../Alerts/SingUp//SimpleModalrole.js";
// import SimpleModal40 from "../Alerts/SingUp//SimpleModalsamemail.js"; 

import { getInsumo,getUnidad } from "../../../../../Redux/actions";

import { useDispatch, useSelector } from "react-redux";
import { addEncabezado } from "../../../../../Redux/actions";
import Select2 from "react-native-select-two"

const EncabTras = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const encabezado = useSelector((store) => store.encabcompra)
  const almacenes = useSelector((store) => store.almacen);
  useEffect(() => {
    dispatch(getInsumo());
    dispatch(getUnidad());
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
}, [])

console.log("encab", encabezado)

  // ALERTAS:

  // REGISTRO USUARIO OK
//   const [isModalVisible1, setisModalVisible1] = useState(false);
//   const [chooseData1, setchooseData1] = useState();

//   const changeModalVisible1 = (bool) => {
//     setisModalVisible1(bool);
//   };

//   const setData1 = (data) => {
//     setchooseData1(data);
//   };

//   // REGISTRO TRANSPORTISTA OK
//   const [isModalVisible2, setisModalVisible2] = useState(false);
//   const [chooseData2, setchooseData2] = useState();

//   const changeModalVisible2 = (bool) => {
//     setisModalVisible2(bool);
//   };

//   const setData2 = (data) => {
//     setchooseData2(data);
//   };


//   // NOMBRE MAL INGRESADO
//   const [isModalVisible3, setisModalVisible3] = useState(false);
//   const [chooseData3, setchooseData3] = useState();

//   const changeModalVisible3 = (bool) => {
//     setisModalVisible3(bool);
//   };

//   const setData3 = (data) => {
//     setchooseData3(data);
//   };

//   // APELLIDO MAL INGRESADO

//   const [isModalVisible4, setisModalVisible4] = useState(false);
//   const [chooseData4, setchooseData4] = useState();

//   const changeModalVisible4 = (bool) => {
//     setisModalVisible4(bool);
//   };

//   const setData4 = (data) => {
//     setchooseData4(data);
//   };

//   //MAIL MAL INGRESADO
//   const [isModalVisible5, setisModalVisible5] = useState(false);
//   const [chooseData5, setchooseData5] = useState();

//   const changeModalVisible5 = (bool) => {
//     setisModalVisible5(bool);
//   };

//   const setData5 = (data) => {
//     setchooseData5(data);
//   };

//   // CONTRASEÑA MAL INGRESADA

//   const [isModalVisible6, setisModalVisible6] = useState(false);
//   const [chooseData6, setchooseData6] = useState();

//   const changeModalVisible6 = (bool) => {
//     setisModalVisible6(bool);
//   };

//   const setData6 = (data) => {
//     setchooseData6(data);
//   };
//   // TELEFONO MAL INGRESADO

//   const [isModalVisible7, setisModalVisible7] = useState(false);
//   const [chooseData7, setchooseData7] = useState();

//   const changeModalVisible7 = (bool) => {
//     setisModalVisible7(bool);
//   };

//   const setData7 = (data) => {
//     setchooseData7(data);
//   };
//   // NO ACEPTA TERMINOS
//   const [isModalVisible8, setisModalVisible8] = useState(false);
//   const [chooseData8, setchooseData8] = useState();

//   const changeModalVisible8 = (bool) => {
//     setisModalVisible8(bool);
//   };

//   const setData8 = (data) => {
//     setchooseData8(data);
//   };

//   // NO ELIJE ROL
//   const [isModalVisible9, setisModalVisible9] = useState(false);
//   const [chooseData9, setchooseData9] = useState();

//   const changeModalVisible9 = (bool) => {
//     setisModalVisible9(bool);
//   };

//   const setData9 = (data) => {
//     setchooseData9(data);
//   };

//   // MAIL REPETIDO
//   const [isModalVisible40, setisModalVisible40] = useState(false);
//   const [chooseData40, setchooseData40] = useState();

//   const changeModalVisible40 = (bool) => {
//     setisModalVisible40(bool);
//   };

//   const setData40 = (data) => {
//     setchooseData40(data);
//   };

  


  // useEffect(()=>{
  //   //console.log('aqui esta la respuestaaaa:',respuesta);
  //   if(respuesta?.role === true){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileUser");
  //     changeModalVisible1(true)
  //   }if(respuesta?.role === false){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileCarrier");
  //     changeModalVisible2(true)
  //   }if(respuesta?.role === 1){
  //     // alert('El mail ingresado ya se encuentra en uso!')
  //     changeModalVisible40(true)
  //   }
  // },[respuesta]);


  var alms = almacenes?.map( item => { 
    return { name: item.nombre, id:item.id }; 
  });

  const [reg, setReg] = useState({
    alm: "",
    alm2:"",
    ord: "",
    fecha: "",
  });

  const [place, setPlace] = useState({
    alm:"Almacén de Origen",
    alm2:"Almacén de Destino",
  });


  // const ChangeInput = (e) => {
  //   setReg({
  //     // y sino es  generos y platforms, directamente pongo lo que escribo en el input
  //     ...reg,
  //     [e.target.name]: e.target.value,
  //   });
  // };

  const handleSubmit = (e) => {
    e.preventDefault();

    
      // obj.route.params.establecimiento_tipo_id = reg.tipo,
      // obj.route.params.direccion = reg.direccion,
      // obj.route.params.localidad = reg.localidad,
      // obj.route.params.provincia = reg.provincia,
      // obj.route.params.pais = reg.pais,
      // obj.route.params.zona_id = reg.zona,
      // obj.route.params.observaciones = reg.observaciones;

      const obj = {
        // establecimiento_id: Number(reg.est),
        tipo_movimiento_id:3,
        origen_almacen_id: Number(reg.alm),
        destino_almacen_id: Number(reg.alm2),
        orden_de_compra: reg.ord,
        fecha_movimiento: reg.fecha,
        nro_movimiento:"22333383",
    };


    // validaciones 
//     if (!obj.name ) {
//       changeModalVisible3(true)
//       return
//   }
//   if (!obj.lastName) {
//       // alert("Por favor escribe el Apellido correctamente!")
//       changeModalVisible4(true)
//       return
//   }
//   if (!obj.eMail.includes('.com') || !obj.eMail.includes('@')  ) {
//     changeModalVisible5(true)
//     // alert("Por favor escribe un correo electrónico válido!")
//     return
// } if (!obj.password) {
//   changeModalVisible6(true)
//   // alert("Por favor escribe una Contraseña válida!")
//   return
// }
// if (!obj.phone) {
//   changeModalVisible7(true)
//       // alert("Por favor escribe un número de telefono válido!")
//       return
//   }
//   if (!obj.business) {
//     changeModalVisible8(true)
//     // alert("Debes aceptar los términos para poder registrarte!")
//     return
//   }if (!obj.secret) {
//     changeModalVisible9(true)
//   // alert("Por favor elije un Rol!")
//   return
// }



dispatch(addEncabezado(obj));
 console.log("Estoy enviado", obj);
    setReg({
        alm: "",
        alm2:"",
        ord: "",
        fecha: "",
    });
navigation.navigate("Traslados")
//     console.log(obj);
};

  //funciones para cambiar e.value de los inputs

  // const handelChangeAlm = (name) => {
  //   setReg({
  //     ...reg,
  //     alm: name,
  //   });
  // };
  // const handelChangeAlm2 = (name) => {
  //   setReg({
  //     ...reg,
  //     alm2: name,
  //   });
  // };
  const handelChangeAlm = (name) => {
    console.log("alm",name)
    setReg({
      ...reg,
      alm: name,
    });

    const filtrado6 = alms?.filter((f) => f.id === name)
    setPlace({
      ...place,
     alm: filtrado6[0].name,
    });
    // handelChangeFilter(place.alm)


  };
  const handelChangeAlm2 = (name) => {


    const filtrado6 = alms?.filter((f) => f.id === name)
        console.log("alm2",name)
    setReg({
      ...reg,
      alm2: name,
    });
    setPlace({
      ...place,
     alm2: filtrado6[0].name,
    });
    // handelChangeFilter(place.alm)


  };
  const handelChangeOrd = (name) => {
    setReg({
      ...reg,
      ord: name,
    });
  };
  const handelChangeFecha = (name) => {
    setReg({
      ...reg,
      fecha: name,
    });
  };
    
  

  return (
    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : null}
    keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
    <View style={{ flex: 1,  backgroundColor: 'white'  }}>
      {/* <View style={{marginTop:hp("-2%"),marginLeft:wp("0%"),marginBottom:hp("0%")}}>
        <HeaderBar  screen={'null'} style={{color:"white"}}/>
        </View> */}
    <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffffff" }}
      showsVerticalScrollIndicator={false}
    >
      
      {/* Brand View */}
      {/* <ImageBackground
        source={require("../Home/logo.png")}
        resizeMode= "contain"
        style={{
            display:'flex',
            marginTop:  hp('-18%'),
          height: hp('60%') ,
          width: wp('110%') ,
          alignSelf: "center",
        }}
      >
      </ImageBackground> */}
      {/* Botton View */}
      <View style={styles.bottonView}>
        {/* Welcome View */}
        <View style={{ display: "flex", alignSelf: "center", marginTop:hp("7%"), marginBottom:hp("10%"), width:wp("90%")  }}>
          <Text style={{ color: "#151f27", fontSize: hp("3.5%"),fontWeight: '600', marginTop: hp("3%"),textAlign:"center" }}>
            A continuación debes completar el encabezado, esta información es común para todos los insumos a trasladar:
          </Text>
          
        
        </View>
        {/* inputs */}
        <View style={styles.FormView}>
        <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Almacén de Origen"
          title={place.alm}
          data={alms}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeAlm(name[0])}
        />
                <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Almacén de Destino"
          title={place.alm2}
          data={alms}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeAlm2(name[0])}
        />
          {/* <TextInput
            name="almacenamiento"
            value={reg.alm}
            onChangeText={(name) => handelChangeAlm(name)}
            placeholder="Almacén de Origen*"
            style={styles.TextInput}
          ></TextInput>
              <TextInput
            name="almacenamiento2"
            value={reg.alm2}
            onChangeText={(name) => handelChangeAlm2(name)}
            placeholder="Almacén de Destino*"
            style={styles.TextInput}
          ></TextInput> */}

          {/* <TextInput
            value={reg.ord}
            onChangeText={(name) => handelChangeOrd(name)}
            name="orden de compra"
            placeholder="Orden de Compra"
            style={styles.TextInput}
          ></TextInput> */}
          <TextInput
            value={reg.fecha}
            onChangeText={(name) => handelChangeFecha(name)}
            name="fecha"
            placeholder="Fecha"
            style={styles.TextInput}
          ></TextInput>



          {/* <TextInput
            value={reg.provincia}
            onChangeText={(name) => handelChangeProv(name)}
            name="provincia"
            placeholder="Latitud"
            style={styles.TextInput}
          ></TextInput> */}

          {/* <TextInput
          keyboardType={'phone-pad'}
            value={reg.telefono}
            onChangeText={(name) => handelChangeTel(name)}
            name="telefono"
            placeholder="País"
            style={styles.TextInput}
          ></TextInput> */}
          {/* <TextInput
            value={reg.pais}
            onChangeText={(name) => handelChangePais(name)}
            name="pais"
            placeholder="Longitud"
            style={styles.TextInput}
          ></TextInput> */}

          <TouchableOpacity style={styles.Button} onPress={handleSubmit } >
            <Text style={styles.ButtonText} >
              Crear!
            </Text>
            {/* <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible1}
                  nRequestClose={() => changeModalVisible1(false)}
                >
                  <SimpleModal1
                    changeModalVisible1={changeModalVisible1}
                    setData1={setData1}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible2}
                  nRequestClose={() => changeModalVisible2(false)}
                >
                  <SimpleModal2
                    changeModalVisible2={changeModalVisible2}
                    setData2={setData2}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible3}
                  nRequestClose={() => changeModalVisible3(false)}
                >
                  <SimpleModal3
                    changeModalVisible3={changeModalVisible3}
                    setData3={setData3}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible4}
                  nRequestClose={() => changeModalVisible4(false)}
                >
                  <SimpleModal4
                    changeModalVisible4={changeModalVisible4}
                    setData4={setData4}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible5}
                  nRequestClose={() => changeModalVisible5(false)}
                >
                  <SimpleModal5
                    changeModalVisible5={changeModalVisible5}
                    setData5={setData5}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible6}
                  nRequestClose={() => changeModalVisible6(false)}
                >
                  <SimpleModal6
                    changeModalVisible6={changeModalVisible6}
                    setData6={setData6}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible7}
                  nRequestClose={() => changeModalVisible7(false)}
                >
                  <SimpleModal7
                    changeModalVisible7={changeModalVisible7}
                    setData7={setData7}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible8}
                  nRequestClose={() => changeModalVisible8(false)}
                >
                  <SimpleModal8
                    changeModalVisible8={changeModalVisible8}
                    setData8={setData8}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible9}
                  nRequestClose={() => changeModalVisible9(false)}
                >
                  <SimpleModal9
                    changeModalVisible9={changeModalVisible9}
                    setData9={setData9}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible40}
                  nRequestClose={() => changeModalVisible40(false)}
                >
                  <SimpleModal40
                    changeModalVisible40={changeModalVisible40}
                    setData40={setData40}
                  />
                  
                  </Modal> */}
          </TouchableOpacity>
          
         
        </View>

        
      </View>
    </ScrollView>
    </View>
    </KeyboardAvoidingView>
  );
};

export default EncabTras;

const styles = StyleSheet.create({
  brandView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  brandViewText: {
    color: "#FFC107",
    fontSize: 45,
    fontWeight: "bold",
    textTransform: "uppercase",
    // justifyContent:'flex-start'
  },
  bottonView: {
    flex: 1.5,
    backgroundColor: "white",
    bottom: 50,
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
  },
  FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: -20,
  },
  TextInput: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("2.9%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  TextInput2: {
    width: wp("90%"),
    borderWidth: 2,
    borderColor: "#37C2D0",
    height: hp("3.7%"),
    borderRadius: 10,
    textAlign:"center",
    // marginLeft: wp("5%"),
    marginBottom:hp("2%"),
    color: "black",
    backgroundColor:"lightgrey",
    fontSize:hp("2%"),
  },
  Button: {
    width: "90%",
    color: "black",
    height: 52,
    backgroundColor: "rgb(24,116,28)",
    borderRadius: 10,
    marginTop: hp("10%"),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  ButtonText: {
    // fontWeight: "bold",
    fontSize: hp("3%"),
    color: "white",
  },
  SingUpText: {
    color: "#4632a1",
    fontSize: 20,
  },
  TextButton: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  preg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  pregunta: {
    color: "red",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    padding: 50,
    marginTop: 20,
    borderRadius: 20,
  },
  text: {
    // marginVertical: 20,
    fontSize: 22,
    color: "white",
    fontWeight: "bold",
  },
  TouchableOpacity: {
    backgroundColor: "black",
    alignSelf: "stretch",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    
  },
  checkbox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  checkboxx: {
    marginTop: 15,
  },
  imgPerfil: {
    width: hp('25%'),
    height: hp('25%'),
    borderRadius: hp("30%"),
    borderColor: "rgb(0,140,207)",
    borderWidth: wp('0.8%'),
    marginTop: hp('3%'),
    backgroundColor:"#e1e1e1"
  },
  imgAdd: {
    width: wp("15%"),
    height: wp("15%"),
    marginLeft: wp("35%"),
    marginTop: hp('-13%'),
    borderWidth: hp('0.5%'),
    borderColor: "rgb(189,191,198)",
    borderRadius: 50,
  },
});