const TABLES_URL = 'https://agronomo.herokuapp.com/'

async function syncMultyTables({ maxDelay, setTableState, tables, groupName, ignoreUrl }) {
    const promisesArray = [];
    const objToReturn = { table: groupName, n: 0, tableDetails: {}, errorApi: null, errorInsert: null };
    try {
        const newState = {
            tablesDetails: {},
            tableLoading: false,
            tableLoadingInsert: false,
            tableErrorApi: null,
            tableErrorInsert: null,
            numberOfItems: 0
        }

        // iteramos el array guardamos las promesas con los resultados en un array de promesas
        for (let i = 0; i < tables.length; i++) {

            let table = tables[i]
            let url
            if (ignoreUrl) {
                url = table
            } else {
                if (i === 0) {
                    url = table
                } else {
                    url = `${groupName}/${table}`
                }
            }
            promisesArray.push(syncTable({ table, url }))
        }

        // hacemos un Promise.all y obtenemos un array con los resultados, iteramos y contruimos el objeto que mandamos al estado
        const results = await Promise.all([...promisesArray]);

        // iteramos el array de resultados y cargamos los resultados en cada tabla para subirlo al estado
        for (let result of results) {
            newState.tablesDetails[result.table] = {
                error: result.errorApi,
                errorInsert: result.errorInsert,
                n: result.n
            }
            newState.numberOfItems = newState.numberOfItems + result.n;
            if (result.errorApi !== null) {
                newState.tableErrorApi = result.errorApi;
            }
            if (result.errorInsert !== null) {
                newState.tableErrorInsert = result.errorInsert;
            }
        }
        objToReturn.tableDetails = newState.tablesDetails;
        objToReturn.n = newState.numberOfItems;
        objToReturn.errorApi = newState.tableErrorApi;
        objToReturn.errorInsert = newState.tableErrorInsert;

        setTableState(prev => ({ ...prev, ...newState }))
        return objToReturn;
    } catch (e) {
        setTableState(prev => ({ ...prev, tableLoading: false, tableLoadingInsert: false, tableErrorApi: e.message }))
        return { ...objToReturn, errorApi: e.message }
    }
}


async function syncTable({ table, url }) {

    let numberOfItems = 0;
    let resultsFromApi = [];
    const objToReturn = { table, n: 0, errorApi: null, errorInsert: null }

    //Consulta API
    try {
        let query = TABLES_URL + url
        let results;

        await fetch(query, {
            method: 'GET',
            redirect: 'follow',
        })
            .then(response => response.json())
            .then(data => {
                if (data.detail) {
                    console.log(table, data)
                    objToReturn.errorApi = data.detail
                    return { ...objToReturn }
                } else {
                    results = data.data ? data.data : data
                }
            })

        for (let i in results) {
            let registro = {};
            registro = Object.assign(registro, results[i]);
            registro.syncState = true;

            resultsFromApi.push(registro);
        }
        numberOfItems = resultsFromApi.length;
        if (numberOfItems > 0) {
            objToReturn.n = numberOfItems;
        }
    } catch (e) {
        objToReturn.errorApi = "Hubo un problema en la red."
        return { ...objToReturn }
    }


    try {
        const res = await global.SQLF.InsertSQLite(table, resultsFromApi);
        return objToReturn;
    } catch (e) {
        objToReturn.errorInsert = "Hubo un porblema al guardar la informacion."
        return { ...objToReturn }
    }
}

async function syncSingleTable({ table, maxDelay, setTableState }) {

    let numberOfItems = 0;
    let resultsFromApi = [];
    const objToReturn = { table, n: 0, errorApi: null, errorInsert: null }

    //Consulta a la API
    try {
        await global.SQLF.DeleteTable(table)
        let query = TABLES_URL + table
        let results;

        await fetch(query, {
            method: 'GET',
            redirect: 'follow',
        })
            .then(response => response.json())
            .then(data => {
                if (data.detail) {
                    setTableState(prev => ({ ...prev, tableLoading: false, tableErrorApi: data.detail }))
                    return { ...objToReturn, errorApi: data.detail }
                } else {
                    results = data.data ? data.data : data
                }
            })


        for (let i in results) {
            let registro = {};
            registro = Object.assign(registro, results[i]);
            registro.syncState = true;

            resultsFromApi.push(registro);
        }
        numberOfItems = resultsFromApi.length;
        objToReturn.n = numberOfItems;
        if (numberOfItems === 0) {
            setTableState(prev => ({ ...prev, tableLoading: false, tableLoadingInsert: false }))
            return objToReturn;
        }
        setTableState(prev => ({ ...prev, tableLoading: false, numberOfItems }))

    } catch (e) {
        setTableState(prev => ({ ...prev, tableLoading: false, tableErrorApi: e.message ? e.message : 'No definido' }))
        return { ...objToReturn, errorApi: e.message ? e.message : e }
    }

    //Insert en SQL
    try {
        const result = await global.SQLF.InsertSQLite(table, resultsFromApi)
        setTableState(prev => ({ ...prev, tableLoadingInsert: false }))
        return objToReturn;

    } catch (e) {
        setTableState(prev => ({ ...prev, tableLoadingInsert: false, tableErrorInsert: "Hubo un porblema al guardar la informacion." }));
        return { ...objToReturn, errorInsert: "Hubo un porblema al guardar la informacion." }
    }

}

export { syncMultyTables, syncTable, syncSingleTable }
