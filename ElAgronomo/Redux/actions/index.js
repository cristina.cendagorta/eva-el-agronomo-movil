import axios from "axios";

//ACA ESTAN TODAS LAS ACCIONES PARA ENVIAR Y RECIBIR INFORMACION CON EL BACK

export function logiar(payload) {
    return async function (dispatch) {
      try {
        // console.log("SALIENDO DE DISPACHT LOGIAR",payload)
        const response = await axios.post(`https://test-registro.herokuapp.com/login/${payload.eMail},${payload.password}`)

        .then((r) => {
            dispatch({
                type: "LOGEO",
                payload: r.data,
                // token: r.data.token,
            });
            
            // console.log("Aca llega respuesta del login",r.data);
         
        });
       
      }catch(error){
        dispatch({
          type: "LOGEO",
          payload: error.response,
          // token: r.data.token,
      });
        // console.log("error",error.response);
      }
    };
}

export function registro(payload) {
    return async function () {
      try {
        console.log(payload)
        let body = {
          email: payload.email,
          password: payload.password
        }
        //const response = await axios.post(`${API_URLS}/api/adminregister`, payload)
        const response = await axios.post(`https://agronomo.herokuapp.com/usuario/registro/`, body )
        .then((r) => {
          console.log(r.data);
        });
       
      }catch(error){

        console.log(error.response);
      }
    };
}

export function forgotmail(payload) {
  return async function () {
    try {
      console.log(payload)
      //const response = await axios.post(`${API_URLS}/api/adminregister`, payload)
      const response = await axios.post(`https://test-registro.herokuapp.com/forgotpass/${payload.mail}`)
      .then((r) => {
        console.log(r.data);
      });
     
    }catch(error){

      console.log(error.response);
    }
  };
}

export function olvidepass(payload) {
  return async function () {
    try {
      console.log(payload)
      //const response = await axios.post(`${API_URLS}/api/adminregister`, payload)
      const response = await axios.post(`https://test-registro.herokuapp.com/forgotpassnew/${payload.mail},${payload.codigo},${payload.contraseña}`)
      .then((r) => {
        console.log(r.data);
      });
     
    }catch(error){

      console.log(error.response);
    }
  };
}


export function getEmpresa() {
  console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/empresas/');
        dispatch({
          type: "GET_EMPRESA",
          payload: json.data,
        });
        console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function newempresa(obj) {
  console.log("objetoooo",(obj.route.params))

 return (dispatch) =>
fetch("https://agronomo.herokuapp.com/create_empresas/", {
  method: "post",
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },

  //make sure to serialize your JSON body
  body: JSON.stringify(obj.route.params)
})
.then((resp) => resp.json())
      .then((json) => {
        dispatch({
          type: "NEWEMP",
          payload: json,
        });
        console.log("Aqui esta el token llegando en la action logiarusuario:",json);
      });
}

export function getEstablecimiento(empresa_id) {
    return async function (dispatch) {
      try {
        var json = await axios(`https:/agronomo.herokuapp.com/${empresa_id}/establecimientos/` );
        dispatch({
          type: "GET_ESTABLECIMIENTO",
          payload: json.data,
        });
        console.log('Esto llega a getadminreg', json.data) 
      } catch (error) {
        console.log(error);
      }
    };
}

export function newestablecimiento(obj,empresa_id) {
  console.log("objetoooo2",obj,empresa_id)

 return (dispatch) =>
fetch(`https:/agronomo.herokuapp.com/${empresa_id}/create_establecimientos/`, {
  method: "post",
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }, 

  //make sure to serialize your JSON body
  body: JSON.stringify(obj)
})
.then((resp) => resp.json())
      .then((json) => {
        dispatch({
          type: "NEWEST",
          payload: json,
        });
        console.log("Aqui esta el token llegando en la action logiarusuario:",json);
      });
}

export function newestablecimiento2(obj,empresa_id) {
  console.log("objetoooo2",obj,empresa_id)

 return (dispatch) =>
fetch(`https:/agronomo.herokuapp.com/${empresa_id}/create_establecimientos/`, {
  method: "post",
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }, 

  //make sure to serialize your JSON body
  body: JSON.stringify(obj.route.params)
})
.then((resp) => resp.json())
      .then((json) => {
        dispatch({
          type: "NEWEST",
          payload: json,
        });
        console.log("Aqui esta el token llegando en la action logiarusuario:",json);
      });
}

export function getAlmacen() {
  console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/almacenes/');
        dispatch({
          type: "GET_ALMACEN",
          payload: json.data,
        });
        console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function newAlmacen(obj) {
  console.log("objetoooo2",(obj))

 return (dispatch) =>
fetch("https://agronomo.herokuapp.com/create_almacenes/", {
  method: "post",
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },

  //make sure to serialize your JSON body
  body: JSON.stringify(obj)
})
.then((resp) => resp.json())
      .then((json) => {
        dispatch({
          type: "NEWALM",
          payload: json,
        });
        console.log("Aqui esta el token llegando en la action logiarusuario:",json);
      });
}
export function getInsumo() {
  console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/insumos/');
        dispatch({
          type: "GET_INSUMO",
          payload: json.data,
        });
        console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function newInsumo(obj) {
  console.log("objetoooo2",(obj.route.params))
 return (dispatch) =>
fetch("https://agronomo.herokuapp.com/create_insumos/", {
  method: "post",
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },

  //make sure to serialize your JSON body
  body: JSON.stringify(obj.route.params)
})
.then((resp) => resp.json())
      .then((json) => {
        dispatch({
          type: "NEWINS",
          payload: json,
        });
        console.log("Aqui esta el token llegando en la action logiarusuario:",json);
      });
}
export function getLote() {
  console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/lotes/');
        dispatch({
          type: "GET_LOTE",
          payload: json.data,
        });
        console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function newLote(obj) {
  console.log("objetoooo2",(obj))

 return (dispatch) =>
fetch("https://agronomo.herokuapp.com/create_lotes/", {
  method: "post",
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },

  //make sure to serialize your JSON body
  body: JSON.stringify(obj)
  // body: JSON.stringify(obj.route.params)
})
.then((resp) => resp.json())
      .then((json) => {
        dispatch({
          type: "NEWLOTE",
          payload: json,
        });
        console.log("Aqui esta el token llegando en la action logiarusuario:",json);
      });
}



export function getCondIva() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/empresa/cond_ivas');
        dispatch({
          type: "GET_IVA",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function getRubEmp() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/empresa/rubro_empresas');
        dispatch({
          type: "GET_RUBEMP",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function getMon() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/empresa/monedas');
        dispatch({
          type: "GET_MON",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function getTipEst() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/establecimiento/tipo_establecimientos');
        dispatch({
          type: "GET_TIPEST",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}
export function getZonas() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/establecimiento/zonas');
        dispatch({
          type: "GET_ZONAS",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function getTipAlm() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/almacen/tipo_almacenes');
        dispatch({
          type: "GET_TIPALM",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function getUnidad() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/insumo/unidades');
        dispatch({
          type: "GET_UNIDAD",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function getFamilia() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/insumo/familias');
        dispatch({
          type: "GET_FAMILIA",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}
export function getSubfamilia() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/insumo/subfamilias');
        dispatch({
          type: "GET_SUBFAMILIA",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}
export function getTarea() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/insumo/tareas');
        dispatch({
          type: "GET_TAREA",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}
export function getRubro() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/insumo/rubro_insumos');
        dispatch({
          type: "GET_RUBRO",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}
export function getErogacion() {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        var json = await axios('https://agronomo.herokuapp.com/insumo/tipo_erogaciones');
        dispatch({
          type: "GET_EROGACION",
          payload: json.data,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function addEncabCom(obj) {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        dispatch({
          type: "ADD_ENCABCOMPRA",
          payload: obj,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function addEncabezado(obj) {
  // console.log("que pasa con consultreg")
  console.log("objetoooo2",obj)

  return (dispatch) =>
 fetch(`https:/agronomo.herokuapp.com/create_encabezado_movimiento/`, {
   method: "post",
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json'
   }, 
 
   //make sure to serialize your JSON body
   body: JSON.stringify(obj)
 })
 .then((resp) => resp.json())
       .then((json) => {
         dispatch({
           type: "ADD_ENCABCOMPRA",
           payload: json,
         });
         console.log("Aqui esta el token llegando en la action logiarusuario:",json);
       });
}

export function newDetCom(obj) {
  // console.log("que pasa con consultreg")
  console.log("objetoooo2",obj)

  return (dispatch) =>
 fetch(`https:/agronomo.herokuapp.com/create_movimiento_detalle/`, {
   method: "post",
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json'
   }, 
 
   //make sure to serialize your JSON body
   body: JSON.stringify(obj)
 })
 .then((resp) => resp.json())
       .then((json) => {
         dispatch({
           type: "ADD_DETCOMPRA",
           payload: json,
         });
         console.log("Aqui esta el token llegando en la action logiarusuario:",json);
       });
}

export function getCom(id) {
  // console.log("que pasa con consultreg")
  console.log("que pasa con consultreg",id)
  return async function (dispatch) {
    try {
      var json = await axios(`https://agronomo.herokuapp.com/movimiento_detalle/?id=${id}`);
      dispatch({
        type: "GET_COMPRA",
        payload: json.data,
      });
      console.log('Esto llega a getadminreg', json.data)
    } catch (error) {
      console.log(error);
    }
  };
 
}

export function getDetalle() {
  // console.log("que pasa con consultreg")
  return async function (dispatch) {
    try {
      var json = await axios(`https://agronomo.herokuapp.com/movimiento_detalle/`);
      dispatch({
        type: "GET_DETALLE",
        payload: json.data,
      });
      console.log('Esto llega a getadminreg', json.data)
    } catch (error) {
      console.log(error);
    }
  };
 
}

export function getMov() {
  // console.log("que pasa con consultreg")
  return async function (dispatch) {
    try {
      var json = await axios(`https://agronomo.herokuapp.com/encabezado_movimiento/`);
      dispatch({
        type: "GET_MOVS",
        payload: json.data,
      });
      console.log('Esto llega a getadminreg', json.data)
    } catch (error) {
      console.log(error);
    }
  };
 
}



export function getExist() {
  // console.log("que pasa con consultreg")
  console.log("que pasa con consultreg")
  return async function (dispatch) {
    try {
      var json = await axios(`https://agronomo.herokuapp.com/existencias/`);
      dispatch({
        type: "GET_EXIST",
        payload: json.data,
      });
      console.log('Esto llega a getadminreg', json.data)
    } catch (error) {
      console.log(error);
    }
  };
 
}

export function getExistIns(id) {
  // console.log("que pasa con consultreg")
  console.log("que pasa con consultreg",id)
  return async function (dispatch) {
    try {
      var json = await axios(`https://agronomo.herokuapp.com/existencias/total/?id=${id}`);
      dispatch({
        type: "GET_EXISTINS",
        payload: json.data,
      });
      console.log('Esto llega a getadminreg', json.data)
    } catch (error) {
      console.log(error);
    }
  };
 
}

export function addEncabAju(obj) {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        dispatch({
          type: "ADD_ENCABAJUSTE",
          payload: obj,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function newDetAju(obj) {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        dispatch({
          type: "ADD_DETAJUSTE",
          payload: obj,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function addEncabTras(obj) {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        dispatch({
          type: "ADD_ENCABTRAS",
          payload: obj,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}

export function newDetTras(obj) {
  // console.log("que pasa con consultreg")
    return async function (dispatch) {
      try {
        dispatch({
          type: "ADD_DETTRAS",
          payload: obj,
        });
        // console.log('Esto llega a getadminreg', json.data)
      } catch (error) {
        console.log(error);
      }
    };
}