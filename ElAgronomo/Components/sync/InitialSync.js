import React, { useEffect, useState } from "react"
import { Text, View, StyleSheet, ActivityIndicator, BackHandler } from "react-native"
import { Icon } from '@rneui/themed'
import { syncMultyTables, syncSingleTable } from "./InitialSyncFunctions";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const SyncElement = ({ tableName, tableLoading, tableLoadingInsert, tableError, tableErrorInsert, numberOfItems }) => (
    <View style={styles.ItemBox}>
        <Text style={styles.ItemText}>{tableName}</Text>
        {
            tableLoading === true ?
                <ActivityIndicator size="small" color="#007d3c" />
                :
                tableError ?
                    <Icon name={'close'} color='#C9C9CE' style={styles.closeIcon} type='MaterialCommunityIcons' />
                    :
                    tableLoadingInsert ?
                        <View style={styles.iconsView}>
                            <View style={{ ...styles.badge, backgroundColor: '#007d3c' }}><Text style={{ ...styles.textBadge, color: '#FFF' }}>{numberOfItems}</Text></View>
                            <Icon name={'check'} color='#007d3c' style={styles.closeIcon} type='MaterialCommunityIcons' />
                        </View>
                        :
                        tableErrorInsert ?
                            <View style={styles.iconsView}>
                                <View style={{ ...styles.badge, backgroundColor: '#C9C9CE' }}><Text style={{ ...styles.textBadge, color: '#4D4D4D' }}>{numberOfItems}</Text></View>
                                <Icon name={'close'} color='#C9C9CE' style={styles.closeIcon} type='MaterialCommunityIcons' />
                            </View>
                            :
                            <View style={styles.iconsView}>
                                <View style={styles.badge}><Text style={styles.textBadge}>{numberOfItems}</Text></View>
                                {/*  <Icon name='checkAll' color='#007d3c' style={styles.checkIcon} type='MaterialCommunityIcons' /> */}
                                <MaterialCommunityIcons name="check-all" style={styles.checkIcon} />
                            </View>
        }
    </View>
);

const InitialSync = ({ navigation }) => {

    const [almacenTables, setAlmacenTables] = useState({
        tableName: 'Almacen',
        tablesDetails: {
            tipo_almacenes: { error: null, errorInsert: null, n: 0 },
            almacenes: { error: null, errorInsert: null, n: 0 }
        },
        tableLoading: true,
        tableLoadingInsert: true,
        tableErrorApi: null,
        tableErrorInsert: null,
        numberOfItems: 0
    });
    const [empresaTables, setEmpresaTables] = useState({
        tableName: 'Empresa',
        tablesDetails: {
            cond_ivas: { error: null, errorInsert: null, n: 0 },
            monedas: { error: null, errorInsert: null, n: 0 },
            rubro_empresas: { error: null, errorInsert: null, n: 0 },
            empresas: { error: null, errorInsert: null, n: 0 },
        },
        tableLoading: true,
        tableLoadingInsert: true,
        tableErrorApi: null,
        tableErrorInsert: null,
        numberOfItems: 0
    });
    const [establecimientosTables, setEstablecimientosTables] = useState({
        tableName: 'Establecimientos',
        tablesDetails: {
            zonas: { error: null, errorInsert: null, n: 0 },
            tipo_establecimientos: { error: null, errorInsert: null, n: 0 },
            establecimientos: { error: null, errorInsert: null, n: 0 }
        },
        tableLoading: true,
        tableLoadingInsert: true,
        tableErrorApi: null,
        tableErrorInsert: null,
        numberOfItems: 0
    });
    const [lotesTable, setLotesTable] = useState({
        tableName: 'Lotes',
        table: 'lotes',
        tableLoading: true,
        tableLoadingInsert: true,
        tableErrorApi: null,
        tableErrorInsert: null,
        numberOfItems: 0
    });
    const [facturacionTables, setFacturacionTables] = useState({
        tableName: 'Facturación',
        table: 'facturaciones',
        tableLoading: true,
        tableLoadingInsert: true,
        tableErrorApi: null,
        tableErrorInsert: null,
        numberOfItems: 0
    });
    const [insumosTables, setInsumosTables] = useState({
        tableName: 'Insumos',
        tablesDetails: {
            tareas: { error: null, errorInsert: null, n: 0 },
            unidades: { error: null, errorInsert: null, n: 0 },
            familias: { error: null, errorInsert: null, n: 0 },
            subfamilias: { error: null, errorInsert: null, n: 0 },
            rubro_insumos: { error: null, errorInsert: null, n: 0 },
            tipo_erogaciones: { error: null, errorInsert: null, n: 0 },
            tipo_movimiento_insumos: { error: null, errorInsert: null, n: 0 },
            insumos: { error: null, errorInsert: null, n: 0 },
        },
        tableLoading: true,
        tableLoadingInsert: true,
        tableErrorApi: null,
        tableErrorInsert: null,
        numberOfItems: 0
    });

    // Esta tabla de momento no la vamos a utilizar
    const [relacionalesTables, setRelacionalesTables] = useState({
        tableName: 'Relacionales',
        tablesDetails: {
            inquilinos: { error: null, errorInsert: null, n: 0 },
            establecimiento_almacenes: { error: null, errorInsert: null, n: 0 }
        },
        tableLoading: true,
        tableLoadingInsert: true,
        tableErrorApi: null,
        tableErrorInsert: null,
        numberOfItems: 0
    });
    const [usuarioTables, setUsuarioTables] = useState({
        tableName: 'Usuario',
        table: 'usuarios',
        tableLoading: true,
        tableLoadingInsert: true,
        tableErrorApi: null,
        tableErrorInsert: null,
        numberOfItems: 0
    });
    const [valuacionesTables, setValuacionesTables] = useState({
        tableName: 'Valuaciones',
        tablesDetails: {
            insumos_valorizacion: { error: null, errorInsert: null, n: 0 },
            tipo_metodo_valorizacion: { error: null, errorInsert: null, n: 0 },
            tipo_valorizacion_empresas: { error: null, errorInsert: null, n: 0 },
            historicos_precio_segun_criterio: { error: null, errorInsert: null, n: 0 },
        },
        tableLoading: true,
        tableLoadingInsert: true,
        tableErrorApi: null,
        tableErrorInsert: null,
        numberOfItems: 0
    });
    const [otrosTables, setOtrosTables] = useState({
        tableName: 'Otros',
        tablesDetails: {
            emisor_tarjetas: { error: null, errorInsert: null, n: 0 },
            log_auditoria_usuarios: { error: null, errorInsert: null, n: 0 },
            tipo_metodo_valorizacion: { error: null, errorInsert: null, n: 0 },
            tipo_valorizacion_empresas: { error: null, errorInsert: null, n: 0 },
            historicos_precio_segun_criterio: { error: null, errorInsert: null, n: 0 },
            encabezado_movimiento: { error: null, errorInsert: null, n: 0 },
            movimiento_detalle: { error: null, errorInsert: null, n: 0 },
            stocks: { error: null, errorInsert: null, n: 0 },
            roles: { error: null, errorInsert: null, n: 0 }
        },
        tableLoading: true,
        tableLoadingInsert: true,
        tableErrorApi: null,
        tableErrorInsert: null,
        numberOfItems: 0
    });

    const maxDelayConfig = 600000


    //Detectamos la accion de presion el boton back
    useEffect(() => {
        BackHandler.addEventListener(
            'hardwareBackPress',
            handleBackButtonPressAndroid
        );
        return () => {
            BackHandler.removeEventListener(
                'hardwareBackPress',
                handleBackButtonPressAndroid
            );
        }
    }, [])

    //Si la pantalla esta activa, cancelamos la acción de vovler atras
    const handleBackButtonPressAndroid = () => {
        if (!navigation.isFocused()) {
            return false;
        }
        return true;
    };

    const getDataErp = async () => {
        let promisesArray = []

        try {
            promisesArray = [
                syncSingleTable({ table: 'facturaciones', maxDelay: maxDelayConfig, setTableState: setFacturacionTables }),
                syncSingleTable({ table: 'usuarios', maxDelay: maxDelayConfig, setTableState: setUsuarioTables }),
                syncSingleTable({ table: 'lotes', maxDelay: maxDelayConfig, setTableState: setLotesTable }),
                syncMultyTables({ maxDelay: maxDelayConfig, tables: ['establecimientos', 'zonas', 'tipo_establecimientos'], setTableState: setEstablecimientosTables, groupName: "establecimiento" }),
                syncMultyTables({ maxDelay: maxDelayConfig, tables: ['empresas', 'cond_ivas', 'monedas', 'rubro_empresas'], setTableState: setEmpresaTables, groupName: "empresa" }),
                syncMultyTables({ maxDelay: maxDelayConfig, tables: ['insumos', 'tareas', 'unidades', 'familias', 'subfamilias', 'rubro_insumos', 'tipo_erogaciones', 'tipo_movimiento_insumos'], setTableState: setInsumosTables, groupName: "insumo" }),
                syncMultyTables({ maxDelay: maxDelayConfig, tables: ['almacenes', 'tipo_almacenes'], setTableState: setAlmacenTables, groupName: "almacen" }),
                syncMultyTables({ maxDelay: maxDelayConfig, tables: ['insumos_valorizacion', 'tipo_metodo_valorizacion', 'tipo_valorizacion_empresas', 'historicos_precio_segun_criterio'], setTableState: setValuacionesTables, ignoreUrl: true }),
                syncMultyTables({ maxDelay: maxDelayConfig, tables: ['emisor_tarjetas', 'log_auditoria_usuarios', 'tipo_metodo_valorizacion', 'tipo_valorizacion_empresas', 'historicos_precio_segun_criterio', 'movimiento_detalle', 'stocks', 'roles'], setTableState: setOtrosTables, ignoreUrl: true }), // falta agregar una tabla, problemas en la estructura

                /* syncMultyTables({ maxDelay: maxDelayConfig, tables: ['inquilinos', 'establecimiento_almacenes'], setTableState: setRelacionalesTables }),*/ //de momento no se utiliza estas tablas
            ];

            const results = await Promise.all(promisesArray);

            return results;
        } catch (e) {
            return Promise.reject(e)
        }
    }

    useEffect(() => {
        if (navigation.isFocused()) {
            getDataErp()
                .then(results => {
                    if (results.find((result) => result.errorApi || result.errorInsert) !== undefined) {
                        //ACA abajo analizamos los errores y derivamos a distintas pantallas (en desarrollo)
                        /* if (results.find((result) => result.errorApi)) {
                            const errorObj = results.find((result) => result.errorApi);
                            navigation.navigate('ErrorNetwork', { resultsArrFromInitialSync: results, errorType: 'initialSync', errMsg: '', error: `${errorObj.table}: ${errorObj.errorApi}` })
                        } else {
                            const errorObj = results.find((result) => result.errorInsert);
                            navigation.navigate('ErrorDB', { resultsArrFromInitialSync: results, errMsg: '', error: `${errorObj.table}: ${errorObj.errorInsert}`, errorType: 'initialSync' })
                        } */
                    } else {
                        //En caso de que no existan errores actualizamos el tiempo de syncronizacion y redirigimos a la pantalla home
                        /* global.SQLF.UpdateLastSync('All').then(res =>  navigation.navigate('HomeStack')*/

                    }
                })
                .catch((e) => {
                    //En caso de error redirigr a pantalla correspondiente (en desarrollo)
                })
        }

    }, [])

    return (
        <View style={styles.principalView} >
            <View>
                <Text style={styles.principalText}>Sincronización Inicial</Text>
                <Text style={styles.secondaryText}>Espere mientras descargamos y guardamos los datos.</Text>
            </View>

            <SyncElement tableName={almacenTables.tableName} tableLoading={almacenTables.tableLoading} tableLoadingInsert={almacenTables.tableLoadingInsert} tableErrorInsert={almacenTables.tableErrorInsert} tableError={almacenTables.tableErrorApi} numberOfItems={almacenTables.numberOfItems} />
            <SyncElement tableName={empresaTables.tableName} tableLoading={empresaTables.tableLoading} tableLoadingInsert={empresaTables.tableLoadingInsert} tableErrorInsert={empresaTables.tableErrorInsert} tableError={empresaTables.tableErrorApi} numberOfItems={empresaTables.numberOfItems} />
            <SyncElement tableName={establecimientosTables.tableName} tableLoading={establecimientosTables.tableLoading} tableLoadingInsert={establecimientosTables.tableLoadingInsert} tableErrorInsert={establecimientosTables.tableErrorInsert} tableError={establecimientosTables.tableErrorApi} numberOfItems={establecimientosTables.numberOfItems} />
            <SyncElement tableName={facturacionTables.tableName} tableLoading={facturacionTables.tableLoading} tableLoadingInsert={facturacionTables.tableLoadingInsert} tableErrorInsert={facturacionTables.tableErrorInsert} tableError={facturacionTables.tableErrorApi} numberOfItems={facturacionTables.numberOfItems} />
            <SyncElement tableName={insumosTables.tableName} tableLoading={insumosTables.tableLoading} tableLoadingInsert={insumosTables.tableLoadingInsert} tableErrorInsert={insumosTables.tableErrorInsert} tableError={insumosTables.tableErrorApi} numberOfItems={insumosTables.numberOfItems} />
            <SyncElement tableName={lotesTable.tableName} tableLoading={lotesTable.tableLoading} tableLoadingInsert={lotesTable.tableLoadingInsert} tableErrorInsert={lotesTable.tableErrorInsert} tableError={lotesTable.tableErrorApi} numberOfItems={lotesTable.numberOfItems} />
            {/*             <SyncElement tableName={relacionalesTables.tableName} tableLoading={relacionalesTables.tableLoading} tableLoadingInsert={relacionalesTables.tableLoadingInsert} tableErrorInsert={relacionalesTables.tableErrorInsert} tableError={relacionalesTables.tableErrorApi} numberOfItems={relacionalesTables.numberOfItems} />
 */}
            <SyncElement tableName={usuarioTables.tableName} tableLoading={usuarioTables.tableLoading} tableLoadingInsert={usuarioTables.tableLoadingInsert} tableErrorInsert={usuarioTables.tableErrorInsert} tableError={usuarioTables.tableErrorApi} numberOfItems={usuarioTables.numberOfItems} />
            <SyncElement tableName={valuacionesTables.tableName} tableLoading={valuacionesTables.tableLoading} tableLoadingInsert={valuacionesTables.tableLoadingInsert} tableErrorInsert={valuacionesTables.tableErrorInsert} tableError={valuacionesTables.tableErrorApi} numberOfItems={valuacionesTables.numberOfItems} />
            <SyncElement tableName={otrosTables.tableName} tableLoading={otrosTables.tableLoading} tableLoadingInsert={otrosTables.tableLoadingInsert} tableErrorInsert={otrosTables.tableErrorInsert} tableError={otrosTables.tableErrorApi} numberOfItems={otrosTables.numberOfItems} />

        </View>
    )
}

export default InitialSync

const styles = StyleSheet.create({
    principalView: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: 20
    },
    principalText: {
        marginTop: 20,
        color: '#007d3c',
        fontSize: 28,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    secondaryText: {
        color: '#757575',
        marginTop: 25,
        marginBottom: 50,
        fontSize: 19,
        textAlign: 'center'
    },
    ItemBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 8,
        borderBottomWidth: 1,
        borderColor: '#C9C9CE',
        width: "80%",
        margin: 3
    },
    ItemText: {
        color: '#757575',
        fontSize: 16,
        textAlignVertical: 'center',
        color: '#222222'
    },
    checkIcon: {
        fontSize: 18,
        color: '#007d3c',
        fontWeight: 'bold'
    },
    closeIcon: {
        fontSize: 18,
        color: '#C9C9CE',
        fontWeight: 'bold'
    },
    iconsView: {
        display: 'flex',
        flexDirection: 'row'
    },
    badge: {
        backgroundColor: '#007d3c',
        borderRadius: 10,
        paddingTop: 3,
        paddingRight: 5,
        paddingLeft: 5,
        marginRight: 5
    },
    textBadge: {
        fontSize: 10,
        color: '#fff',
    },
})