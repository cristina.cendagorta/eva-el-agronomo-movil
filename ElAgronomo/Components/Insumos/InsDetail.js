import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView,
    Modal,
    Button,
    ActivityIndicator,
    FlatList,
  } from "react-native";
  import React, { useState, useEffect, useRef,useMemo } from "react";
  import { useNavigation } from "@react-navigation/core";
  import { useDispatch, useSelector } from "react-redux";
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
  import { useIsFocused } from "@react-navigation/native";
  import { getUnidad } from "../../Redux/actions";
  import { getFamilia } from "../../Redux/actions";
  import { getSubfamilia } from "../../Redux/actions";
  import { getTarea } from "../../Redux/actions";
  import { getRubro } from "../../Redux/actions";
  import { getErogacion } from "../../Redux/actions";
const InsDetail = (item) => {
const dispatch = useDispatch();
const navigation = useNavigation();
// me traigo todo lo que necesito del back
const unidades = useSelector((store) => store.unidades);
const familias = useSelector((store) => store.familias);
const subfamilias = useSelector((store) => store.subfamilias);
const tareas = useSelector((store) => store.tareas);
const rubros = useSelector((store) => store.rubros);
const erogaciones = useSelector((store) => store.erogaciones);
const isFocused = useIsFocused();

useEffect(() => {
  if(isFocused){ 
    // apenas se monta el componente ejecuto acciones para traer info del back
  dispatch(getUnidad());
  dispatch(getFamilia());
  dispatch(getSubfamilia());
  dispatch(getTarea());
  dispatch(getRubro());
  dispatch(getErogacion());
  setTimeout(function(){ 
    setLoading(true)
    }, 1500);
} 
}, [,isFocused]);
const [isLoading, setLoading] = useState(false);
console.log("item",familias,subfamilias,tareas,rubros,erogaciones)
// filtro el insumo que tengo, con las caracteristicas que tiene,
const unidadess = unidades.filter((f) => f.detalle_unidad === item.route.params.detalle_unidad)
const familiass = familias.filter((f) => f.detalle_familia === item.route.params.detalle_familia)
const subfamiliass = subfamilias.filter((f) => f.detalle_subfamilia === item.route.params.detalle_subfamilia)
const tareass = tareas.filter((f) => f.detalle_tarea === item.route.params.detalle_tarea)
const rubross = rubros.filter((f) => f.detalle_rubro_insumo === item.route.params.detalle_rubro_insumo)
const erogacioness = erogaciones.filter((f) => f.nombre_tipo_erogacion === item.route.params.nombre_tipo_erogacion)
console.log("familiass", familiass)

{ if (isLoading === false || unidades === null){
  return (
  <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
  <ActivityIndicator size="large" color="#007d3c" />
</View>
)
}
}

{ if(isLoading === true && unidades !== null){ // muestro todos los detalles

  return(

    <ScrollView style={{backgroundColor:"white"}}>
            <Text style={{
        // marginLeft:wp("3%"),
      color:"grey",
      fontSize:hp("4%"),
      fontWeight:"bold",
      textAlign:"center",
      marginBottom:hp("5%")
      }}>
    Detalle del Insumo
    </Text>
        
        <View style={{marginTop:hp("2%"),}}>
        <View style={{
        marginBottom:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Nombre
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.nombre} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Abreviatura
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.abreviatura}
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Código Externo
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.codigo_externo} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Unidad
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {unidadess[0].detalle_unidad} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Familia
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {familiass[0].detalle_familia} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Subfamilia
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {subfamiliass[0].detalle_subfamilia} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Tarea
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {tareass[0].detalle_tarea} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Rubro
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {rubross[0].detalle_rubro_insumo} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey", 
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Tipo de Erogación
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {erogacioness[0].nombre_tipo_erogacion} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  

{/* <FlatList
 data={data}
 keyExtractor={({ id }, index) => id}
 renderItem={({ item }) => (
//    <ItemDetail item={item}/>   

            <TouchableOpacity
            //   onPress={() => 
            //     navigation.navigate('EstDetail', item)
            // }
            >
            <ListItem>
            <Icon name='business' />
            
      <ListItem.Content>
        <ListItem.Title>{item.nombre}</ListItem.Title>
        <ListItem.Subtitle>{item.abreviatura}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron color="black" />
    </ListItem>
          </TouchableOpacity>

  )}
/> */}
 {/* <FlatList
        data={item}
        keyExtractor={ (item) => item.id }
        renderItem={({item, index}) =>{
          return (
            <TouchableOpacity
              onPress={() => 
                navigation.navigate('EstDetail', item)
            }>
            <ListItem>
            <Icon name='business' />
            
      <ListItem.Content>
        <ListItem.Title>{item.nombre}</ListItem.Title>
        <ListItem.Subtitle>{item.abreviatura}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron color="black" />
    </ListItem>
          </TouchableOpacity>
          )
        }}
        // <ListItemEst item ={item} />}
        ItemSeparatorComponent= { () =>       <View style={{ marginBottom: 10, borderColor:'#00000020', borderWidth:0.5}}></View>}
        ListHeaderComponent = { () => <Text style={{fontWeight:"bold", fontSize:18,marginBottom:10,}}>Establecimientos</Text>}
        // <ListItem item = {item} />}
            /> */}
</ScrollView>
)
}
}
}

const styles = StyleSheet.create({
    container: {
      margin: 4,
      padding: 30
    }
  })


export default InsDetail;