
import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView,
    Modal,
    Button,
    ActivityIndicator,
    FlatList,
  } from "react-native";
  import { useNavigation } from "@react-navigation/core";
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import empresa from "../../Components/Configuration/empresa.png"
import { getLote } from "../../Redux/actions";
import { useIsFocused } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useEffect } from "react";
import { getEstablecimiento } from "../../Redux/actions";
import { ListItem, Icon } from '@rneui/themed'

const LotesxEst = (est) => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    // me traigo lotes , establecimientos
    const lote = useSelector((store) => store.lote);
    const establecimientos = useSelector((store) => store.establecimiento);
    const isFocused = useIsFocused();
console.log("est",est)
// filtro los lotes que pertenecen a ese establecimiento
    const lotes = lote.filter((f) => f.establecimiento_id === est.route.params) 
  console.log("lotes",lotes)
  return(
    <SafeAreaView style={{marginTop:30}}>
    <FlatList
           data={lotes} // a esos lotes, los pongo en una lista
           keyExtractor={ (item) => item.id }
           renderItem={({item, index}) =>{
             return (
               <TouchableOpacity
                 onPress={() => 
                   navigation.navigate('LoteDetail', item)
               }>
               <ListItem>
               <Icon name='business' />
               
         <ListItem.Content>
           <ListItem.Title>{item.codigo}</ListItem.Title>
           <ListItem.Subtitle>{item.superficie} Hectáreas</ListItem.Subtitle>
         </ListItem.Content>
         <ListItem.Chevron color="black" />
       </ListItem>
             </TouchableOpacity>
             )
           }}
           // <ListItemEst item ={item} />}
           ItemSeparatorComponent= { () =>       <View style={{ marginBottom: 10, borderColor:'#00000020', borderWidth:0.5}}></View>}
           ListHeaderComponent = { () => <Text style={{fontWeight:"bold", fontSize:18,marginBottom:20, textAlign:"center"}}>Lotes</Text>}
           // <ListItem item = {item} />}
               />
   </SafeAreaView>
)


} 

export default LotesxEst;