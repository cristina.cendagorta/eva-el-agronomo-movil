import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import MapView, {
  MAP_TYPES,
  Polygon,
  ProviderPropType,
  Marker,
} from 'react-native-maps';
import {MaterialCommunityIcons} from "@expo/vector-icons"
const { width, height } = Dimensions.get('window');
import { newLote } from '../../Redux/actions';
import { connect } from "react-redux";
const ASPECT_RATIO = width / height;
const LATITUDE = -34.61315;
const LONGITUDE = -58.37723;
const LATITUDE_DELTA = 0.0922;
const anchor = { x: 0.02, y: 0.02 }
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;

//aca dibujo un poligono para representar perimetro de un lote

class PolygonCreator extends React.Component {
  constructor(props) {
    super(props);
//estados locales
    this.state = {
      showsUserLocation: true,
      followsUserLocation : true,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      polygon:{
        id:1
      },
      editar:false,
      polygons: [],
      editing: null,
      creatingHole: false,
      area: 0,
      areas: false,
    };
    console.log("editing",this.state.editing);
    
  }

// aca empieza

calcArea(locations) { // est es para calcular el area

  if (!locations.length) {    
      return 0;
  }
  if (locations.length < 3) {
      return 0;
  }
  let radius = 6371000;

  const diameter = radius * 2;
  const circumference = diameter * Math.PI;
  const listY = [];
  const listX = [];
  const listArea = [];
  // calculate segment x and y in degrees for each point

  const latitudeRef = locations[0].latitude;
  const longitudeRef = locations[0].longitude;
  for (let i = 1; i < locations.length; i++) {
    let latitude = locations[i].latitude;
    let longitude = locations[i].longitude;
    listY.push(this.calculateYSegment(latitudeRef, latitude, circumference));

    listX.push(this.calculateXSegment(longitudeRef, longitude, latitude, circumference));

  }

  // calculate areas for each triangle segment
  for (let i = 1; i < listX.length; i++) {
    let x1 = listX[i - 1];
    let y1 = listY[i - 1];
    let x2 = listX[i];
    let y2 = listY[i];
    listArea.push(this.calculateAreaInSquareMeters(x1, x2, y1, y2));

  }

  // sum areas of all triangle segments
  let areasSum = 0;
  listArea.forEach(area => areasSum = areasSum + area)


  // get abolute value of area, it can't be negative
  let areaCalc = Math.abs(areasSum);// Math.sqrt(areasSum * areasSum);  
  console.log("area",areaCalc);
  return areaCalc
}

calculateAreaInSquareMeters(x1, x2, y1, y2) {
  return (y1 * x2 - x1 * y2) / 2;
}

calculateYSegment(latitudeRef, latitude, circumference) {
  return (latitude - latitudeRef) * circumference / 360.0;
}

calculateXSegment(longitudeRef, longitude, latitude, circumference)     {
  return (longitude - longitudeRef) * circumference * Math.cos((latitude * (Math.PI / 180))) / 360.0;
}


// aca termina







  

  finish() {

   this.setState({area: ((this.calcArea(this.state.editing.coordinates))*0.0001).toFixed(2)})  // aca ejecuto el calcular area
   this.setState({areas:true}) // esto es para mostrar el area



    
  }
  finish2() {


    const obj = this.props.route.params // aca me traigo toda la info
    obj.poligono = JSON.stringify(this.state.editing.coordinates) // le meto en el objeto, la info pero pasado a un string
    obj.superficie_calculada = Number(this.state.area) // guardo el area calculado
    this.props.newLote(obj); // hago el post hacia el back
    this.props.navigation.navigate("Lotes")

    
  }

  createHole() {
    const { editing, creatingHole } = this.state;
    if (!creatingHole) {
      this.setState({
        creatingHole: true,
        editing: {
          ...editing,
          holes: [...editing.holes, []],
        },
      });
    } else {
      const holes = [...editing.holes];
      if (holes[holes.length - 1].length === 0) {
        holes.pop();
        this.setState({
          editing: {
            ...editing,
            holes,
          },
        });
      }
      this.setState({ creatingHole: false });
    }
  }
  
  onPress(e) {
    if(this.state.editar === true){
    const { editing, creatingHole } = this.state;
    if (!editing) {
      this.setState({
        editing: {
          coordinates: [e.nativeEvent.coordinate],
        },
      });
    } else if (!creatingHole) {
      this.setState({
        editing: {
          ...editing,
          coordinates: [...editing.coordinates, e.nativeEvent.coordinate],
        },
      });
    } else {
      const holes = [...editing.holes];
      holes[holes.length - 1] = [
        ...holes[holes.length - 1],
        e.nativeEvent.coordinate,
      ];
      this.setState({
        editing: {
          ...editing,
          coordinates: [...editing.coordinates],
        },
      });
    }
  }
  }
  

  render() {
    const mapOptions = {
      scrollEnabled: true,
    };

    if (this.state.editing) {
      mapOptions.scrollEnabled = false;
      mapOptions.onPanDrag = e => this.onPress(e);
    }

    return (
      <View style={styles.container}>
        
        
        <MapView
          showsUserLocation={this.state.showsUserLocation}
          followsUserLocation={this.state.followsUserLocation}
          provider={this.props.provider}
          style={styles.map}
          mapType={MAP_TYPES.HYBRID}
          initialRegion={this.state.region}
          onPress={e => this.onPress(e)}
          {...mapOptions}
        >
          {this.state.polygons.map(polygon => (
           
            <Polygon
              key={this.state.polygon + 1}
              coordinates={polygon.coordinates}
              holes={polygon.holes}
              strokeColor="#F00"
              fillColor="rgba(255,0,0,0.5)"
              strokeWidth={1}
            />
          ))}
          {this.state.editing && (
     
            <Polygon
              key={this.state.polygon + 1}
              coordinates={this.state.editing.coordinates} // esto dibuja cada punto
              holes={this.state.editing.holes}
              strokeColor="rgba(121,170,242,255)"
              fillColor="rgba(121,170,242,0.4)"
              strokeWidth={hp("0.5%")}
            />
          )}
          {
          this.state.editar === true &&  this.state.editing != null? // si esta activa el editar poligono
        //  console.log("hola", this.state.editing)
           this.state.editing.coordinates.map(polygon => (
       <Marker // este el marcador de cada punto
       coordinate={polygon}
       pinColor="blue"
       id={polygon.id}
      //  centerOffset={{ x: -58, y: 100 }}
    anchor={{ x: 0.5, y: 0.5 }}
       >
       <Image
       source={require('./pin.png')}
       style={{width: hp("3%"), height: hp("3%")}}
       resizeMode="cover"
     />
     
      </Marker>
       

)
)
: null
}
        </MapView>
        <View style={{height:hp("8%"), backgroundColor:"white", marginBottom:hp("60%"), width:wp("90%"), borderRadius:hp("1%"), flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
        <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='vector-square' size={hp("3.5%")} style={{marginRight:wp("3%")}}/>
                    <Text style={{color:"black", fontWeight:"bold", }}>
                Demarcá los limites del lote a agregar
              </Text>

        </View>
        {this.state.area != 0 ?
              <View style={{height:hp("8%"), backgroundColor:"white", marginTop:hp("-8%"), width:wp("90%"), borderRadius:hp("1%"), flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
              <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='vector-square' size={hp("3.5%")} style={{marginRight:wp("3%")}}/>
                          <Text style={{color:"black", fontWeight:"bold", }}>
                     El Área del lote es de : {this.state.area} Hectáreas
                    </Text>
      
              </View>
              : null
        
      }
  
        <View style={styles.buttonContainer}>
          {
          
          this.state.editar === false
          
          ?
          
            <TouchableOpacity
              onPress={() => this.setState({editar:true})}
              style={[styles.bubble, styles.button]}
            >

              <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='vector-square' size={hp("3.5%")}/>
                    <Text style={{color:"white", fontWeight:"bold", }}>
                Dibujar
              </Text>
       
            </TouchableOpacity>
             :       <TouchableOpacity
                    onPress={() => this.setState({editing:null,editar:false,area:0, areas:false})}
                    style={[styles.bubble, styles.button]}
                  >
                    <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='delete' size={hp("3.5%")}/>
                    <Text style={{color:"white", fontWeight:"bold", }}>
                    
                      Limpiar
                    </Text>
                  </TouchableOpacity>

                  
          }
          {this.state.editar === true &&  (
            this.state.areas === false ?
            <TouchableOpacity
              onPress={() => this.finish()}
              style={[styles.bubble, styles.button]}
            >
              <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='vector-square' size={hp("3.5%")}/>
              <Text style={{color:"white", fontWeight:"bold", }}>Calcular Área
              </Text>
            </TouchableOpacity>
            :
            <TouchableOpacity
            onPress={() => this.finish2()}
            style={[styles.bubble, styles.button]}
          >
            <MaterialCommunityIcons color={"rgba(121,170,242,255)"} name='check-decagram' size={hp("3.5%")}/>
            <Text style={{color:"white", fontWeight:"bold", }}>Finalizar
            </Text>
          </TouchableOpacity>
          )
        }
  
        </View>
      </View>
    );
  }
}

PolygonCreator.propTypes = {
  provider: ProviderPropType,
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    // paddingHorizontal: 18,
    height:hp("10%"),
    paddingVertical: 12,
    borderRadius: hp("2%"),
    // flexDirection: 'row',
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: wp("30%"),
    paddingHorizontal: 12,
    alignItems: 'center',
    textAlignVertical:"center",
    marginHorizontal: 10,
    backgroundColor:"rgba(0,0,0,0.8)",
    // flexDirection: 'row',
    
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

export default connect(null, {newLote}) (PolygonCreator);
