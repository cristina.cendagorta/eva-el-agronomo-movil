import React, { useState, useEffect, useRef,useMemo } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { useNavigation } from "@react-navigation/core";
import {
  Text,
  ScrollView,
  ImageBackground,
  Dimensions,
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Modal
} from "react-native";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import {MaterialCommunityIcons} from "@expo/vector-icons"
import { useDispatch, useSelector } from "react-redux";
import { olvidepass } from "../../Redux/actions";
import ModalSuccess from '../Alerts/ModalSuccess';

//POR SI OLVIDASTE LA CONTRASEÑA

const ForgotPass2 = (mail) => {
    const dispatch = useDispatch();
    const navigation = useNavigation();

    console.log("mail",mail)

    const [show, setShow] = useState(false);
    const [visible,setVisible] = useState(true);
    const [show2, setShow2] = useState(false);
    const [visible2,setVisible2] = useState(true);
    const ref_input3 = useRef();
    const ref_input4 = useRef();

    const [pass, setPass] = useState({
        codigo: "",
        contraseña: "",
        contraseña2: "",
      });

      const handelChangeCode = (name) => {
        setPass({
          ...pass,
          codigo: name,
        });
      };

      const handelChangePass = (name) => {
        setPass({
          ...pass,
          contraseña: name,
        });
      };
      const handelChangePass2 = (name) => {
        setPass({
          ...pass,
          contraseña2: name,
        });
      };

      const handleSubmit = (e) => {
        e.preventDefault();
        pass.contraseña != pass.contraseña2 || !pass.contraseña ? alert("la contraseña no coincide") : null
        const obj = {
          mail:mail.route.params,
          codigo:pass.codigo,
          contraseña: pass.contraseña,    
        };
    
    dispatch(olvidepass(obj));
     console.log("Estoy enviado", obj);
        setPass({
            codigo: "",
            contraseña: "",
            contraseña2: "",
        });
        changeModalVisible2(true)
    // navigation.navigate("Login")
        console.log(obj);
    };

          /// --> ESTADO PARA EL MODAL DE SUCCESS <-- ///
          const [isModalVisible2, setisModalVisible2] = useState(false);
          const [chooseData2, setchooseData2] = useState();
      
          const changeModalVisible2 = (bool) => {
            setisModalVisible2(bool);
          };
      
          const setData2 = (data) => {
            setchooseData2(data);
          };
    

return(
  <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
  <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : null}
      keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
<ScrollView
  style={{ flex: 1, backgroundColor: "#ffffffff" }}
  showsVerticalScrollIndicator={false}
>
      <View>
          <Text style={styles.texto}>Autentificación</Text>
    </View>
    <View style={styles.view2}>
          <Text style={styles.texto2}>Un código de autentificación ha sido enviado a {mail.route.params}</Text>
    </View>
    <View >
    <OTPInputView 
    pinCount={6}
    style={styles.otp}
    codeInputFieldStyle={styles.otpin}
    AutoFocusOnLoad={false}
    onCodeFilled  =  { ( codigo  =>  { 
        handelChangeCode(codigo) 
     } )}
    />
    </View>
    <View style={styles.newpass}>
          <Text style={styles.texto}>Ingrese su nueva contraseña</Text>
    </View>
    <View style={styles.inputs}>
    <TextInput
            value={pass.contraseña}
            onChangeText={(name) => handelChangePass(name)}
            name="contraseña"
            placeholder="Contraseña*"
            secureTextEntry={visible}
            style={styles.TextInput}
            ref={ref_input3}
            returnKeyType="next"
            onSubmitEditing={() => ref_input4.current.focus()}
            
          ></TextInput>
          <TouchableOpacity style={styles.btnEye} onPress={
            ()=>{
              setVisible(!visible)
              setShow(!show)
            }
          }>
            <MaterialCommunityIcons
            name={show === false ? 'eye-off-outline' : 'eye-outline'}
            size={hp("3.7%")}
            color={"#344c40"}
            />
            </TouchableOpacity>

          <TextInput
            value={pass.contraseña2}
            onChangeText={(name) => handelChangePass2(name)}
            name="contraseña2"
            placeholder="Repetir Contraseña*"
            secureTextEntry={visible2}
            style={styles.TextInput}
            ref={ref_input4}
            
          ></TextInput>
             <TouchableOpacity style={styles.btnEye2} onPress={
            ()=>{
              setVisible2(!visible2)
              setShow2(!show2)
            }
          }>
            <MaterialCommunityIcons
            name={show2 === false ? 'eye-off-outline' : 'eye-outline'}
            size={hp("3.7%")}
            color={"#344c40"}
            />
            </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.Button} onPress={handleSubmit } >
            <Text style={styles.ButtonText} >
              Enviar
            </Text>
            <Modal
         transparent={true}
         animationType="fade"
         visible={isModalVisible2}
         nRequestClose={() => changeModalVisible2(false)}
        >
        <ModalSuccess
          changeModalVisible2={changeModalVisible2}
          setData2={setData2} />
        </Modal>
          </TouchableOpacity>  
    </ScrollView> 
    </KeyboardAvoidingView>
    </SafeAreaView>
);

}

export default ForgotPass2;

const styles = StyleSheet.create({

    texto:{
        fontSize:hp("4%"),
        fontWeight:"bold",
        textAlign:"center",
        marginTop:hp("3%")
    },
    texto2:{
        fontSize:hp("2.8%"),
        textAlign:"center",
        color:"grey",
        // marginBottom:hp("-10%")
    },
    view2:{
        width:wp("90%"), 
        alignSelf:"center", 
        // marginTop:hp("1%")
    },
    newpass:{
        marginTop:hp("-42%")
    },
    otp:{
      height:hp("100%"),
        width:wp("90%"),
        alignSelf:"center",
        marginTop:hp("-40%")
    },
    otpin:{
        borderRadius:hp("1%"),
        backgroundColor:"rgba(0,140,207,0.3)",
        color:"black",
        fontSize:hp("3.5%"),
        fontWeight:"bold"
    },
    inputs:{
        marginTop:hp("3.5%"),
        alignSelf:"center",
        width:wp("100%")
    },
    TextInput: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#eceef2",
        height: hp("7.8%"),
        borderRadius: 10,
        paddingLeft: hp("2.5%"),
        marginTop: hp("3%"),
        color: "black",
        fontSize:hp("2.3%"),
        backgroundColor:"#eceef2",
        marginLeft:wp("5%")
      },
      btnEye: {
        position:"absolute",
        alignSelf:"flex-end",
        marginTop:hp("5%"),
        paddingRight:wp("8%")
      },
      btnEye2: {
        position:"absolute",
        alignSelf:"flex-end",
        marginTop:hp("16%"),
        paddingRight:wp("8%")
      },
      Button: {
        width: "90%",
        color: "black",
        height: hp("8%"),
        backgroundColor: "rgb(0,140,207)",
        borderRadius: 10,
        marginTop: hp("8%"),
        display: "flex",
        justifyContent: "center",
        alignSelf: "center",
        shadowOpacity: 80,
        elevation: 15,
      },
      ButtonText: {
        alignSelf:"center",
        fontWeight: "bold",
        fontSize: hp("3.3%"),
        color: "white",
      },
})