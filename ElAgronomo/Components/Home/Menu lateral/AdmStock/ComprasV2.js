import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect, useRef } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {SafeAreaView, StyleSheet, View, Text, Image, TouchableOpacity, ActivityIndicator} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {MaterialCommunityIcons} from "@expo/vector-icons"
import { useDispatch, useSelector } from "react-redux";
import { useIsFocused } from "@react-navigation/native";
import empresa from "../../../Configuration/empresa.png"
const Compras = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const encabezado = useSelector((store) => store.encabcompra);
  const detalle = useSelector((store) => store.detcompra);
console.log("enc", encabezado)
  const isFocused = useIsFocused();
  useEffect(() => {
    if(isFocused){ 


  } 
  }, [,isFocused]);
  const [isLoading, setLoading] = useState(false);
  setTimeout(function(){
setLoading(true)
  }, 1500);


    const foods = [
        {
          id: '1',
          name: 'Tierra',
          ingredients: 'Adquisición de Insumos',
          icon: 'cart-plus',
          screen:'compras',
          price:100,
          
        },
        {
          id: '2',
          name: 'Agua',
          ingredients: 'Entregas / Recepciones',
          icon: 'account-hard-hat',
          price:100,
        },
        {
          id: '3',
          name: 'Glifosato',
          ingredients: 'Movimiento entre                                                      almacenes/establecimientos',
          icon: 'dolly',
          price:100,
        },
        {
            id: '4',
            name: 'Semillas',
            ingredients: 'Recuento de Stock',
            icon: 'counter',
            price:100,
          },

      ];
      const CartCard = ({item}) => {
        return (
          <View style={style.cartCard}    onPress={() => 
            navigation.navigate('DetCom', item)
        }>
            {/* <Image source={item.image} style={{height: 80, width: 80}} /> */}
            
            <View style={style.actionBtn}>
                   <View style={{width:wp("30%")}}>
                   <Text style={{fontWeight: 'bold', fontSize: 17, color:"green"}}>Producto</Text>
                   <Text style={{fontWeight: 'bold', fontSize: 15}}>{item.name}</Text>
                {/* <Icon name="add" size={25} color={"white"} /> */}
              </View>

                {/* <Icon name="add" size={25} color={"white"} /> */}
              </View>
            <View
              style={{
                height: 100,
                
                marginLeft:wp("10%"),
                // paddingVertical: 20,
                alignItems:"center",
                justifyContent:"center",
                flex: 1,
              }}>
           <Text style={{fontWeight: 'bold', fontSize: 17, color:"green"}}>Cantidad</Text>
                   <Text style={{fontWeight: 'bold', fontSize: 15}}>{item.price}</Text>
            </View>
            <View
              style={{
                height: 100,
                marginLeft:wp("-2%"),
                // paddingVertical: 20,
                alignItems:"center",
                justifyContent:"center",
                flex: 1,
              }}>
           <Text style={{fontWeight: 'bold', fontSize: 17, color:"green"}}>Precio</Text>
                   <Text style={{fontWeight: 'bold', fontSize: 15}}>${item.price}</Text>
            </View>
          </View>
        );
      };


      {if(isLoading === false ){     
        return(
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <ActivityIndicator size="large" color="#007d3c" />
    </View>
    )
    }
    }
    if(isLoading === true && encabezado === null  ){
      return(
        <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
    
    <Image
    source={empresa}
    resizeMode= "contain"
    style={{
        display:'flex',
        marginTop:  hp('-8%'),
      height: hp('70%') ,
      width: wp('80%') ,
      alignSelf: "center",
    }}
  >
  </Image>
  
    <View style={{alignItems:"center"}}>
    <Text style={{
      color:"#182E44",
      fontSize:hp("2.6%"),
      fontWeight:"bold",
      marginTop:hp("-15%")}}>
      Agrega una nueva orden de compra
    </Text>
  </View>
  <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
        <TouchableOpacity onPress={() => navigation.navigate("EncabCom")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
          <View style={{ alignItems: 'center'}}>
            <Text
            
              style={{  
                fontSize: hp("2.8%"),
                color:"white",
                marginTop:hp("2%")                       
                
              }}>
              + Agregar
            </Text>
          </View>
        </TouchableOpacity>
        </View>
  </SafeAreaView>


  )

}
    
    {
      if(isLoading === true &&  encabezado != null  ){
return (
    <SafeAreaView style={{backgroundColor: "white", flex: 1}}>
      <View style={style.header}>
        {/* <Icon name="arrow-back-ios" size={28} onPress={navigation.goBack} /> */}
        
        <Text style={{fontSize: hp("3.2%"), fontWeight: 'bold', color:"black", textAlign:"center"}}>Encabezado</Text>
        <View style={{height:hp("0.2%"), backgroundColor:"grey"}}></View>
        <View style={{flexDirection:"row",backgroundColor:"white"}}>
        <Text style={{fontSize: hp("2.7%"), fontWeight: 'bold', color:"black",marginLeft:wp("5%")}}>Fecha</Text>
        <Text style={{fontSize: hp("2.7%"), color:"grey", marginLeft:wp("37%")}}>{encabezado.fecha}</Text>
        </View>
        <View style={{height:hp("0.1%"), backgroundColor:"grey"}}></View>
        <View style={{flexDirection:"row", backgroundColor:"white"}}>
        <Text style={{fontSize: hp("2.7%"), fontWeight: 'bold', color:"black",marginLeft:wp("5%")}}>Orden de Compra</Text>
        <Text style={{fontSize: hp("2.7%"), color:"grey", marginLeft:wp("16%")}}>{encabezado.ord}</Text>
        </View>
        <View style={{height:hp("0.1%"), backgroundColor:"grey"}}></View>
        <View style={{flexDirection:"row",backgroundColor:"white"}}>
        <Text style={{fontSize: hp("2.7%"), fontWeight: 'bold', color:"black",marginLeft:wp("5%")}}>Almacén</Text>
        <Text style={{fontSize: hp("2.7%"), color:"grey", marginLeft:wp("32%")}}>{encabezado.alm}</Text>
        </View>
        <View style={{height:hp("0.1%"), backgroundColor:"grey"}}></View>
      </View>
      <FlatList
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 80}}
        data={foods}
        renderItem={({item}) => <CartCard item={item} />}
        ListFooterComponentStyle={{paddingHorizontal: 20, marginTop: 20}}
        ListFooterComponent={() => (
          <View>
            <View style={{marginHorizontal: 30}}>
              {/* <PrimaryButton title="CHECKOUT" /> */}
            </View>
          </View>
        )}
      />
        <View style={{alignSelf:"center", justifyContent:"center", }} >
        <TouchableOpacity onPress={() => navigation.navigate("DetCom")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"),marginTop:hp("-13%"), borderRadius:wp("5%")}}>
          <View style={{ alignItems: 'center'}}>
            <Text
            
              style={{  
                fontSize: hp("2.8%"),
                color:"white",
                marginTop:hp("2%")                       
                
              }}>
              + Agregar
            </Text>
          </View>
        </TouchableOpacity>
        </View>
    </SafeAreaView>
);

}

}
}

export default Compras;

const style = StyleSheet.create({
    header: {
      borderRadius:hp("2%"),
      width:wp("90%"),
      height:hp("20%"),
      borderWidth:hp("0.2%"),
      borderColor:"black",
      backgroundColor: "green",
      color:"white",
      paddingVertical: 5,
      flexDirection: 'column',
      alignSelf: 'center',
      marginHorizontal: 20,
    },
    cartCard: {
      height: hp("10%"),
      elevation: 15,
      borderRadius: 10,
      backgroundColor: "white",
      marginVertical: 10,
      marginHorizontal: 20,
      paddingHorizontal: 10,
      flexDirection: 'row',
      alignItems: 'center',
    },
    actionBtn: {
      width: wp("10%"),
      height: wp("10%"),
    borderColor:"#f2f2f2",
      borderRadius: 30,
      borderWidth:hp("0.2%"),
    marginLeft:wp("3%"),
      flexDirection: 'column',
      justifyContent: 'center',
      alignContent: 'center',
    },
    actionBtn2: {
      marginRight:wp("-3%"),

      },
  });