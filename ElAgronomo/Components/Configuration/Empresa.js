import React, { useState, useEffect, useRef,useMemo } from "react";
import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView,
    Modal,
    Button,
    ActivityIndicator,
    SectionList,
    FlatList
  } from "react-native";
  import { useDispatch, useSelector } from "react-redux";
  import { useNavigation } from "@react-navigation/core";
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import empresa from "../Configuration/empresa.png"
 import { getEmpresa } from "../../Redux/actions";
 import { useIsFocused } from "@react-navigation/native";
 import { getCondIva } from "../../Redux/actions";
 import { getMon } from "../../Redux/actions";
import { getRubEmp } from "../../Redux/actions";
  

const Empresa = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  //ME TRAIGO DEL BACK LAS EMPRESAS
  const empresas = useSelector((store) => store.empresa);
  const isFocused = useIsFocused();
  useEffect(() => {
    if(isFocused){ 
      //EJECUTO LAS ACCIONES PARA TRAER, MONEDAS, EMPRESAS, COND DE IVA Y RUBROS
      dispatch(getCondIva());
    dispatch(getEmpresa());
    dispatch(getMon())
    dispatch(getRubEmp())
  } 
  }, [,isFocused]);
  console.log("se ejecuta?",empresas)
  const [isLoading, setLoading] = useState(false);

  setTimeout(function(){
setLoading(true)
  }, 1500);

      //SI ESTA CARGANDO, O NO LLEGO LA INFO DE LAS EMPRESAS
        {if(isLoading === false || empresas === null){     
          return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <ActivityIndicator size="large" color="#007d3c" />
    </View>
    )
  }
      
      } // SI TERMINO DE CARGAR, Y NO HAY UNA EMPRESA INGRESADA
      if(isLoading === true && empresas!= null && empresas.length === 0  ){

        return(
        <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
    
    <Image
    source={empresa}
    resizeMode= "contain"
    style={{
        display:'flex',
        marginTop:  hp('-8%'),
      height: hp('70%') ,
      width: wp('80%') ,
      alignSelf: "center",
    }}
  >
  </Image>
  
    <View style={{alignItems:"center"}}>
    <Text style={{
      color:"#182E44",
      fontSize:hp("2.6%"),
      fontWeight:"bold",
      marginTop:hp("-15%")}}>
      Agrega toda la información de tu empresa
    </Text>
  </View>
  <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
        <TouchableOpacity onPress={() => navigation.navigate("AddEmpresa")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
          <View style={{ alignItems: 'center'}}>
            <Text
            
              style={{  
                fontSize: hp("2.8%"),
                color:"white",
                marginTop:hp("2%")                       
                
              }}>
              + Agregar
            </Text>
          </View>
        </TouchableOpacity>
        </View>
  </SafeAreaView>
  )
}
// EN EL CASO DE QUE HAYA TERMINADO DE CARGAR, Y HAYA UNA EMPRESA CARGADA
if(isLoading === true && empresas.length > 0 ){

  return(
    <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
          <View style={{alignItems:"center", marginTop:hp("20%")}}>
    <Text style={{
      color:"#182E44",
      fontSize:hp("4%"),
      fontWeight:"bold",
      marginBottom:hp("5%")
      }}>
    Razon Social: {empresas[0].razon_social} 
    </Text>
  </View>
  <View style={{alignItems:"center"}}>
    <Text style={{
      color:"#182E44",
      fontSize:hp("4%"),
      fontWeight:"bold",
      marginBottom:hp("5%")
      }}>
     Cuit:{empresas[0].cuit} 
    </Text>
  </View>

  <View style={{alignItems:"center"}}>
    <Text style={{
      color:"#182E44",
      fontSize:hp("4%"),
      fontWeight:"bold",
      marginBottom:hp("5%")
      }}>
     Pais:{empresas[0].direccion_pais} 
    </Text>
  </View>

  <View style={{alignItems:"center"}}>
    <Text style={{
      color:"#182E44",
      fontSize:hp("4%"),
      fontWeight:"bold",
      marginBottom:hp("5%")
      }}>
     Fecha de Cierre:{empresas[0].fecha_cierre} 
    </Text>
  </View>

</SafeAreaView>
)
}
  
};

export default Empresa;