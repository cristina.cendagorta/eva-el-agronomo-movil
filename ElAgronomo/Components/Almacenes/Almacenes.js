import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView,
    Modal,
    Button,
    ActivityIndicator,
    FlatList
  } from "react-native";
  import { useNavigation } from "@react-navigation/core";
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import empresa from "../../Components/Configuration/empresa.png"
 import { getAlmacen } from "../../Redux/actions";
 import { getEstablecimiento } from "../../Redux/actions";
 import { useIsFocused } from "@react-navigation/native";
 import { useDispatch, useSelector } from "react-redux";
 import React, { useState, useEffect } from "react";
 import { ListItem, Icon } from '@rneui/themed'
import { getTipAlm } from "../../Redux/actions";
const Almacenes = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const almacenes = useSelector((store) => store.almacen);
  const almaceness = useSelector((store) => store.tipalm);
  const establecimientos = useSelector((store) => store.establecimiento); 
  const isFocused = useIsFocused();
  useEffect(() => { 
    if(isFocused){ 
      // activo la accion de traer los establecimientos, almacenes y tipo de almacenes
    dispatch(getEstablecimiento());
    dispatch(getAlmacen());
    dispatch(getTipAlm()); 
    setTimeout(function(){
      setLoading(true)
        }, 1000);
    
  } 
  }, [,isFocused]);
  console.log("se ejecuta?",almacenes)
  //este es un estado para ejecutar el loading
  const [isLoading, setLoading] = useState(false);

  console.log("llegaaa", establecimientos)


 // si todavia esta cargando
  {if(isLoading === false ){     
    // visualizo el cargando
    return(
  <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
  <ActivityIndicator size="large" color="#007d3c" />
</View>
)
}
}
{ // si se termino el loading, pero no llego ningun almacen, muestro una pantalla neutra
if(isLoading === true  && almacenes === null ){
      return(
        <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
    
    <Image
    source={empresa}
    resizeMode= "contain"
    style={{
        display:'flex',
        marginTop:  hp('-8%'),
      height: hp('70%') ,
      width: wp('80%') ,
      alignSelf: "center",
    }}
  >
  </Image>
  
    <View style={{alignItems:"center"}}>
    <Text style={{
      color:"#182E44",
      fontSize:hp("2.6%"),
      fontWeight:"bold",
      marginTop:hp("-15%")}}>
      Agrega toda la información de tu almacén
    </Text>
  </View>
  <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
        <TouchableOpacity onPress={() => navigation.navigate("AddAlmacen")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
          <View style={{ alignItems: 'center'}}>
            <Text
            
              style={{  
                fontSize: hp("2.8%"),
                color:"white",
                marginTop:hp("2%")                       
                
              }}>
              + Agregar
            </Text>
          </View>
        </TouchableOpacity>
        </View>
  </SafeAreaView>

  )
}
} // si no esta cargando, y la info llego bien
if(isLoading === true && almacenes != null   ){
// muestro una lista de almacenes
  return(
    <SafeAreaView style={{marginTop:30}}>
 <FlatList
        data={almacenes} // tomo la data de almacenes
        keyExtractor={ (item) => item.id }
        renderItem={({item, index}) =>{
          return (
            <TouchableOpacity
              onPress={() => 
                navigation.navigate('AlmDetail', item) // si toco en algun almacen, lo llevo al detalle
            }>
            <ListItem>
            <Icon name='business' />
            {/* defino que es lo que muestro: */}
      <ListItem.Content> 
        <ListItem.Title>{item.nombre}</ListItem.Title>
        <ListItem.Subtitle>{item.abreviatura}</ListItem.Subtitle>
        {/* <ListItem.Subtitle>{establecimientos[item.establecimiento_id - 1].nombre}</ListItem.Subtitle> */}
      </ListItem.Content>
      <ListItem.Chevron color="black" />
    </ListItem>
          </TouchableOpacity>
          )
        }}
        // <ListItemEst item ={item} />}
        ItemSeparatorComponent= { () =>       <View style={{ marginBottom: 10, borderColor:'#00000020', borderWidth:0.5}}></View>}
        ListHeaderComponent = { () => <Text style={{fontWeight:"bold", fontSize:18,marginBottom:20, width:wp("90%"), alignSelf:"center", textAlign:"center"}}>Almacenes</Text>}
        // <ListItem item = {item} />}
            />
              <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("3%")}} >
        <TouchableOpacity onPress={() => navigation.navigate("AddAlmacen")} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
          <View style={{ alignItems: 'center'}}>
            <Text
            
              style={{  
                fontSize: hp("2.8%"),
                color:"white",
                marginTop:hp("2%")                       
                
              }}>
              + Agregar
            </Text>
          </View>
        </TouchableOpacity>
        </View>
</SafeAreaView>

//     <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
//           <View style={{alignItems:"center", marginTop:hp("20%")}}>
//     <Text style={{
//       color:"#182E44",
//       fontSize:hp("4%"),
//       fontWeight:"bold",
//       marginBottom:hp("5%")
//       }}>
//     Nombre: {almacenes[0].nombre} 
//     </Text>
//   </View>
//   <View style={{alignItems:"center"}}>
//     <Text style={{
//       color:"#182E44",
//       fontSize:hp("4%"),
//       fontWeight:"bold",
//       marginBottom:hp("5%")
//       }}>
//      Geoposición:{almacenes[0].geoposicion} 
//     </Text>
//   </View>

//   <View style={{alignItems:"center"}}>
//     <Text style={{
//       color:"#182E44",
//       fontSize:hp("4%"),
//       fontWeight:"bold",
//       marginBottom:hp("5%")
//       }}>
//      Establecimiento:{almacenes[0].establecimiento_id} 
//     </Text>
//   </View>

//   <View style={{alignItems:"center"}}>
//     <Text style={{
//       color:"#182E44",
//       fontSize:hp("4%"),
//       fontWeight:"bold",
//       marginBottom:hp("5%")
//       }}>
//      Almacén:{almacenes[0].almacenes_tipo_id} 
//     </Text>
//   </View>

// </SafeAreaView>
)


}
};

export default Almacenes;