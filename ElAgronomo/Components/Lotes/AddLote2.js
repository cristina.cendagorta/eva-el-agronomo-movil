import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as ImagePicker from "expo-image-picker";
import { Ionicons } from "@expo/vector-icons";
import { newestablecimiento } from "../../Redux/actions";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  Modal,
  Button,
  KeyboardAvoidingView
} from "react-native";
import Select2 from "react-native-select-two"
import DatePicker from 'react-native-datepicker'
import { LogBox } from 'react-native';
import MapView from 'react-native-maps';
// import HeaderBar from "../Utils/HeaderBar";
// import SimpleModal1 from "./../Alerts/SingUp/SimpleModalok";
// import SimpleModal2 from "../Alerts/SingUp/SimpleModalok2.js";
// import SimpleModal3 from "../Alerts/SingUp//SimpleModalname.js";
// import SimpleModal4 from "../Alerts/SingUp//SimpleModalLastname.js";
// import SimpleModal5 from "../Alerts/SingUp//SimpleModalmail.js";
// import SimpleModal6 from "../Alerts/SingUp//SimpleModalpass.js";
// import SimpleModal7 from "../Alerts/SingUp//SimpleModalphone.js";
// import SimpleModal8 from "../Alerts/SingUp//SimpleModalterms.js";
// import SimpleModal9 from "../Alerts/SingUp//SimpleModalrole.js";
// import SimpleModal40 from "../Alerts/SingUp//SimpleModalsamemail.js"; 



// import CheckBox from "@react-native-community/checkbox";
// import { ModalPicker } from "./ModalPicker";
import { useDispatch, useSelector } from "react-redux";
import { newLote } from "../../Redux/actions";

const AddLote2 = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  // me traigo establecimientos
  const establecimientos = useSelector((store) => store.establecimiento)

  useEffect(() => {
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
}, [])


// los mapeo para ponerlos en los selects
var establecimientoss = establecimientos?.map( item => { 
  return { name: item.nombre, id:item.id }; 
});
  // ALERTAS:

  // REGISTRO USUARIO OK
//   const [isModalVisible1, setisModalVisible1] = useState(false);
//   const [chooseData1, setchooseData1] = useState();

//   const changeModalVisible1 = (bool) => {
//     setisModalVisible1(bool);
//   };

//   const setData1 = (data) => {
//     setchooseData1(data);
//   };

//   // REGISTRO TRANSPORTISTA OK
//   const [isModalVisible2, setisModalVisible2] = useState(false);
//   const [chooseData2, setchooseData2] = useState();

//   const changeModalVisible2 = (bool) => {
//     setisModalVisible2(bool);
//   };

//   const setData2 = (data) => {
//     setchooseData2(data);
//   };


//   // NOMBRE MAL INGRESADO
//   const [isModalVisible3, setisModalVisible3] = useState(false);
//   const [chooseData3, setchooseData3] = useState();

//   const changeModalVisible3 = (bool) => {
//     setisModalVisible3(bool);
//   };

//   const setData3 = (data) => {
//     setchooseData3(data);
//   };

//   // APELLIDO MAL INGRESADO

//   const [isModalVisible4, setisModalVisible4] = useState(false);
//   const [chooseData4, setchooseData4] = useState();

//   const changeModalVisible4 = (bool) => {
//     setisModalVisible4(bool);
//   };

//   const setData4 = (data) => {
//     setchooseData4(data);
//   };

//   //MAIL MAL INGRESADO
//   const [isModalVisible5, setisModalVisible5] = useState(false);
//   const [chooseData5, setchooseData5] = useState();

//   const changeModalVisible5 = (bool) => {
//     setisModalVisible5(bool);
//   };

//   const setData5 = (data) => {
//     setchooseData5(data);
//   };

//   // CONTRASEÑA MAL INGRESADA

//   const [isModalVisible6, setisModalVisible6] = useState(false);
//   const [chooseData6, setchooseData6] = useState();

//   const changeModalVisible6 = (bool) => {
//     setisModalVisible6(bool);
//   };

//   const setData6 = (data) => {
//     setchooseData6(data);
//   };
//   // TELEFONO MAL INGRESADO

//   const [isModalVisible7, setisModalVisible7] = useState(false);
//   const [chooseData7, setchooseData7] = useState();

//   const changeModalVisible7 = (bool) => {
//     setisModalVisible7(bool);
//   };

//   const setData7 = (data) => {
//     setchooseData7(data);
//   };
//   // NO ACEPTA TERMINOS
//   const [isModalVisible8, setisModalVisible8] = useState(false);
//   const [chooseData8, setchooseData8] = useState();

//   const changeModalVisible8 = (bool) => {
//     setisModalVisible8(bool);
//   };

//   const setData8 = (data) => {
//     setchooseData8(data);
//   };

//   // NO ELIJE ROL
//   const [isModalVisible9, setisModalVisible9] = useState(false);
//   const [chooseData9, setchooseData9] = useState();

//   const changeModalVisible9 = (bool) => {
//     setisModalVisible9(bool);
//   };

//   const setData9 = (data) => {
//     setchooseData9(data);
//   };

//   // MAIL REPETIDO
//   const [isModalVisible40, setisModalVisible40] = useState(false);
//   const [chooseData40, setchooseData40] = useState();

//   const changeModalVisible40 = (bool) => {
//     setisModalVisible40(bool);
//   };

//   const setData40 = (data) => {
//     setchooseData40(data);
//   };

  


  // useEffect(()=>{
  //   //console.log('aqui esta la respuestaaaa:',respuesta);
  //   if(respuesta?.role === true){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileUser");
  //     changeModalVisible1(true)
  //   }if(respuesta?.role === false){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileCarrier");
  //     changeModalVisible2(true)
  //   }if(respuesta?.role === 1){
  //     // alert('El mail ingresado ya se encuentra en uso!')
  //     changeModalVisible40(true)
  //   }
  // },[respuesta]);

//hago estados locales


  const [reg, setReg] = useState({
    est: "",
    cod: "",
    sup: "",
    poli: "",
  });

  const [place, setPlace] = useState({
    est: "Establecimiento",
  });

  // const ChangeInput = (e) => {
  //   setReg({
  //     // y sino es  generos y platforms, directamente pongo lo que escribo en el input
  //     ...reg,
  //     [e.target.name]: e.target.value,
  //   });
  // };

  const handleSubmit = (e) => {
    e.preventDefault();

    
      // obj.route.params.establecimiento_tipo_id = reg.tipo,
      // obj.route.params.direccion = reg.direccion,
      // obj.route.params.localidad = reg.localidad,
      // obj.route.params.provincia = reg.provincia,
      // obj.route.params.pais = reg.pais,
      // obj.route.params.zona_id = reg.zona,
      // obj.route.params.observaciones = reg.observaciones;

      const obj = { // armo el objeto a enviar al back
        establecimiento_id: Number(reg.est),
        codigo: reg.cod,
        superficie: Number(reg.sup),
        // poligono: "3",
    };

    // validaciones 
//     if (!obj.name ) {
//       changeModalVisible3(true)
//       return
//   }
//   if (!obj.lastName) {
//       // alert("Por favor escribe el Apellido correctamente!")
//       changeModalVisible4(true)
//       return
//   }
//   if (!obj.eMail.includes('.com') || !obj.eMail.includes('@')  ) {
//     changeModalVisible5(true)
//     // alert("Por favor escribe un correo electrónico válido!")
//     return
// } if (!obj.password) {
//   changeModalVisible6(true)
//   // alert("Por favor escribe una Contraseña válida!")
//   return
// }
// if (!obj.phone) {
//   changeModalVisible7(true)
//       // alert("Por favor escribe un número de telefono válido!")
//       return
//   }
//   if (!obj.business) {
//     changeModalVisible8(true)
//     // alert("Debes aceptar los términos para poder registrarte!")
//     return
//   }if (!obj.secret) {
//     changeModalVisible9(true)
//   // alert("Por favor elije un Rol!")
//   return
// }



// dispatch(newLote(obj));
 console.log("Estoy enviado", obj);
    setReg({
    est: "",
    cod: "",
    sup: "",
    poli: "",
    });
    setPlace({
      est: "Establecimiento",
    });
navigation.navigate("AddLote",obj) // envio el objeto hacia el addlote que seria donde divujo el poligono
//     console.log(obj);
};

const handleSubmit2 = (e) => { // este se activa sin agregar la info de la ubicacion
  e.preventDefault();

  
    // obj.route.params.establecimiento_tipo_id = reg.tipo,
    // obj.route.params.direccion = reg.direccion,
    // obj.route.params.localidad = reg.localidad,
    // obj.route.params.provincia = reg.provincia,
    // obj.route.params.pais = reg.pais,
    // obj.route.params.zona_id = reg.zona,
    // obj.route.params.observaciones = reg.observaciones;

    const obj = {
      establecimiento_id: Number(reg.est),
      codigo: reg.cod,
      superficie: Number(reg.sup),
      // poligono: "3",
  };

  // validaciones 
//     if (!obj.name ) {
//       changeModalVisible3(true)
//       return
//   }
//   if (!obj.lastName) {
//       // alert("Por favor escribe el Apellido correctamente!")
//       changeModalVisible4(true)
//       return
//   }
//   if (!obj.eMail.includes('.com') || !obj.eMail.includes('@')  ) {
//     changeModalVisible5(true)
//     // alert("Por favor escribe un correo electrónico válido!")
//     return
// } if (!obj.password) {
//   changeModalVisible6(true)
//   // alert("Por favor escribe una Contraseña válida!")
//   return
// }
// if (!obj.phone) {
//   changeModalVisible7(true)
//       // alert("Por favor escribe un número de telefono válido!")
//       return
//   }
//   if (!obj.business) {
//     changeModalVisible8(true)
//     // alert("Debes aceptar los términos para poder registrarte!")
//     return
//   }if (!obj.secret) {
//     changeModalVisible9(true)
//   // alert("Por favor elije un Rol!")
//   return
// }



dispatch(newLote(obj)); // agrego lote , sin poligono
console.log("Estoy enviado", obj);
  setReg({
  est: "",
  cod: "",
  sup: "",
  poli: "",
  });
navigation.navigate("Lotes")
//     console.log(obj);
};

  //funciones para cambiar e.value de los inputs


  const handelChangeEst = (name) => {
    setReg({
      ...reg,
      est: name,
    });

    const filtrado6 = establecimientos?.filter((f) => f.id === name)
    setPlace({
      ...place,
     est: filtrado6[0].nombre,
    });
  };
  const handelChangeCod = (name) => {
    setReg({
      ...reg,
      cod: name,
    });
  };
  const handelChangeSup = (name) => {
    setReg({
      ...reg,
      sup: name,
    });
  };
  const handelChangePoli = (name) => {
    setReg({
      ...reg,
      poli: name,
    });
  };

  
  

  return (
    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : null}
    keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
    <View style={{ flex: 1,  backgroundColor: 'white'  }}>
      {/* <View style={{marginTop:hp("-2%"),marginLeft:wp("0%"),marginBottom:hp("0%")}}>
        <HeaderBar  screen={'null'} style={{color:"white"}}/>
        </View> */}
    <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffffff" }}
      showsVerticalScrollIndicator={false}
    >
      
      {/* Brand View */}
      {/* <ImageBackground
        source={require("../Home/logo.png")}
        resizeMode= "contain"
        style={{
            display:'flex',
            marginTop:  hp('-18%'),
          height: hp('60%') ,
          width: wp('110%') ,
          alignSelf: "center",
        }}
      >
      </ImageBackground> */}
      {/* Botton View */}
      <View style={styles.bottonView}>
        {/* Welcome View */}
        <View style={{ padding: 40, display: "flex", alignItems: "center", }}>
          <Text style={{ color: "#151f27", fontSize: hp("4.7%"),fontWeight: '600', marginTop: hp("3%") }}>
            Datos de tu Lote
          </Text>
        
        </View>
        {/* inputs */}
        <View style={styles.FormView}>
          <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Establecimiento"
          title={place.est}
          data={establecimientoss}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeEst(name[0])}
        />

          <TextInput
            value={reg.cod}
            onChangeText={(name) => handelChangeCod(name)}
            name="codigo de lote"
            placeholder="Código de Lote"
            style={styles.TextInput}
          ></TextInput>
          <TextInput
            value={reg.sup}
            onChangeText={(name) => handelChangeSup(name)}
            name="superficie"
            placeholder="Superficie (Hectáreas)"
            style={styles.TextInput}
          ></TextInput>

{/* <MapView
    initialRegion={{
      latitude: -34.62545,
      longitude: -58.45321,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }}
    style={{width:wp("70%"),height:wp("40%"),marginTop: hp("2%"), borderWidth:wp("3%"), borderColor:"black"}}
  /> */}


          {/* <TextInput
            value={reg.provincia}
            onChangeText={(name) => handelChangeProv(name)}
            name="provincia"
            placeholder="Latitud"
            style={styles.TextInput}
          ></TextInput> */}

          {/* <TextInput
          keyboardType={'phone-pad'}
            value={reg.telefono}
            onChangeText={(name) => handelChangeTel(name)}
            name="telefono"
            placeholder="País"
            style={styles.TextInput}
          ></TextInput> */}
          {/* <TextInput
            value={reg.pais}
            onChangeText={(name) => handelChangePais(name)}
            name="pais"
            placeholder="Longitud"
            style={styles.TextInput}
          ></TextInput> */}
<View style={{flexDirection:"row", marginLeft:-20,}}>
          <TouchableOpacity style={styles.Button} onPress={handleSubmit } >
            <Text style={styles.ButtonText} >
              Agregar Mapa
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.Button} onPress={handleSubmit2 } >
            <Text style={styles.ButtonText} >
              + Agregar
            </Text>
          </TouchableOpacity>
          
         
        </View>

        </View>
      </View>
    </ScrollView>
    </View>
    </KeyboardAvoidingView>
  );
};

export default AddLote2;

const styles = StyleSheet.create({
  brandView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  brandViewText: {
    color: "#FFC107",
    fontSize: 45,
    fontWeight: "bold",
    textTransform: "uppercase",
    // justifyContent:'flex-start'
  },
  bottonView: {
    flex: 1.5,
    backgroundColor: "white",
    bottom: 50,
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
  },
  FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: -20,
  },
  TextInput: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("6.9%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.3%"),
  },
  Button: {
    width: "35%",
    color: "black",
    height: 52,
    backgroundColor: "rgb(24,116,28)",
    borderRadius: 10,
    marginTop: hp("10%"),
    marginLeft:20,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  ButtonText: {
    // fontWeight: "bold",
    fontSize: hp("3%"),
    color: "white",
  },
  SingUpText: {
    color: "#4632a1",
    fontSize: 20,
  },
  TextButton: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  preg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  pregunta: {
    color: "red",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    padding: 50,
    marginTop: 20,
    borderRadius: 20,
  },
  text: {
    // marginVertical: 20,
    fontSize: 22,
    color: "white",
    fontWeight: "bold",
  },
  TouchableOpacity: {
    backgroundColor: "black",
    alignSelf: "stretch",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    
  },
  checkbox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  checkboxx: {
    marginTop: 15,
  },
  imgPerfil: {
    width: hp('25%'),
    height: hp('25%'),
    borderRadius: hp("30%"),
    borderColor: "rgb(0,140,207)",
    borderWidth: wp('0.8%'),
    marginTop: hp('3%'),
    backgroundColor:"#e1e1e1"
  },
  imgAdd: {
    width: wp("15%"),
    height: wp("15%"),
    marginLeft: wp("35%"),
    marginTop: hp('-13%'),
    borderWidth: hp('0.5%'),
    borderColor: "rgb(189,191,198)",
    borderRadius: 50,
  },
});