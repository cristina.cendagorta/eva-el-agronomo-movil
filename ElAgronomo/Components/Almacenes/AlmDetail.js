import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView,
    Modal,
    Button,
    ActivityIndicator,
    FlatList,
  } from "react-native";
  import React, { useState, useEffect, useRef,useMemo } from "react";
  import { useNavigation } from "@react-navigation/core";
  import { useDispatch, useSelector } from "react-redux";
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

  //COMPONENTE PARA MOSTRAR EL DETALLE DEL ALMACEN
const AlmDetail = (item) => {
  const navigation = useNavigation();
    const establecimientos = useSelector((store) => store.establecimiento);
    const geo = JSON.parse(item.route.params.geoposicion) //aca me traigo la info de si tengo locacion del almacen
console.log("item",item.route.params )
  return(

    <ScrollView style={{backgroundColor:"white"}}>
            <Text style={{
        marginLeft:wp("3%"),
      color:"grey",
      fontSize:hp("4%"),
      fontWeight:"bold",
      marginBottom:hp("5%")
      }}>
    Detalle del Almacén
    </Text>
        
        <View>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Nombre
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.nombre} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Abreviatura
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.abreviatura} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  {/* <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Establecimiento
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {establecimientos[item.route.params.establecimiento_id - 1].nombre} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View> */}
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Descripción
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.descripcion} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Tipo de Almacén
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.detalle_tipo_almacen} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Latitud
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {geo != null ?geo[0].latitude : null} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey", 
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%") 
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Longitud
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {geo != null ?geo[0].longitude : null} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Observaciones
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.observaciones} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  <View style={{
      marginTop:hp("1%")
      }}>
    <Text style={{
        marginLeft:wp("3%"),
      color:"black",
      fontSize:hp("3%"),
      fontWeight:"bold",
    //   marginBottom:hp("5%")
      }}>
    Establecimiento
    </Text>
    <Text style={{
      color:"grey",
      marginLeft:wp("3%"),
      fontSize:hp("2.5%"),
      marginTop:hp("1%")
      }}>
   {item.route.params.establecimiento_id} 
    </Text>
    <View style={{
        marginTop:hp("1%"),
      height:hp("0.2%"),
      backgroundColor:"grey",
      }}>
  </View>
  </View>
  

  {item.route.params.geoposicion != null ?
  <View style={{alignSelf:"center", justifyContent:"center", marginTop:hp("2%"), marginBottom:hp("2%")}} >
    <TouchableOpacity onPress={() => navigation.navigate("MarkerMap", item)} style={{ backgroundColor:"green", width:wp("45%"), height:hp("8%"), borderRadius:wp("5%")}}>
      <View style={{ alignItems: 'center'}}>
        <Text
        
          style={{  
            fontSize: hp("2.8%"),
            color:"white",
            marginTop:hp("2%")                       
            
          }}>
          Ver en el Mapa
        </Text>
      </View>
    </TouchableOpacity>
    </View>
    : null
  }
</ScrollView>
)
}

const styles = StyleSheet.create({
    container: {
      margin: 4,
      padding: 30
    }
  })


export default AlmDetail;