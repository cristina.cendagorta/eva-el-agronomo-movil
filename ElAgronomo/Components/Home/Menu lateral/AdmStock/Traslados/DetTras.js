import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as ImagePicker from "expo-image-picker";
import { Ionicons } from "@expo/vector-icons";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  Modal,
  Button,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import { useIsFocused } from "@react-navigation/native";
import { LogBox } from 'react-native';
import { getInsumo, getUnidad, newDetCom } from "../../../../../Redux/actions";
import Select2 from "react-native-select-two"
// import HeaderBar from "../Utils/HeaderBar";
// import SimpleModal1 from "./../Alerts/SingUp/SimpleModalok";
// import SimpleModal2 from "../Alerts/SingUp/SimpleModalok2.js";
// import SimpleModal3 from "../Alerts/SingUp//SimpleModalname.js";
// import SimpleModal4 from "../Alerts/SingUp//SimpleModalLastname.js";
// import SimpleModal5 from "../Alerts/SingUp//SimpleModalmail.js";
// import SimpleModal6 from "../Alerts/SingUp//SimpleModalpass.js";
// import SimpleModal7 from "../Alerts/SingUp//SimpleModalphone.js";
// import SimpleModal8 from "../Alerts/SingUp//SimpleModalterms.js";
// import SimpleModal9 from "../Alerts/SingUp//SimpleModalrole.js";
// import SimpleModal40 from "../Alerts/SingUp//SimpleModalsamemail.js"; 

import { useDispatch, useSelector } from "react-redux";


const DetCom = (id) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  // const insumos = useSelector((store) => store.insumo);
  // const unidades = useSelector((store) => store.unidades);
  const encabezado = useSelector((store) => store.encabcompra);
  const ins = useSelector((store) => store.getexistins);
  const isFocused = useIsFocused();

  useEffect(() => {
    if(isFocused){ 
      dispatch(getInsumo())
      dispatch(getUnidad())
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
  } 
  }, [,isFocused]);
  const [isLoading, setLoading] = useState(false);
  setTimeout(function(){
setLoading(true)
  }, 1500);


  console.log("ins",ins)


  // const respuesta = useSelector((store) => store.responseReg)
console.log("llega2",encabezado)

const insumos = [
  {
    "abreviatura": "H20",
    "abreviatura_tipo_erogacion": "ACA",
    "activo": true,
    "codigo_externo": "1234f",
    "detalle_familia": "AGROQUIMICOS",
    "detalle_rubro_insumo": "GRANOS",
    "detalle_subfamilia": "CURASEMILLAS",
    "detalle_tarea": "SIEMBRA",
    "detalle_unidad": "KILOGRAMO",
    "id": 1,
    "lote_control": true,
    "nombre": "Agua",
    "nombre_tipo_erogacion": "ACARREOS",
    "reposicion_alerta": true,
    "reposicion_alerta_email": "user@example.com",
    "reposicion_cantidad": 10,
    "reposicion_control": true,
    "vencimiento_control": true,
  },
  {
    "abreviatura": "GLF",
    "abreviatura_tipo_erogacion": "ATE",
    "activo": true,
    "codigo_externo": "GLF111",
    "detalle_familia": "AGROQUIMICOS",
    "detalle_rubro_insumo": "INSUMOS",
    "detalle_subfamilia": "HERBICIDAS",
    "detalle_tarea": "COSECHA",
    "detalle_unidad": "GALON",
    "id": 2,
    "lote_control": true,
    "nombre": "GLIFOSATO",
    "nombre_tipo_erogacion": "APLICACION TERRESTRE",
    "reposicion_alerta": true,
    "reposicion_alerta_email": "ASD@gmail.com",
    "reposicion_cantidad": 30,
    "reposicion_control": true,
    "vencimiento_control": true,
  },
 {
    "abreviatura": "string",
    "abreviatura_tipo_erogacion": "AAE",
    "activo": true,
    "codigo_externo": "string",
    "detalle_familia": "COMBUSTIBLES Y LUBRICANTES",
    "detalle_rubro_insumo": "INSUMOS",
    "detalle_subfamilia": "CURASEMILLAS",
    "detalle_tarea": "SIEMBRA",
    "detalle_unidad": "METRO CUBICO",
    "id": 3,
    "lote_control": true,
    "nombre": "string",
    "nombre_tipo_erogacion": "APLICACION AEREA",
    "reposicion_alerta": true,
    "reposicion_alerta_email": "user@example.com",
    "reposicion_cantidad": 100,
    "reposicion_control": true,
    "vencimiento_control": true,
  },
]


const unidades = [
   {
    "detalle_unidad": "KILOGRAMO",
    "id": 1,
  },
   {
    "detalle_unidad": "METRO CUBICO",
    "id": 2,
  },
   {
    "detalle_unidad": "GALON",
    "id": 3,
  },
   {
    "detalle_unidad": "ONZA",
    "id": 4,
  },
   {
    "detalle_unidad": "LITRO",
    "id": 5,
  },
   {
    "detalle_unidad": "QUINTAL",
    "id": 6,
  },
   {
    "detalle_unidad": "TONELADA",
    "id": 7,
  },
   {
    "detalle_unidad": "HECTAREA",
    "id": 8,
  },
   {
    "detalle_unidad": "UNIDAD",
    "id": 9,
  },
]

if(insumos != null){
  var insumoss = insumos?.map( item => { 
    return { name: item.nombre, id:item.id }; 
  });
}


  // ALERTAS:

  // REGISTRO USUARIO OK
//   const [isModalVisible1, setisModalVisible1] = useState(false);
//   const [chooseData1, setchooseData1] = useState();

//   const changeModalVisible1 = (bool) => {
//     setisModalVisible1(bool);
//   };

//   const setData1 = (data) => {
//     setchooseData1(data);
//   };

//   // REGISTRO TRANSPORTISTA OK
//   const [isModalVisible2, setisModalVisible2] = useState(false);
//   const [chooseData2, setchooseData2] = useState();

//   const changeModalVisible2 = (bool) => {
//     setisModalVisible2(bool);
//   };

//   const setData2 = (data) => {
//     setchooseData2(data);
//   };


//   // NOMBRE MAL INGRESADO
//   const [isModalVisible3, setisModalVisible3] = useState(false);
//   const [chooseData3, setchooseData3] = useState();

//   const changeModalVisible3 = (bool) => {
//     setisModalVisible3(bool);
//   };

//   const setData3 = (data) => {
//     setchooseData3(data);
//   };

//   // APELLIDO MAL INGRESADO

//   const [isModalVisible4, setisModalVisible4] = useState(false);
//   const [chooseData4, setchooseData4] = useState();

//   const changeModalVisible4 = (bool) => {
//     setisModalVisible4(bool);
//   };

//   const setData4 = (data) => {
//     setchooseData4(data);
//   };

//   //MAIL MAL INGRESADO
//   const [isModalVisible5, setisModalVisible5] = useState(false);
//   const [chooseData5, setchooseData5] = useState();

//   const changeModalVisible5 = (bool) => {
//     setisModalVisible5(bool);
//   };

//   const setData5 = (data) => {
//     setchooseData5(data);
//   };

//   // CONTRASEÑA MAL INGRESADA

//   const [isModalVisible6, setisModalVisible6] = useState(false);
//   const [chooseData6, setchooseData6] = useState();

//   const changeModalVisible6 = (bool) => {
//     setisModalVisible6(bool);
//   };

//   const setData6 = (data) => {
//     setchooseData6(data);
//   };
//   // TELEFONO MAL INGRESADO

//   const [isModalVisible7, setisModalVisible7] = useState(false);
//   const [chooseData7, setchooseData7] = useState();

//   const changeModalVisible7 = (bool) => {
//     setisModalVisible7(bool);
//   };

//   const setData7 = (data) => {
//     setchooseData7(data);
//   };
//   // NO ACEPTA TERMINOS
//   const [isModalVisible8, setisModalVisible8] = useState(false);
//   const [chooseData8, setchooseData8] = useState();

//   const changeModalVisible8 = (bool) => {
//     setisModalVisible8(bool);
//   };

//   const setData8 = (data) => {
//     setchooseData8(data);
//   };

//   // NO ELIJE ROL
//   const [isModalVisible9, setisModalVisible9] = useState(false);
//   const [chooseData9, setchooseData9] = useState();

//   const changeModalVisible9 = (bool) => {
//     setisModalVisible9(bool);
//   };

//   const setData9 = (data) => {
//     setchooseData9(data);
//   };

//   // MAIL REPETIDO
//   const [isModalVisible40, setisModalVisible40] = useState(false);
//   const [chooseData40, setchooseData40] = useState();

//   const changeModalVisible40 = (bool) => {
//     setisModalVisible40(bool);
//   };

//   const setData40 = (data) => {
//     setchooseData40(data);
//   };

  


  // useEffect(()=>{
  //   //console.log('aqui esta la respuestaaaa:',respuesta);
  //   if(respuesta?.role === true){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileUser");
  //     changeModalVisible1(true)
  //   }if(respuesta?.role === false){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileCarrier");
  //     changeModalVisible2(true)
  //   }if(respuesta?.role === 1){
  //     // alert('El mail ingresado ya se encuentra en uso!')
  //     changeModalVisible40(true)
  //   }
  // },[respuesta]);




  const [reg, setReg] = useState({
    cant: "",
    pre: "",
    lote: "",
    fec: "",
    obs:"",
    uni:"",
  });

  const [place, setPlace] = useState({
    prod: "Producto",
    venc:"",
    lote:"",
    uni:"",

  });

  console.log("lote", place.prod)



  // const ChangeInput = (e) => {
  //   setReg({
  //     // y sino es  generos y platforms, directamente pongo lo que escribo en el input
  //     ...reg,
  //     [e.target.name]: e.target.value,
  //   });
  // };

  const handleSubmit = (e) => {
    e.preventDefault();

    
      // obj.route.params.establecimiento_tipo_id = reg.tipo,
      // obj.route.params.direccion = reg.direccion,
      // obj.route.params.localidad = reg.localidad,
      // obj.route.params.provincia = reg.provincia,
      // obj.route.params.pais = reg.pais,
      // obj.route.params.zona_id = reg.zona,
      // obj.route.params.observaciones = reg.observaciones;

      const obj = {
        // establecimiento_id: Number(reg.est),
        insumo_id: Number(reg.prod),
        unidad_id:Number(reg.uni),
        cantidad:Number(reg.cant),
        precio_unitario:Number(reg.pre),
        // nro_lote:reg.lote,
        fecha_vencimiento:reg.fec,
        observaciones:reg.obs,
        encabezado_movimiento_id:id.route.params
    };




    // validaciones 
//     if (!obj.name ) {
//       changeModalVisible3(true)
//       return
//   }
//   if (!obj.lastName) {
//       // alert("Por favor escribe el Apellido correctamente!")
//       changeModalVisible4(true)
//       return
//   }
//   if (!obj.eMail.includes('.com') || !obj.eMail.includes('@')  ) {
//     changeModalVisible5(true)
//     // alert("Por favor escribe un correo electrónico válido!")
//     return
// } if (!obj.password) {
//   changeModalVisible6(true)
//   // alert("Por favor escribe una Contraseña válida!")
//   return
// }
// if (!obj.phone) {
//   changeModalVisible7(true)
//       // alert("Por favor escribe un número de telefono válido!")
//       return
//   }
//   if (!obj.business) {
//     changeModalVisible8(true)
//     // alert("Debes aceptar los términos para poder registrarte!")
//     return
//   }if (!obj.secret) {
//     changeModalVisible9(true)
//   // alert("Por favor elije un Rol!")
//   return
// }



dispatch(newDetCom(obj));
 console.log("Estoy enviado", obj);
    setReg({
      cant: "",
      pre: "",
      lote: "",
      fec: "",
      obs:"",
      uni:"",
    });
navigation.navigate("Traslados",id.route.params)
//     console.log(obj);
};

  //funciones para cambiar e.value de los inputs
  const handelChangeProd = (name) => {
    console.log(name)
    setReg({
      ...reg,
      uni: name,
    });
    const filtrado = insumos?.filter((f) => f.id === name)
    console.log("filtrado",filtrado)

    const filtrado2 = unidades?.filter((f) => f.detalle_unidad === filtrado[0].detalle_unidad)
    console.log("filtra",filtrado2, unidades)
    setReg({
      ...reg,
      prod:name,
      uni: filtrado2[0].id,
    });
    setPlace({
      ...place,
      prod: filtrado[0].nombre,
      uni:filtrado2[0].detalle_unidad,
      venc:filtrado[0].vencimiento_control,
      lote:filtrado[0].lote_control
    });
}
  const handelChangeCant = (name) => {
    setReg({
      ...reg,
      cant: name,
    });
  };
  const handelChangePre = (name) => {
    setReg({
      ...reg,
      pre: name,
    });
  };
  const handelChangeLote = (name) => {
    setReg({
      ...reg,
      lote: name,
    });
  };
  const handelChangeVenc = (name) => {
    setReg({
      ...reg,
      fec: name,
    });
  };
  const handelChangeObs = (name) => {
    setReg({
      ...reg,
      obs: name,
    });
  };
  



  

  {if(isLoading === false || unidades === null  ){     
    return(
  <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
  <ActivityIndicator size="large" color="#007d3c" />
</View>
)
}
}

{
  if(isLoading === true && unidades !== null ){


  return (
    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : null}
    keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
    <View style={{ flex: 1,  backgroundColor: 'white'  }}>
      {/* <View style={{marginTop:hp("-2%"),marginLeft:wp("0%"),marginBottom:hp("0%")}}>
        <HeaderBar  screen={'null'} style={{color:"white"}}/>
        </View> */}
    <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffffff" }}
      showsVerticalScrollIndicator={false}
    >
      
      {/* Brand View */}
      {/* <ImageBackground
        source={require("../Home/logo.png")}
        resizeMode= "contain"
        style={{
            display:'flex',
            marginTop:  hp('-18%'),
          height: hp('60%') ,
          width: wp('110%') ,
          alignSelf: "center",
        }}
      >
      </ImageBackground> */}
      {/* Botton View */}
      <View style={styles.bottonView}>
        {/* Welcome View */}
        <View style={{ display: "flex", alignSelf: "center", marginTop:hp("7%"), marginBottom:hp("5%"), width:wp("90%")  }}>
          <Text style={{ color: "#151f27", fontSize: hp("3.5%"),fontWeight: '600', marginTop: hp("3%"),textAlign:"center" }}>
            Completa la información de los insumos a trasladar
          </Text>
        
        </View>
        {/* inputs */}
        <View style={styles.FormView}>
        <View style={{flexDirection:"row"}}>
        <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Insumo"
          title={place.prod}
          data={insumoss != null ? insumoss : null}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeProd(name[0])}
        />
          {/* <TextInput
            name="establecimiento"
            value={reg.est}
            onChangeText={(name) => handelChangeEst(name)}
            placeholder="Producto"
            style={styles.TextInput}
          ></TextInput> */}
          <TouchableOpacity style={styles.Button1} onPress={() => navigation.navigate("AddInsumo") } >
            {/* <Text style={styles.ButtonText} >
              A
            </Text> */}
             <Ionicons name="add-outline" size={23} color="white"/>

          </TouchableOpacity>
          </View>
          { place.uni !== "" ?
                <TextInput
                // keyboardType={'phone-pad'}
                value={`${place.uni.toString()}`}
                
                // onChangeText={(name) => handelChangeCod(name)}
                name="cantidad"
                // placeholder={place.uni}
                editable={false}
                style={styles.TextInput}
              ></TextInput>

              : null

          }
    

          <TextInput
            keyboardType={'phone-pad'}
            value={reg.cant}
            onChangeText={(name) => handelChangeCant(name)}
            name="cantidad"
            placeholder="Cantidad"
            style={styles.TextInput}
          ></TextInput>
          {/* <TextInput
           keyboardType={'phone-pad'}
            value={reg.pre}
            onChangeText={(name) => handelChangePre(name)}
            name="precio unitario"
            placeholder="Precio Unitario"
            style={styles.TextInput}
          ></TextInput> */}
                {
         place.venc === true ?
                     <TextInput
                      keyboardType={'phone-pad'}
            value={reg.fec}
            onChangeText={(name) => handelChangeVenc(name)}
            name="Fecha de Vencimiento"
            placeholder="Fecha de Vencimiento"
            style={styles.TextInput}
          ></TextInput>
          : null
        }
          {
         place.lote === true ?
                 <TextInput
                 keyboardType={'phone-pad'}
                 value={reg.lote}
                 onChangeText={(name) => handelChangeLote(name)}
                 name="Número de Lote"
                 placeholder="Número de Lote"
                 style={styles.TextInput}
               ></TextInput>
               : null
          
 }
 
<TextInput
            value={reg.obs}
            onChangeText={(name) => handelChangeObs(name)}
            name="Observaciones"
            placeholder="Observaciones"
            multiline={true}
            style={styles.TextInput2}
          ></TextInput>


          <TouchableOpacity style={styles.Button} onPress={handleSubmit } >
            <Text style={styles.ButtonText} >
              Agregar!
            </Text>
            {/* <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible1}
                  nRequestClose={() => changeModalVisible1(false)}
                >
                  <SimpleModal1
                    changeModalVisible1={changeModalVisible1}
                    setData1={setData1}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible2}
                  nRequestClose={() => changeModalVisible2(false)}
                >
                  <SimpleModal2
                    changeModalVisible2={changeModalVisible2}
                    setData2={setData2}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible3}
                  nRequestClose={() => changeModalVisible3(false)}
                >
                  <SimpleModal3
                    changeModalVisible3={changeModalVisible3}
                    setData3={setData3}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible4}
                  nRequestClose={() => changeModalVisible4(false)}
                >
                  <SimpleModal4
                    changeModalVisible4={changeModalVisible4}
                    setData4={setData4}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible5}
                  nRequestClose={() => changeModalVisible5(false)}
                >
                  <SimpleModal5
                    changeModalVisible5={changeModalVisible5}
                    setData5={setData5}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible6}
                  nRequestClose={() => changeModalVisible6(false)}
                >
                  <SimpleModal6
                    changeModalVisible6={changeModalVisible6}
                    setData6={setData6}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible7}
                  nRequestClose={() => changeModalVisible7(false)}
                >
                  <SimpleModal7
                    changeModalVisible7={changeModalVisible7}
                    setData7={setData7}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible8}
                  nRequestClose={() => changeModalVisible8(false)}
                >
                  <SimpleModal8
                    changeModalVisible8={changeModalVisible8}
                    setData8={setData8}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible9}
                  nRequestClose={() => changeModalVisible9(false)}
                >
                  <SimpleModal9
                    changeModalVisible9={changeModalVisible9}
                    setData9={setData9}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible40}
                  nRequestClose={() => changeModalVisible40(false)}
                >
                  <SimpleModal40
                    changeModalVisible40={changeModalVisible40}
                    setData40={setData40}
                  />
                  
                  </Modal> */}
          </TouchableOpacity>
          
         
        </View>

        
      </View>
    </ScrollView>
    </View>
    </KeyboardAvoidingView>
  );
}
}
};

export default DetCom;

const styles = StyleSheet.create({
  brandView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  brandViewText: {
    color: "#FFC107",
    fontSize: 45,
    fontWeight: "bold",
    textTransform: "uppercase",
    // justifyContent:'flex-start'
  },
  bottonView: {
    flex: 1.5,
    backgroundColor: "white",
    bottom: 50,
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
  },
  FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: -20,
  },
  TextInput: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("2.9%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  TextInput2: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("20.7%"),
    textAlignVertical:"top",
    borderRadius: 10,
    paddingTop:15,
    paddingLeft: hp("2.5%"),
    marginTop: hp("2.9%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  Button: {
    width: "90%",
    color: "black",
    height: 52,
    backgroundColor: "rgb(24,116,28)",
    borderRadius: 10,
    marginTop: hp("2%"),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  Button1: {
    width: wp("8%"),
    height: wp("8%"),
    color: "black",
    backgroundColor: "rgb(24,116,28)",
    borderRadius: wp("10%"),
    marginLeft: wp("-10%"),
    marginRight:wp("2%"),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
    marginTop:hp("4%")
    // flexDirection:"row",
  },
  ButtonText: {
    // fontWeight: "bold",
    fontSize: hp("3%"),
    color: "white",
  },
  SingUpText: {
    color: "#4632a1",
    fontSize: 20,
  },
  TextButton: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  preg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  pregunta: {
    color: "red",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    padding: 50,
    marginTop: 20,
    borderRadius: 20,
  },
  text: {
    // marginVertical: 20,
    fontSize: 22,
    color: "white",
    fontWeight: "bold",
  },
  TouchableOpacity: {
    backgroundColor: "black",
    alignSelf: "stretch",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    
  },
  checkbox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  checkboxx: {
    marginTop: 15,
  },
  imgPerfil: {
    width: hp('25%'),
    height: hp('25%'),
    borderRadius: hp("30%"),
    borderColor: "rgb(0,140,207)",
    borderWidth: wp('0.8%'),
    marginTop: hp('3%'),
    backgroundColor:"#e1e1e1"
  },
  imgAdd: {
    width: wp("15%"),
    height: wp("15%"),
    marginLeft: wp("35%"),
    marginTop: hp('-13%'),
    borderWidth: hp('0.5%'),
    borderColor: "rgb(189,191,198)",
    borderRadius: 50,
  },
});