import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ScrollView,
    ImageBackground,
    Dimensions,
    KeyboardAvoidingView,
    SafeAreaView,
    Modal,
    Button,
    ActivityIndicator,
    FlatList,
  } from "react-native";
  import { useNavigation } from "@react-navigation/core";
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import empresa from "../../Components/Configuration/empresa.png"
import { getLote } from "../../Redux/actions";
import { useIsFocused } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import React, { useState, useEffect } from "react";
import { getEstablecimiento } from "../../Redux/actions";
import { ListItem, Icon } from '@rneui/themed'

const InsxFam = (famid) => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    // me traigo insumos y familias
    const familias = useSelector((store) => store.familias);
    const insumos = useSelector((store) => store.insumo);
    const isFocused = useIsFocused();
    console.log("insumos",famid)
    // filtro los insumos segun la familia
    const filtrado = insumos.filter((f) => f.detalle_familia === famid.route.params.detalle_familia)
    console.log("famid",famid.route.params, "insumos",insumos)

  return(
    <SafeAreaView style={{marginTop:30}}>
    <FlatList
           data={filtrado} // muestro los insumos de esa familia
           keyExtractor={ (item) => item.id }
           renderItem={({item, index}) =>{
             return (
               <TouchableOpacity
                 onPress={() => 
                   navigation.navigate('InsDetail', item)
               }>
               <ListItem>
               <Icon name='business' />
               {/* muestro sus detalles */}
         <ListItem.Content> 
           <ListItem.Title>{item.nombre}</ListItem.Title>
           <ListItem.Subtitle>{item.abreviatura}</ListItem.Subtitle>
         </ListItem.Content>
         <ListItem.Chevron color="black" />
       </ListItem>
             </TouchableOpacity>
             )
           }}
           // <ListItemEst item ={item} />}
           ItemSeparatorComponent= { () =>       <View style={{ marginBottom: 10, borderColor:'#00000020', borderWidth:0.5}}></View>}
           ListHeaderComponent = { () => <Text style={{fontWeight:"bold", fontSize:18,marginBottom:20, textAlign:"center"}}>Insumos</Text>}
           // <ListItem item = {item} />}
               />
   </SafeAreaView>
)


} 

export default InsxFam;