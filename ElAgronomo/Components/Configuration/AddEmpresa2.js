import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as ImagePicker from "expo-image-picker";
import { Ionicons } from "@expo/vector-icons";
import { newempresa } from "../../Redux/actions";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  Modal,
  Button,
  KeyboardAvoidingView,
  ActivityIndicator
} from "react-native";
import { LogBox } from 'react-native';
import Select2 from "react-native-select-two"
// import HeaderBar from "../Utils/HeaderBar";
// import SimpleModal1 from "./../Alerts/SingUp/SimpleModalok";
// import SimpleModal2 from "../Alerts/SingUp/SimpleModalok2.js";
// import SimpleModal3 from "../Alerts/SingUp//SimpleModalname.js";
// import SimpleModal4 from "../Alerts/SingUp//SimpleModalLastname.js";
// import SimpleModal5 from "../Alerts/SingUp//SimpleModalmail.js";
// import SimpleModal6 from "../Alerts/SingUp//SimpleModalpass.js";
// import SimpleModal7 from "../Alerts/SingUp//SimpleModalphone.js";
// import SimpleModal8 from "../Alerts/SingUp//SimpleModalterms.js";
// import SimpleModal9 from "../Alerts/SingUp//SimpleModalrole.js";
// import SimpleModal40 from "../Alerts/SingUp//SimpleModalsamemail.js"; 


import CheckBox from "expo-checkbox";

// import CheckBox from "@react-native-community/checkbox";
// import { ModalPicker } from "./ModalPicker";
import { useDispatch, useSelector } from "react-redux";
import { getRubEmp } from "../../Redux/actions";
import { getMon } from "../../Redux/actions";


const AddEmpresa2 = (obj) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  //ME TRAIGO RUBROS Y MONEDAS PARA LOS SELECT
  const rubros = useSelector((store) => store.rubemp);
  const monedas = useSelector((store) => store.monedas);

  useEffect(() => {
    setTimeout(function(){ 
      setLoading(true) 
        }, 1000);
   
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
}, [])

console.log("obj",rubros,monedas )

const [isLoading, setLoading] = useState(false);
//MAPEO LA INFO PARA PODER PONERLA DENTRO DEL SELECT
var rubross = rubros?.map( item => { 
  return { name: item.detalle_rubro_empresa, id:item.id }; 
});
var monedass = monedas?.map( item => { 
  return { name: item.detalle_moneda, id:item.id }; 
});

  // ALERTAS:

  // REGISTRO USUARIO OK
//   const [isModalVisible1, setisModalVisible1] = useState(false);
//   const [chooseData1, setchooseData1] = useState();

//   const changeModalVisible1 = (bool) => {
//     setisModalVisible1(bool);
//   };

//   const setData1 = (data) => {
//     setchooseData1(data);
//   };

//   // REGISTRO TRANSPORTISTA OK
//   const [isModalVisible2, setisModalVisible2] = useState(false);
//   const [chooseData2, setchooseData2] = useState();

//   const changeModalVisible2 = (bool) => {
//     setisModalVisible2(bool);
//   };

//   const setData2 = (data) => {
//     setchooseData2(data);
//   };


//   // NOMBRE MAL INGRESADO
//   const [isModalVisible3, setisModalVisible3] = useState(false);
//   const [chooseData3, setchooseData3] = useState();

//   const changeModalVisible3 = (bool) => {
//     setisModalVisible3(bool);
//   };

//   const setData3 = (data) => {
//     setchooseData3(data);
//   };

//   // APELLIDO MAL INGRESADO

//   const [isModalVisible4, setisModalVisible4] = useState(false);
//   const [chooseData4, setchooseData4] = useState();

//   const changeModalVisible4 = (bool) => {
//     setisModalVisible4(bool);
//   };

//   const setData4 = (data) => {
//     setchooseData4(data);
//   };

//   //MAIL MAL INGRESADO
//   const [isModalVisible5, setisModalVisible5] = useState(false);
//   const [chooseData5, setchooseData5] = useState();

//   const changeModalVisible5 = (bool) => {
//     setisModalVisible5(bool);
//   };

//   const setData5 = (data) => {
//     setchooseData5(data);
//   };

//   // CONTRASEÑA MAL INGRESADA

//   const [isModalVisible6, setisModalVisible6] = useState(false);
//   const [chooseData6, setchooseData6] = useState();

//   const changeModalVisible6 = (bool) => {
//     setisModalVisible6(bool);
//   };

//   const setData6 = (data) => {
//     setchooseData6(data);
//   };
//   // TELEFONO MAL INGRESADO

//   const [isModalVisible7, setisModalVisible7] = useState(false);
//   const [chooseData7, setchooseData7] = useState();

//   const changeModalVisible7 = (bool) => {
//     setisModalVisible7(bool);
//   };

//   const setData7 = (data) => {
//     setchooseData7(data);
//   };
//   // NO ACEPTA TERMINOS
//   const [isModalVisible8, setisModalVisible8] = useState(false);
//   const [chooseData8, setchooseData8] = useState();

//   const changeModalVisible8 = (bool) => {
//     setisModalVisible8(bool);
//   };

//   const setData8 = (data) => {
//     setchooseData8(data);
//   };

//   // NO ELIJE ROL
//   const [isModalVisible9, setisModalVisible9] = useState(false);
//   const [chooseData9, setchooseData9] = useState();

//   const changeModalVisible9 = (bool) => {
//     setisModalVisible9(bool);
//   };

//   const setData9 = (data) => {
//     setchooseData9(data);
//   };

//   // MAIL REPETIDO
//   const [isModalVisible40, setisModalVisible40] = useState(false);
//   const [chooseData40, setchooseData40] = useState();

//   const changeModalVisible40 = (bool) => {
//     setisModalVisible40(bool);
//   };

//   const setData40 = (data) => {
//     setchooseData40(data);
//   };

  


  // useEffect(()=>{
  //   //console.log('aqui esta la respuestaaaa:',respuesta);
  //   if(respuesta?.role === true){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileUser");
  //     changeModalVisible1(true)
  //   }if(respuesta?.role === false){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileCarrier");
  //     changeModalVisible2(true)
  //   }if(respuesta?.role === 1){
  //     // alert('El mail ingresado ya se encuentra en uso!')
  //     changeModalVisible40(true)
  //   }
  // },[respuesta]);




  const [reg, setReg] = useState({
    calle: "",
    nro: "",
    localidad: "",
    provincia: "",
    cod: "",
    rubro:"",
    fecha: "",
    mon1:"",
    mon2:"",
  });

  const [place, setPlace] = useState({
    rubro: "Rubro",
    mon1: "Moneda Primaria",
    mon2: "Moneda Secundaria",
  });

  const [check, setCheck] = useState(false);

  const CheckboxChange = (e) => {
    setCheck(!check);
  };

  // const ChangeInput = (e) => {
  //   setReg({
  //     // y sino es  generos y platforms, directamente pongo lo que escribo en el input
  //     ...reg,
  //     [e.target.name]: e.target.value,
  //   });
  // };

  const handleSubmit = (e) => {
    e.preventDefault();

    //ME TRAIGO ALGUNOS VALUES POR PARAMS, Y LOS OTROS LOS COMPLETO
      obj.route.params.direccion_calle = reg.calle,
      obj.route.params.direccion_nro = reg.nro,
      obj.route.params.direccion_localidad = reg.localidad,
      obj.route.params.direccion_provincia = reg.provincia,
      obj.route.params.direccion_cod_postal = reg.cod,
      obj.route.params.fecha_cierre = reg.fecha,  
      obj.route.params.moneda_primaria_id = Number(reg.mon1),
      obj.route.params.moneda_secundaria_id = Number(reg.mon2);
      obj.route.params.rubro_empresa_id = Number(reg.rubro);

    

    // validaciones 
//     if (!obj.name ) {
//       changeModalVisible3(true)
//       return
//   }
//   if (!obj.lastName) {
//       // alert("Por favor escribe el Apellido correctamente!")
//       changeModalVisible4(true)
//       return
//   }
//   if (!obj.eMail.includes('.com') || !obj.eMail.includes('@')  ) {
//     changeModalVisible5(true)
//     // alert("Por favor escribe un correo electrónico válido!")
//     return
// } if (!obj.password) {
//   changeModalVisible6(true)
//   // alert("Por favor escribe una Contraseña válida!")
//   return
// }
// if (!obj.phone) {
//   changeModalVisible7(true)
//       // alert("Por favor escribe un número de telefono válido!")
//       return
//   }
//   if (!obj.business) {
//     changeModalVisible8(true)
//     // alert("Debes aceptar los términos para poder registrarte!")
//     return
//   }if (!obj.secret) {
//     changeModalVisible9(true)
//   // alert("Por favor elije un Rol!")
//   return
// }



dispatch(newempresa(obj));
//  console.log("Estoy enviado", obj);
    setReg({
      calle: "",
      nro: "",
      localidad: "",
      provincia: "",
      cod: "",
      rubro:"",
      fecha: "",
      mon1:"",
      mon2:"",
    });

    setPlace({
      rubro: "Rubro",
      mon1: "Moneda Primaria",
      mon2: "Moneda Secundaria",
    });
navigation.navigate("HomeStack")
//     console.log(obj);
};

  //funciones para cambiar e.value de los inputs

  const handelChangeCalle = (name) => {
    setReg({
      ...reg,
      calle: name,
    });
  };
  const handelChangeNro = (name) => {
    setReg({
      ...reg,
      nro: name,
    });
  };
  const handelChangeLocal = (name) => {
    setReg({
      ...reg,
      localidad: name,
    });
  };
  const handelChangeProv = (name) => {
    setReg({
      ...reg,
      provincia: name,
    });
  };

  const handelChangeCod = (name) => {
    setReg({
      ...reg,
      cod: name,
    });
  };

  const handelChangeFecha = (businessname) => {
    setReg({
      ...reg,
      fecha: businessname,
    });
  };
  const handelChangeRub = (name) => {
    console.log(name)
    setReg({
      ...reg,
      rubro: name,
    });

    const filtrado = rubros?.filter((f) => f.id === name)
    console.log("filtra",filtrado)
    setPlace({
      ...place,
      rubro: filtrado[0].detalle_rubro_empresa,
    });
  };
  const handelChangeMon1 = (name) => {
    console.log(name)
    setReg({
      ...reg,
      mon1: name,
    });

    const filtrado = monedas?.filter((f) => f.id === name)
    console.log("filtra",filtrado)
    setPlace({
      ...place,
      mon1: filtrado[0].detalle_moneda,
    });
  };

  const handelChangeMon2 = (name) => {
    console.log(name)
    setReg({
      ...reg,
      mon2: name,
    });

    const filtrado = monedas?.filter((f) => f.id === name)
    console.log("filtra",filtrado)
    setPlace({
      ...place,
      mon2: filtrado[0].detalle_moneda,
    });
  };
  
   ////--> IMAGE PICKER <-- ////
   const [selectedImage, setSelectedImage] = useState(null);

   let openImagePickerAsync = async () => {
     let permissionResult =
       await ImagePicker.requestMediaLibraryPermissionsAsync();
 
     if (permissionResult.granted === false) {
       alert("Se requiere el permiso para acceder a la cámara");
       return;
     }
 
     //Si es true va a venir a pickerResult
     const pickerResult = await ImagePicker.launchImageLibraryAsync({
       mediaTypes: ImagePicker.MediaTypeOptions.Images,
       allowsEditing: true,
       aspect: [4, 3],
       quality: 1,
     });
 
     if (pickerResult.cancelled !== true) {
       let newFile = {
         uri: pickerResult.uri,
         type: `logi/${pickerResult.uri.split(".")[1]}`,
         name: `logi.${pickerResult.uri.split(".")[1]}`,
       };
       handleUpload(newFile);
     }
   };
 
   const handleUpload = (image) => {
     const data = new FormData();
     data.append("file", image);
     data.append("upload_preset", "logiexpress");
     data.append("cloud_name", "elialvarez");
 
     fetch("https://api.cloudinary.com/v1_1/elialvarez/image/upload", {
       method: "post",
       body: data,
     })
       .then((res) => res.json())
       .then((data) => {
         //console.log(data)
         setSelectedImage(data.url);
       });
   };


   {if(isLoading === false || rubros === null  || monedas === null){     
    return(
  <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
  <ActivityIndicator size="large" color="#007d3c" /> 
</View>
)
}
}
if(rubros != null && isLoading === true && monedas != null){ 
  return (
    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : null}
    keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
    <View style={{ flex: 1,  backgroundColor: 'white'  }}>
      {/* <View style={{marginTop:hp("-2%"),marginLeft:wp("0%"),marginBottom:hp("0%")}}>
        <HeaderBar  screen={'null'} style={{color:"white"}}/>
        </View> */}
    <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffffff" }}
      showsVerticalScrollIndicator={false}
    >
      
      {/* Brand View */}
      {/* <ImageBackground
        source={require("../Home/logo.png")}
        resizeMode= "contain"
        style={{
            display:'flex',
            marginTop:  hp('-18%'),
          height: hp('60%') ,
          width: wp('110%') ,
          alignSelf: "center",
        }}
      >
      </ImageBackground> */}
      {/* Botton View */}
      <View style={styles.bottonView}>
        {/* Welcome View */}
        <View style={{ padding: 40, display: "flex", alignItems: "center" }}>
          <Text style={{ color: "#151f27", fontSize: hp("4.7%"),fontWeight: '600', marginTop: hp("3%") }}>
            Datos de tu empresa
          </Text>
        
        </View>
        {/* inputs */}
        <View style={styles.FormView}>
          <TextInput
            name="calle"
            value={reg.calle}
            onChangeText={(name) => handelChangeCalle(name)}
            placeholder="Dirección Calle"
            style={styles.TextInput}
          ></TextInput>

          <TextInput
           keyboardType={'phone-pad'}
            value={reg.nro}
            onChangeText={(name) => handelChangeNro(name)}
            name="nro"
            placeholder="Dirección Número"
            style={styles.TextInput}
          ></TextInput>
          <TextInput
          autoComplete="birthdate-full"
            icon="mail"
            value={reg.localidad}
            onChangeText={(name) => handelChangeLocal(name)}
            name="localidad"
            placeholder="Localidad"
            style={styles.TextInput}
          ></TextInput>

          <TextInput
            value={reg.provincia}
            onChangeText={(name) => handelChangeProv(name)}
            name="provincia"
            placeholder="Provincia"
            style={styles.TextInput}
          ></TextInput>
          <TextInput
            value={reg.cod}
            onChangeText={(name) => handelChangeCod(name)}
            name="cod"
            placeholder="Código Postal"
            style={styles.TextInput}
          ></TextInput>
          <TextInput
            value={reg.fecha}
            onChangeText={(secret) => handelChangeFecha(secret)}
            name="fecha"
            placeholder="Fecha de Cierre"
            style={styles.TextInput}
          ></TextInput>
                       <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Seleccionar Rubro"
          title={place.rubro}
          data={rubross}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeRub(name[0])}
        />
                     <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Seleccionar Moneda Primaria"
          title={place.mon1}
          data={monedass}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeMon1(name[0])}
        />
                     <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Seleccionar Moneda Secundaria"
          title={place.mon2}
          data={monedass}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeMon2(name[0])}
        />

          <TouchableOpacity style={styles.Button} onPress={handleSubmit } >
            <Text style={styles.ButtonText} >
              Crear!
            </Text>
            {/* <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible1}
                  nRequestClose={() => changeModalVisible1(false)}
                >
                  <SimpleModal1
                    changeModalVisible1={changeModalVisible1}
                    setData1={setData1}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible2}
                  nRequestClose={() => changeModalVisible2(false)}
                >
                  <SimpleModal2
                    changeModalVisible2={changeModalVisible2}
                    setData2={setData2}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible3}
                  nRequestClose={() => changeModalVisible3(false)}
                >
                  <SimpleModal3
                    changeModalVisible3={changeModalVisible3}
                    setData3={setData3}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible4}
                  nRequestClose={() => changeModalVisible4(false)}
                >
                  <SimpleModal4
                    changeModalVisible4={changeModalVisible4}
                    setData4={setData4}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible5}
                  nRequestClose={() => changeModalVisible5(false)}
                >
                  <SimpleModal5
                    changeModalVisible5={changeModalVisible5}
                    setData5={setData5}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible6}
                  nRequestClose={() => changeModalVisible6(false)}
                >
                  <SimpleModal6
                    changeModalVisible6={changeModalVisible6}
                    setData6={setData6}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible7}
                  nRequestClose={() => changeModalVisible7(false)}
                >
                  <SimpleModal7
                    changeModalVisible7={changeModalVisible7}
                    setData7={setData7}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible8}
                  nRequestClose={() => changeModalVisible8(false)}
                >
                  <SimpleModal8
                    changeModalVisible8={changeModalVisible8}
                    setData8={setData8}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible9}
                  nRequestClose={() => changeModalVisible9(false)}
                >
                  <SimpleModal9
                    changeModalVisible9={changeModalVisible9}
                    setData9={setData9}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible40}
                  nRequestClose={() => changeModalVisible40(false)}
                >
                  <SimpleModal40
                    changeModalVisible40={changeModalVisible40}
                    setData40={setData40}
                  />
                  
                  </Modal> */}
          </TouchableOpacity>
          
         
        </View>

        
      </View>
    </ScrollView>
    </View>
    </KeyboardAvoidingView>
  );
}
};

export default AddEmpresa2;

const styles = StyleSheet.create({
  brandView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  brandViewText: {
    color: "#FFC107",
    fontSize: 45,
    fontWeight: "bold",
    textTransform: "uppercase",
    // justifyContent:'flex-start'
  },
  bottonView: {
    flex: 1.5,
    backgroundColor: "white",
    bottom: 50,
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
  },
  FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: -20,
  },
  TextInput: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("2.9%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.4%"),
  },
  Button: {
    width: "90%",
    color: "black",
    height: 52,
    backgroundColor: "rgb(24,116,28)",
    borderRadius: 10,
    marginTop: hp("5%"),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  ButtonText: {
    // fontWeight: "bold",
    fontSize: hp("3%"),
    color: "white",
  },
  SingUpText: {
    color: "#4632a1",
    fontSize: 20,
  },
  TextButton: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  preg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  pregunta: {
    color: "red",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    padding: 50,
    marginTop: 20,
    borderRadius: 20,
  },
  text: {
    // marginVertical: 20,
    fontSize: 22,
    color: "white",
    fontWeight: "bold",
  },
  TouchableOpacity: {
    backgroundColor: "black",
    alignSelf: "stretch",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    
  },
  checkbox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  checkboxx: {
    marginTop: 15,
  },
  imgPerfil: {
    width: hp('25%'),
    height: hp('25%'),
    borderRadius: hp("30%"),
    borderColor: "rgb(0,140,207)",
    borderWidth: wp('0.8%'),
    marginTop: hp('3%'),
    backgroundColor:"#e1e1e1"
  },
  imgAdd: {
    width: wp("15%"),
    height: wp("15%"),
    marginLeft: wp("35%"),
    marginTop: hp('-13%'),
    borderWidth: hp('0.5%'),
    borderColor: "rgb(189,191,198)",
    borderRadius: 50,
  },
});