import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as ImagePicker from "expo-image-picker";
import { Ionicons } from "@expo/vector-icons";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  Modal,
  Button,
  Switch,
  KeyboardAvoidingView
} from "react-native";
import DatePicker from 'react-native-datepicker'
import { LogBox } from 'react-native';
import { newInsumo } from "../../Redux/actions";
// import HeaderBar from "../Utils/HeaderBar";
// import SimpleModal1 from "./../Alerts/SingUp/SimpleModalok";
// import SimpleModal2 from "../Alerts/SingUp/SimpleModalok2.js";
// import SimpleModal3 from "../Alerts/SingUp//SimpleModalname.js";
// import SimpleModal4 from "../Alerts/SingUp//SimpleModalLastname.js";
// import SimpleModal5 from "../Alerts/SingUp//SimpleModalmail.js";
// import SimpleModal6 from "../Alerts/SingUp//SimpleModalpass.js";
// import SimpleModal7 from "../Alerts/SingUp//SimpleModalphone.js";
// import SimpleModal8 from "../Alerts/SingUp//SimpleModalterms.js";
// import SimpleModal9 from "../Alerts/SingUp//SimpleModalrole.js";
// import SimpleModal40 from "../Alerts/SingUp//SimpleModalsamemail.js"; 


import CheckBox from "expo-checkbox";

// import CheckBox from "@react-native-community/checkbox";
// import { ModalPicker } from "./ModalPicker";
import { useDispatch, useSelector } from "react-redux";
// import { adminregister } from "../../Redux/actions/index";

const AddInsumo2 = (obj) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  // const respuesta = useSelector((store) => store.responseReg)

  useEffect(() => {
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
}, [])

console.log("obj",obj.route.params) 



  // ALERTAS:

  // REGISTRO USUARIO OK
//   const [isModalVisible1, setisModalVisible1] = useState(false);
//   const [chooseData1, setchooseData1] = useState();

//   const changeModalVisible1 = (bool) => {
//     setisModalVisible1(bool);
//   };

//   const setData1 = (data) => {
//     setchooseData1(data);
//   };

//   // REGISTRO TRANSPORTISTA OK
//   const [isModalVisible2, setisModalVisible2] = useState(false);
//   const [chooseData2, setchooseData2] = useState();

//   const changeModalVisible2 = (bool) => {
//     setisModalVisible2(bool);
//   };

//   const setData2 = (data) => {
//     setchooseData2(data);
//   };


//   // NOMBRE MAL INGRESADO
//   const [isModalVisible3, setisModalVisible3] = useState(false);
//   const [chooseData3, setchooseData3] = useState();

//   const changeModalVisible3 = (bool) => {
//     setisModalVisible3(bool);
//   };

//   const setData3 = (data) => {
//     setchooseData3(data);
//   };

//   // APELLIDO MAL INGRESADO

//   const [isModalVisible4, setisModalVisible4] = useState(false);
//   const [chooseData4, setchooseData4] = useState();

//   const changeModalVisible4 = (bool) => {
//     setisModalVisible4(bool);
//   };

//   const setData4 = (data) => {
//     setchooseData4(data);
//   };

//   //MAIL MAL INGRESADO
//   const [isModalVisible5, setisModalVisible5] = useState(false);
//   const [chooseData5, setchooseData5] = useState();

//   const changeModalVisible5 = (bool) => {
//     setisModalVisible5(bool);
//   };

//   const setData5 = (data) => {
//     setchooseData5(data);
//   };

//   // CONTRASEÑA MAL INGRESADA

//   const [isModalVisible6, setisModalVisible6] = useState(false);
//   const [chooseData6, setchooseData6] = useState();

//   const changeModalVisible6 = (bool) => {
//     setisModalVisible6(bool);
//   };

//   const setData6 = (data) => {
//     setchooseData6(data);
//   };
//   // TELEFONO MAL INGRESADO

//   const [isModalVisible7, setisModalVisible7] = useState(false);
//   const [chooseData7, setchooseData7] = useState();

//   const changeModalVisible7 = (bool) => {
//     setisModalVisible7(bool);
//   };

//   const setData7 = (data) => {
//     setchooseData7(data);
//   };
//   // NO ACEPTA TERMINOS
//   const [isModalVisible8, setisModalVisible8] = useState(false);
//   const [chooseData8, setchooseData8] = useState();

//   const changeModalVisible8 = (bool) => {
//     setisModalVisible8(bool);
//   };

//   const setData8 = (data) => {
//     setchooseData8(data);
//   };

//   // NO ELIJE ROL
//   const [isModalVisible9, setisModalVisible9] = useState(false);
//   const [chooseData9, setchooseData9] = useState();

//   const changeModalVisible9 = (bool) => {
//     setisModalVisible9(bool);
//   };

//   const setData9 = (data) => {
//     setchooseData9(data);
//   };

//   // MAIL REPETIDO
//   const [isModalVisible40, setisModalVisible40] = useState(false);
//   const [chooseData40, setchooseData40] = useState();

//   const changeModalVisible40 = (bool) => {
//     setisModalVisible40(bool);
//   };

//   const setData40 = (data) => {
//     setchooseData40(data);
//   };

  


  // useEffect(()=>{
  //   //console.log('aqui esta la respuestaaaa:',respuesta);
  //   if(respuesta?.role === true){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileUser");
  //     changeModalVisible1(true)
  //   }if(respuesta?.role === false){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileCarrier");
  //     changeModalVisible2(true)
  //   }if(respuesta?.role === 1){
  //     // alert('El mail ingresado ya se encuentra en uso!')
  //     changeModalVisible40(true)
  //   }
  // },[respuesta]);




  const [reg, setReg] = useState({
    mail: "",
    cant: "",
  });

  // esto es para los switchs
const [isEnabled, setIsEnabled] = useState(false);
const toggleSwitch = () => setIsEnabled(previousState => !previousState);
const [isEnabled1, setisEnabled1] = useState(false);
const toggleSwitch1 = () => setisEnabled1(previousState => !previousState);
const [isEnabled2, setisEnabled2] = useState(false);
const toggleSwitch2 = () => setisEnabled2(previousState => !previousState);
const [isEnabled3, setisEnabled3] = useState(false);
const toggleSwitch3 = () => setisEnabled3(previousState => !previousState);




  const [check, setCheck] = useState(false);

  const CheckboxChange = (e) => {
    setCheck(!check);
  };

  // const ChangeInput = (e) => {
  //   setReg({
  //     // y sino es  generos y platforms, directamente pongo lo que escribo en el input
  //     ...reg,
  //     [e.target.name]: e.target.value,
  //   });
  // };

  const handleSubmit = (e) => {
    e.preventDefault();

    //al objeto que ya tenia, le agrego nuevos valores
      obj.route.params.lote_control = isEnabled,
      obj.route.params.vencimiento_control = isEnabled1,
      obj.route.params.reposicion_control = isEnabled2,
      obj.route.params.reposicion_cantidad = Number(reg.cant),
      obj.route.params.reposicion_alerta = isEnabled3,
      obj.route.params.reposicion_alerta_email = reg.mail,
   

    

    // validaciones 
//     if (!obj.name ) {
//       changeModalVisible3(true)
//       return
//   }
//   if (!obj.lastName) {
//       // alert("Por favor escribe el Apellido correctamente!")
//       changeModalVisible4(true)
//       return
//   }
//   if (!obj.eMail.includes('.com') || !obj.eMail.includes('@')  ) {
//     changeModalVisible5(true)
//     // alert("Por favor escribe un correo electrónico válido!")
//     return
// } if (!obj.password) {
//   changeModalVisible6(true)
//   // alert("Por favor escribe una Contraseña válida!")
//   return
// }
// if (!obj.phone) {
//   changeModalVisible7(true)
//       // alert("Por favor escribe un número de telefono válido!")
//       return
//   }
//   if (!obj.business) {
//     changeModalVisible8(true)
//     // alert("Debes aceptar los términos para poder registrarte!")
//     return
//   }if (!obj.secret) {
//     changeModalVisible9(true)
//   // alert("Por favor elije un Rol!")
//   return
// }



dispatch(newInsumo(obj)); // ejecuto la aaccion del post
//  console.log("Estoy enviado", obj);
    setReg({
      mail: "",
      cant: "",

    });
navigation.navigate("Insumos")
//     console.log(obj);
};

  //funciones para cambiar e.value de los inputs

  const handelChangeMail = (name) => {
    setReg({
      ...reg,
      mail: name,
    });
  };
  const handelChangeCant = (name) => {
    setReg({
      ...reg,
      cant: name,
    });
  };


  

  return (
    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : null}
    keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
    <View style={{ flex: 1,  backgroundColor: 'white'  }}>
      {/* <View style={{marginTop:hp("-2%"),marginLeft:wp("0%"),marginBottom:hp("0%")}}>
        <HeaderBar  screen={'null'} style={{color:"white"}}/>
        </View> */}
    <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffffff" }}
      showsVerticalScrollIndicator={false}
    >
      
      {/* Brand View */}
      {/* <ImageBackground
        source={require("../Home/logo.png")}
        resizeMode= "contain"
        style={{
            display:'flex',
            marginTop:  hp('-18%'),
          height: hp('60%') ,
          width: wp('110%') ,
          alignSelf: "center",
        }}
      >
      </ImageBackground> */}
      {/* Botton View */}
      <View style={styles.bottonView}>
        {/* Welcome View */}
        <View style={{ padding: 40, display: "flex", alignItems: "center" }}>
          <Text style={{ color: "#151f27", fontSize: hp("4.7%"),fontWeight: '600', marginTop: hp("3%") }}>
            Controles de tu Insumo
          </Text>
        
        </View>
        {/* inputs */}
        <View style={styles.FormView}>
     
        <View style={styles.switch}>
        <Text style={{marginTop:hp("1.5%"), marginRight:wp("40%"), fontSize:hp("3%"), fontWeight:"bold"}}>
            Control por Lote
          </Text>
    
      <Switch
      style={{ transform: [{ scaleX: hp("0.2%") }, { scaleY: hp("0.2%") }] }}
        trackColor={{ false: "lightgrey", true: "lightgreen" }}
        thumbColor={isEnabled ? "green" : "grey"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isEnabled}
      />
      
    </View>
    <View style={styles.switch}>
        <Text style={{marginTop:hp("1.5%"), marginRight:wp("25%"), fontSize:hp("3%"), fontWeight:"bold"}}>
            Control de Vencimiento
          </Text>
    
      <Switch
      style={{ transform: [{ scaleX: hp("0.2%") }, { scaleY: hp("0.2%") }] }}
        trackColor={{ false: "lightgrey", true: "lightgreen" }}
        thumbColor={isEnabled1 ? "green" : "grey"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch1}
        value={isEnabled1}
      />
      
    </View>
    <View style={styles.switch}>
        <Text style={{marginTop:hp("1.5%"), marginRight:wp("28%"), fontSize:hp("3%"), fontWeight:"bold"}}>
            Control de Reposición
          </Text>
    
      <Switch
      style={{ transform: [{ scaleX: hp("0.2%") }, { scaleY: hp("0.2%") }] }}
        trackColor={{ false: "lightgrey", true: "lightgreen" }}
        thumbColor={isEnabled2 ? "green" : "grey"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch2}
        value={isEnabled2}
      />
      
    </View>
    { isEnabled2 === true
?
          <TextInput
          autoComplete="birthdate-full"
            icon="mail"
            value={reg.cant}
            onChangeText={(name) => handelChangeCant(name)}
            name="cantidad"
            placeholder="Punto de reposición"
            style={styles.TextInput2}
          ></TextInput>
          : null
}

<View style={styles.switch}>
        <Text style={{marginTop:hp("1.5%"), marginRight:wp("37%"), fontSize:hp("3%"), fontWeight:"bold"}}>
            Reposición Alerta
          </Text>
    
      <Switch
      style={{ transform: [{ scaleX: hp("0.2%") }, { scaleY: hp("0.2%") }] }}
        trackColor={{ false: "lightgrey", true: "lightgreen" }}
        thumbColor={isEnabled3 ? "green" : "grey"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch3}
        value={isEnabled3}
      />
      
    </View>

{ isEnabled3 === true
?
          <TextInput
          autoComplete="birthdate-full"
            icon="mail"
            value={reg.mail}
            onChangeText={(name) => handelChangeMail(name)}
            name="mail"
            placeholder="Dirección de Correo Electrónico"
            style={styles.TextInput2}
            autoCapitalize='characters'
          ></TextInput>
          : null
}


          <TouchableOpacity style={styles.Button} onPress={handleSubmit } >
            <Text style={styles.ButtonText} >
              Crear!
            </Text>
            {/* <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible1}
                  nRequestClose={() => changeModalVisible1(false)}
                >
                  <SimpleModal1
                    changeModalVisible1={changeModalVisible1}
                    setData1={setData1}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible2}
                  nRequestClose={() => changeModalVisible2(false)}
                >
                  <SimpleModal2
                    changeModalVisible2={changeModalVisible2}
                    setData2={setData2}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible3}
                  nRequestClose={() => changeModalVisible3(false)}
                >
                  <SimpleModal3
                    changeModalVisible3={changeModalVisible3}
                    setData3={setData3}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible4}
                  nRequestClose={() => changeModalVisible4(false)}
                >
                  <SimpleModal4
                    changeModalVisible4={changeModalVisible4}
                    setData4={setData4}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible5}
                  nRequestClose={() => changeModalVisible5(false)}
                >
                  <SimpleModal5
                    changeModalVisible5={changeModalVisible5}
                    setData5={setData5}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible6}
                  nRequestClose={() => changeModalVisible6(false)}
                >
                  <SimpleModal6
                    changeModalVisible6={changeModalVisible6}
                    setData6={setData6}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible7}
                  nRequestClose={() => changeModalVisible7(false)}
                >
                  <SimpleModal7
                    changeModalVisible7={changeModalVisible7}
                    setData7={setData7}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible8}
                  nRequestClose={() => changeModalVisible8(false)}
                >
                  <SimpleModal8
                    changeModalVisible8={changeModalVisible8}
                    setData8={setData8}
                  />                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible9}
                  nRequestClose={() => changeModalVisible9(false)}
                >
                  <SimpleModal9
                    changeModalVisible9={changeModalVisible9}
                    setData9={setData9}
                  />
                  
                  </Modal>
                  <Modal
                  transparent={true}
                  animationType="fade"
                  visible={isModalVisible40}
                  nRequestClose={() => changeModalVisible40(false)}
                >
                  <SimpleModal40
                    changeModalVisible40={changeModalVisible40}
                    setData40={setData40}
                  />
                  
                  </Modal> */}
          </TouchableOpacity>
          
         
        </View>

        
      </View>
    </ScrollView>
    </View>
    </KeyboardAvoidingView>
  );
};

export default AddInsumo2;

const styles = StyleSheet.create({
  brandView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  brandViewText: {
    color: "#FFC107",
    fontSize: 45,
    fontWeight: "bold",
    textTransform: "uppercase",
    // justifyContent:'flex-start'
  },
  bottonView: {
    flex: 1.5,
    backgroundColor: "white",
    bottom: 50,
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
  },
  FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: -20,
  },
  TextInput: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("2.9%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  TextInput2: {
    width: "90%",
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("1.5%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  Button: {
    width: "90%",
    color: "black",
    height: 52,
    backgroundColor: "rgb(24,116,28)",
    borderRadius: 10,
    marginTop: hp("5%"),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  ButtonText: {
    // fontWeight: "bold",
    fontSize: hp("3%"),
    color: "white",
  },
  SingUpText: {
    color: "#4632a1",
    fontSize: 20,
  },
  TextButton: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  preg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  pregunta: {
    color: "red",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    padding: 50,
    marginTop: 20,
    borderRadius: 20,
  },
  text: {
    // marginVertical: 20,
    fontSize: 22,
    color: "white",
    fontWeight: "bold",
  },
  TouchableOpacity: {
    backgroundColor: "black",
    alignSelf: "stretch",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    
  },
  checkbox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  checkboxx: {
    marginTop: 15,
  },
  imgPerfil: {
    width: hp('25%'),
    height: hp('25%'),
    borderRadius: hp("30%"),
    borderColor: "rgb(0,140,207)",
    borderWidth: wp('0.8%'),
    marginTop: hp('3%'),
    backgroundColor:"#e1e1e1"
  },
  imgAdd: {
    width: wp("15%"),
    height: wp("15%"),
    marginLeft: wp("35%"),
    marginTop: hp('-13%'),
    borderWidth: hp('0.5%'),
    borderColor: "rgb(189,191,198)",
    borderRadius: 50,
  },
  switch:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop:hp("1%")

}
});