import { useNavigation } from "@react-navigation/core";
import React, { useState, useEffect } from "react";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as ImagePicker from "expo-image-picker";
import { Ionicons } from "@expo/vector-icons";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  Modal,
  Button,
  KeyboardAvoidingView
} from "react-native";
import DatePicker from 'react-native-datepicker'
import { LogBox } from 'react-native';
import CountryPicker from 'react-native-country-picker-modal'
// import HeaderBar from "../Utils/HeaderBar";
// import SimpleModal1 from "./../Alerts/SingUp/SimpleModalok";
// import SimpleModal2 from "../Alerts/SingUp/SimpleModalok2.js";
// import SimpleModal3 from "../Alerts/SingUp//SimpleModalname.js";
// import SimpleModal4 from "../Alerts/SingUp//SimpleModalLastname.js";
// import SimpleModal5 from "../Alerts/SingUp//SimpleModalmail.js";
// import SimpleModal6 from "../Alerts/SingUp//SimpleModalpass.js";
// import SimpleModal7 from "../Alerts/SingUp//SimpleModalphone.js";
// import SimpleModal8 from "../Alerts/SingUp//SimpleModalterms.js";
// import SimpleModal9 from "../Alerts/SingUp//SimpleModalrole.js";
// import SimpleModal40 from "../Alerts/SingUp//SimpleModalsamemail.js"; 

import { newAlmacen } from "../../Redux/actions";
import CheckBox from "expo-checkbox";

// import CheckBox from "@react-native-community/checkbox";
// import { ModalPicker } from "./ModalPicker";
import { useDispatch, useSelector } from "react-redux";
// import { adminregister } from "../../Redux/actions/index";
import Select2 from "react-native-select-two"
const AddAlmacen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  //me traigo los tipos de almacenes y los establecimientos
  const almacenes = useSelector((store) => store.tipalm);
  const establecimientos = useSelector((store) => store.establecimiento);

  useEffect(() => { //esto es para que no tire errores
    LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
}, [])

console.log("aaa",almacenes,establecimientos)
//mapeo la información para, cambiar la propiedad, por la que necesito en el select.
var establecimientoss = establecimientos?.map( item => { 
  return { name: item.nombre, id:item.id }; 
});

var almaceness = almacenes?.map( item => { 
  return { name: item.detalle_tipo_almacen, id:item.id }; 
});

  // ALERTAS:

  // REGISTRO USUARIO OK
//   const [isModalVisible1, setisModalVisible1] = useState(false);
//   const [chooseData1, setchooseData1] = useState();

//   const changeModalVisible1 = (bool) => {
//     setisModalVisible1(bool);
//   };

//   const setData1 = (data) => {
//     setchooseData1(data);
//   };

//   // REGISTRO TRANSPORTISTA OK
//   const [isModalVisible2, setisModalVisible2] = useState(false);
//   const [chooseData2, setchooseData2] = useState();

//   const changeModalVisible2 = (bool) => {
//     setisModalVisible2(bool);
//   };

//   const setData2 = (data) => {
//     setchooseData2(data);
//   };


//   // NOMBRE MAL INGRESADO
//   const [isModalVisible3, setisModalVisible3] = useState(false);
//   const [chooseData3, setchooseData3] = useState();

//   const changeModalVisible3 = (bool) => {
//     setisModalVisible3(bool);
//   };

//   const setData3 = (data) => {
//     setchooseData3(data);
//   };

//   // APELLIDO MAL INGRESADO

//   const [isModalVisible4, setisModalVisible4] = useState(false);
//   const [chooseData4, setchooseData4] = useState();

//   const changeModalVisible4 = (bool) => {
//     setisModalVisible4(bool);
//   };

//   const setData4 = (data) => {
//     setchooseData4(data);
//   };

//   //MAIL MAL INGRESADO
//   const [isModalVisible5, setisModalVisible5] = useState(false);
//   const [chooseData5, setchooseData5] = useState();

//   const changeModalVisible5 = (bool) => {
//     setisModalVisible5(bool);
//   };

//   const setData5 = (data) => {
//     setchooseData5(data);
//   };

//   // CONTRASEÑA MAL INGRESADA

//   const [isModalVisible6, setisModalVisible6] = useState(false);
//   const [chooseData6, setchooseData6] = useState();

//   const changeModalVisible6 = (bool) => {
//     setisModalVisible6(bool);
//   };

//   const setData6 = (data) => {
//     setchooseData6(data);
//   };
//   // TELEFONO MAL INGRESADO

//   const [isModalVisible7, setisModalVisible7] = useState(false);
//   const [chooseData7, setchooseData7] = useState();

//   const changeModalVisible7 = (bool) => {
//     setisModalVisible7(bool);
//   };

//   const setData7 = (data) => {
//     setchooseData7(data);
//   };
//   // NO ACEPTA TERMINOS
//   const [isModalVisible8, setisModalVisible8] = useState(false);
//   const [chooseData8, setchooseData8] = useState();

//   const changeModalVisible8 = (bool) => {
//     setisModalVisible8(bool);
//   };

//   const setData8 = (data) => {
//     setchooseData8(data);
//   };

//   // NO ELIJE ROL
//   const [isModalVisible9, setisModalVisible9] = useState(false);
//   const [chooseData9, setchooseData9] = useState();

//   const changeModalVisible9 = (bool) => {
//     setisModalVisible9(bool);
//   };

//   const setData9 = (data) => {
//     setchooseData9(data);
//   };

//   // MAIL REPETIDO
//   const [isModalVisible40, setisModalVisible40] = useState(false);
//   const [chooseData40, setchooseData40] = useState();

//   const changeModalVisible40 = (bool) => {
//     setisModalVisible40(bool);
//   };

//   const setData40 = (data) => {
//     setchooseData40(data);
//   };

  


  // useEffect(()=>{
  //   //console.log('aqui esta la respuestaaaa:',respuesta);
  //   if(respuesta?.role === true){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileUser");
  //     changeModalVisible1(true)
  //   }if(respuesta?.role === false){
  //     // alert('Te has registrado exitosamente!')
  //     // navigation.navigate("CompleteProfileCarrier");
  //     changeModalVisible2(true)
  //   }if(respuesta?.role === 1){
  //     // alert('El mail ingresado ya se encuentra en uso!')
  //     changeModalVisible40(true)
  //   }
  // },[respuesta]);



//genero un estado local, con los datos que luego voy a enviar al back
  const [reg, setReg] = useState({
    nombre: "",
    abre: "",
    desc: "",
    est: "",
    alm: "",
    obs: "",
  
  });
//estado local, para los placeholder
  const [place, setPlace] = useState({
    est: "Establecimiento",
    alm: "Tipo de Almacén"
  });


  // const ChangeInput = (e) => {
  //   setReg({
  //     // y sino es  generos y platforms, directamente pongo lo que escribo en el input
  //     ...reg,
  //     [e.target.name]: e.target.value,
  //   });
  // };

  //funcion para ejecutar el envio al back,con LOCACION

  const handleSubmit = (e) => {
    e.preventDefault();
    // en un objeto pongo lo que tengo en el estado inicial
    // let rolex = undefined;
    // if (chooseData === "◉ Usuario") {
    //   rolex = true;
    // } if(chooseData === "◉ Transportista") {
    //   rolex = false;
    // }

    //objeto con la información a enviar
    const obj = {
        nombre: reg.nombre,
        abreviatura: reg.abre,
        descripcion: reg.desc,
        establecimiento_id: reg.est,
        almacenes_tipo_id:reg.alm,
        observaciones: reg.obs
    };

    // validaciones 
//     if (!obj.name ) {
//       changeModalVisible3(true)
//       return
//   }
//   if (!obj.lastName) {
//       // alert("Por favor escribe el Apellido correctamente!")
//       changeModalVisible4(true)
//       return
//   }
//   if (!obj.eMail.includes('.com') || !obj.eMail.includes('@')  ) {
//     changeModalVisible5(true)
//     // alert("Por favor escribe un correo electrónico válido!")
//     return
// } if (!obj.password) {
//   changeModalVisible6(true)
//   // alert("Por favor escribe una Contraseña válida!")
//   return
// }
// if (!obj.phone) {
//   changeModalVisible7(true)
//       // alert("Por favor escribe un número de telefono válido!")
//       return
//   }
//   if (!obj.business) {
//     changeModalVisible8(true)
//     // alert("Debes aceptar los términos para poder registrarte!")
//     return
//   }if (!obj.secret) {
//     changeModalVisible9(true)
//   // alert("Por favor elije un Rol!")
//   return
// }



// dispatch(adminregister(obj));
 console.log("Estoy enviado", obj);
//navego
navigation.navigate("AlmMap",obj)
//vuelvo los estados locales a vacios
setReg({
  nombre: "",
  abre: "",
  desc: "",
  est: "",
  alm: "",
  obs: "",
});
//limpio los placeholders
setPlace({
  est: "Establecimiento",
  alm: "Tipo de Almacén"
});

};


  //funcion para ejecutar el envio al back,sin LOCACION

const handleSubmit2 = (e) => {
  e.preventDefault();
  // en un objeto pongo lo que tengo en el estado inicial
  // let rolex = undefined;
  // if (chooseData === "◉ Usuario") {
  //   rolex = true;
  // } if(chooseData === "◉ Transportista") {
  //   rolex = false;
  // }
  const obj = {
      nombre: reg.nombre,
      abreviatura: reg.abre,
      descripcion: reg.desc,
      establecimiento_id: reg.est,
      almacenes_tipo_id:reg.alm,
      observaciones: reg.obs
  };

  // validaciones 
//     if (!obj.name ) {
//       changeModalVisible3(true)
//       return
//   }
//   if (!obj.lastName) {
//       // alert("Por favor escribe el Apellido correctamente!")
//       changeModalVisible4(true)
//       return
//   }
//   if (!obj.eMail.includes('.com') || !obj.eMail.includes('@')  ) {
//     changeModalVisible5(true)
//     // alert("Por favor escribe un correo electrónico válido!")
//     return
// } if (!obj.password) {
//   changeModalVisible6(true)
//   // alert("Por favor escribe una Contraseña válida!")
//   return
// }
// if (!obj.phone) {
//   changeModalVisible7(true)
//       // alert("Por favor escribe un número de telefono válido!")
//       return
//   }
//   if (!obj.business) {
//     changeModalVisible8(true)
//     // alert("Debes aceptar los términos para poder registrarte!")
//     return
//   }if (!obj.secret) {
//     changeModalVisible9(true)
//   // alert("Por favor elije un Rol!")
//   return
// }


console.log("Estoy enviado", obj);
dispatch(newAlmacen(obj));


navigation.navigate("Almacenes")
setReg({
nombre: "",
abre: "",
desc: "",
est: "",
alm: "",
obs: "",
});

setPlace({
est: "Establecimiento",
alm: "Tipo de Almacén"
});

};


  //funciones para cambiar e.value de los inputs

  const handelChangeNombre = (name) => {
    setReg({
      ...reg,
      nombre: name,
    });
  };
  const handelChangeAbre = (name) => {
    setReg({
      ...reg,
      abre: name,
    });
  };
  const handelChangeDesc = (name) => {
    setReg({
      ...reg,
      desc: name,
    });
  };

  const handelChangeEst = (name) => {
    setReg({
      ...reg,
      est: name,
    });

    const filtrado6 = establecimientos?.filter((f) => f.id === name)
    setPlace({
      ...place,
     est: filtrado6[0].nombre,
    });
  };

  const handelChangeAlm = (name) => {
    setReg({
      ...reg,
      alm: name,
    });

    const filtrado6 = almacenes?.filter((f) => f.id === name)
    setPlace({
      ...place,
     alm: filtrado6[0].detalle_tipo_almacen,
    });
  };

  const handelChangeObs = (name) => {
    setReg({
      ...reg,
      obs: name,
    });
  };



  return (
    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : null}
    keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
    <View style={{ flex: 1,  backgroundColor: 'white'  }}>
      {/* <View style={{marginTop:hp("-2%"),marginLeft:wp("0%"),marginBottom:hp("0%")}}>
        <HeaderBar  screen={'null'} style={{color:"white"}}/>
        </View> */}
    <ScrollView
      style={{ flex: 1, backgroundColor: "#ffffffff" }}
      showsVerticalScrollIndicator={false}
    >
      
      {/* Brand View */}
      {/* <ImageBackground
        source={require("../Home/logo.png")}
        resizeMode= "contain"
        style={{
            display:'flex',
            marginTop:  hp('-18%'),
          height: hp('60%') ,
          width: wp('110%') ,
          alignSelf: "center",
        }}
      >
      </ImageBackground> */}
      {/* Botton View */}
      <View style={styles.bottonView}>
        {/* Welcome View */}
        <View style={{ padding: 40, display: "flex", alignItems: "center" }}>
          <Text style={{ color: "#151f27", fontSize: hp("4.7%"),fontWeight: '600', marginTop: hp("1%") }}>
            Datos de tu almacén
          </Text>
        </View>
        {/* inputs */}
        <View style={styles.FormView}>
          <TextInput
            name="nombre"
            value={reg.nombre}
            onChangeText={(name) => handelChangeNombre(name)}
            placeholder="Nombre*"
            style={styles.TextInput}
          ></TextInput>
    
  
          <TextInput
            value={reg.abre}
            onChangeText={(name) => handelChangeAbre(name)}
            name="abreviatura"
            placeholder="Abreviatura*"
            style={styles.TextInput}
            
          ></TextInput>
          
          <TextInput
          value={reg.desc}
          onChangeText={(name) => handelChangeDesc(name)}          
          name="descripcion"
          placeholder="Descripción"
          style={styles.TextInput}
        ></TextInput>
        <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Establecimiento"
          title={place.est}
          data={establecimientoss}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeEst(name[0])}
        />
        <Select2
          isSelectSingle
          style={styles.TextInput}
          colorTheme="green"
          popupTitle="Tipo de Almacén"
          title={place.alm}
          data={almaceness}
          selectButtonText="Aceptar"
          cancelButtonText="Rechazar"
          searchPlaceHolderText="Buscar..."
          onSelect={(name) => handelChangeAlm(name[0])}
        />
        
        <TextInput
          value={reg.obs}
          onChangeText={(name) => handelChangeObs(name)}          
          name="observaciones"
          placeholder="Observaciones"
          style={styles.TextInput}
        ></TextInput>
        
          
        <View style={{flexDirection:"row", marginLeft:-20, marginTop:hp("-2%")}}>
          <TouchableOpacity style={styles.Button} onPress={handleSubmit } >
            <Text style={styles.ButtonText} >
              Agregar Locación
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.Button} onPress={handleSubmit2 } >
            <Text style={styles.ButtonText} >
              + Agregar
            </Text>
          </TouchableOpacity>
          
         
        </View>
          
         
        </View>

        
      </View>
    </ScrollView>
    </View>
    </KeyboardAvoidingView>
  );
};

export default AddAlmacen;

const styles = StyleSheet.create({
  brandView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  brandViewText: {
    color: "#FFC107",
    fontSize: 45,
    fontWeight: "bold",
    textTransform: "uppercase",
    // justifyContent:'flex-start'
  },
  bottonView: {
    flex: 1.5,
    backgroundColor: "white",
    bottom: 50,
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
  },
  FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: -20,
  },
  TextInput: {
    width: wp("90%"),
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("2.9%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  TextInput2: {
    width: wp("90%"),
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("5%"),
    color: "black",
    backgroundColor:"#eceef2",
    fontSize:hp("2.5%"),
  },
  Button: {
    width: "35%",
    color: "black",
    height: 52,
    backgroundColor: "rgb(24,116,28)",
    borderRadius: 10,
    marginTop: hp("5%"),
    marginLeft:20,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  ButtonText: {
    // fontWeight: "bold",
    fontSize: hp("3%"),
    color: "white",
  },
  SingUpText: {
    color: "#4632a1",
    fontSize: 20,
  },
  TextButton: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginTop: 10,
  },
  preg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  pregunta: {
    color: "red",
  },
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    padding: 50,
    marginTop: 20,
    borderRadius: 20,
  },
  text: {
    // marginVertical: 20,
    fontSize: 22,
    color: "white",
    fontWeight: "bold",
  },
  TouchableOpacity: {
    backgroundColor: "black",
    alignSelf: "stretch",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    
  },
  checkbox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  checkboxx: {
    marginTop: 15,
  },
  imgPerfil: {
    width: hp('25%'),
    height: hp('25%'),
    borderRadius: hp("12,5%"),
    borderColor: "rgb(0,140,207)",
    borderWidth: wp('0.8%'),
    marginTop: hp('3%'),
    backgroundColor:"#e1e1e1",
  },
  imgAdd: {
    width: wp("15%"),
    height: wp("15%"),
    marginLeft: wp("35%"),
    marginTop: hp('-13%'),
    borderWidth: hp('0.5%'),
    borderColor: "rgb(189,191,198)",
    borderRadius: 50,
  },
});