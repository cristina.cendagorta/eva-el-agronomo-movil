import React, { useState, useEffect, useRef, useMemo } from "react";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useNavigation } from "@react-navigation/core";
import {
  Text,
  ScrollView,
  ImageBackground,
  Dimensions,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Button,
  Modal,
  Alert,
  BackHandler,
  SafeAreaView,
  KeyboardAvoidingView,
  ActivityIndicator,
  Image,
  FlatList
} from "react-native";
import { logiar } from "../../Redux/actions/index";
import { useDispatch, useSelector } from "react-redux";
import { MaterialCommunityIcons } from "@expo/vector-icons"
import SimpleModal5 from './../Alerts/Login/SimpleModalmail';
import SimpleModal6 from './../Alerts/Login/SimpleModalpass';
import SimpleModal30 from './../Alerts/Login/SimpleModallog';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as SecureStore from "expo-secure-store";
import { useIsFocused } from "@react-navigation/native";

//ESTE ES EL LOGIN, 

const Login = () => {
  //ESTADO LOCAL INICIAL, DE LO QUE VOY A ENVIAR AL BACK
  const [log, setLog] = useState({
    mail: "",
    contraseña: "",
  });
  var auth = "false";
  const [isLoading, setLoading] = useState(false); // ESTADO PARA EL LOADING
  const [show, setShow] = useState(false);
  const [visible, setVisible] = useState(true);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const login = useSelector((store) => store.responseLog);
  const lastNameRef = useRef();
  const isFocused = useIsFocused();

  //FUNCION PARA GUARDAR LA INFO EN EL STORE, KEY = token , VALUE=el string del token
  async function save(key, value) {
    //FUNCION PARA GUARDAR LA INFO EN EL STORE, KEY = token , VALUE=el string del token
    try {
      await SecureStore.setItemAsync(key, value);
    } catch (error) {
      console.log('error', error.response)
    }
  }
  // SE CONSULTA EL VALUE DEL STORE, CON EL KEY
  async function getValueFor() {
    let result = await SecureStore.getItemAsync("tokenn");
    if (result) {
      // console.log("se activa el guardartoken?", result)
      auth = result;
      //  console.log("auth",result);
    } else {
      //   alert('Invalid Key')
    }
  }


  const disabledSummit = useMemo(() => {
    if (
      log.contraseña < 0 && log.mail < 0
    ) {
      return true;
    } else {
      return false;
    }
  }, [log])

  // ESTO SE EJECUTA AL PRINCIPIO

  useEffect(() => {
    if (isFocused) {

      setLoading(true) // SE PONE EL CARGANDO
      getValueFor();     // SE CONSULTA EN EL ALMACENAMIENTO DEL CELU, SI ALGUNA VEZ SE GUARDO ALGO, SI SE GUARDO ALGO, SE PONE EN AUTH

      setTimeout(function () {
        console.log("auth2", auth);
        if (auth != "true") { // SI LO QUE SE TRAE DEL ALM DEL CEL, NO ES TRUE, SE SACA EL CARGANDO
          setLoading(false)
        }
        if (auth === "true") { // SI LO QUE SE TRAE ES UN TRUE, SIGNIFICA QUE YA SE HABIA LOGEADO ANTES, ENTONCES LO DEJAMOS ENTRAR A LA APLICACION
          navigation.navigate("InitialSync")
        }
      }, 4500);
    }

  }, [, isFocused]);

  useEffect(() => {
    if (login?.status != null && login?.status === 500) { // ESTA ES LA RESPUESTA DESDE EL BACK, MOSTRAMOS UN MODAL
      changeModalVisible30(true)
    }
    // if(login?.HTTPHeaders.HTTPStatusCode != null){

    //   setLoading(false)
    //   navigation.navigate("HomeStack")
    // console.log("se activa el?", login?.ResponseMetadata)
    // }
    if (login?.ResponseMetadata != null) {
      save("tokenn", "true");
      navigation.navigate("InitialSync")
      //  setLoading(false)
    }

  }, [login]);



  const navegar = () => { // ME MUEVO AL REGISTRO
    navigation.navigate("SingUp")
  }

  const handelChangeMail = (email) => {
    setLog({
      ...log,
      mail: email,
    });
  };
  const handelChangePass = (pass) => {
    setLog({
      ...log,
      contraseña: pass,
    });
  };

  // MODALES

  // COMBINACION MAIL Y PASS MAL
  const [isModalVisible30, setisModalVisible30] = useState(false);
  const [chooseData30, setchooseData30] = useState();

  const changeModalVisible30 = (bool) => {
    setisModalVisible30(bool);
  };

  const setData30 = (data) => {
    setchooseData30(data);
  };

  //MAIL MAL INGRESADO
  const [isModalVisible5, setisModalVisible5] = useState(false);
  const [chooseData5, setchooseData5] = useState();

  const changeModalVisible5 = (bool) => {
    setisModalVisible5(bool);
  };

  const setData5 = (data) => {
    setchooseData5(data);
  };

  // CONTRASEÑA MAL INGRESADA

  const [isModalVisible6, setisModalVisible6] = useState(false);
  const [chooseData6, setchooseData6] = useState();

  const changeModalVisible6 = (bool) => {
    setisModalVisible6(bool);
  };
  const setData6 = (data) => {
    setchooseData6(data);
  };

  const handleSubmit = (e) => {
    //     e.preventDefault();
    //     // en un objeto pongo lo que tengo en el estado inicial
    const obj = {
      eMail: log.mail.trim(),
      password: log.contraseña,
    };

    //Validaciones:

    if (!obj.eMail.includes("@")) {
      changeModalVisible5(true)
      return;
    }
    if (!obj.password) {
      changeModalVisible6(true)
      return;
    }

    dispatch(logiar(obj)); // EJECUTAMOS ACCION HACIA EL BACK, CON LOS DATOS INGRESADOS
    console.log("Estoy enviado", obj);
    setLog({
      mail: "",
      contraseña: "",
    });

    //     //cuando se cumpla que respuesta != null
    //     //haga un console.log(respuesta)
    setLoading(true)
    setTimeout(function () {
      setLoading(false)
    }, 5000);

  };


  if (isLoading === true) { // CARGANDO...
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator size="large" color="#007d3c" />
      </View>
    )

  } else {


    return ( // INPUTS PARA LOGIARSE

      //Container Start
      <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
        <KeyboardAvoidingView style={{ flex: 1, backgroundColor: "#fff" }} behavior={Platform.OS === "ios" ? "padding" : "height"}>
          <ScrollView
            style={{ flex: 1, backgroundColor: "#ffffffff" }}
            showsVerticalScrollIndicator={false}
          >
            {/* Brand View */}
            <ImageBackground
              source={require("./logo.png")}
              resizeMode="contain"
              style={{
                display: 'flex',
                marginTop: hp('-9%'),
                marginBottom: hp('-7%'),
                height: hp('60%'),
                width: wp('50%'),
                alignSelf: "center",
              }}
            >
            </ImageBackground>

            {/* Botton View */}
            <View style={styles.bottonView}>
              {/* Welcome View */}
              <View style={{ padding: hp("3%"), display: "flex", alignItems: "center", }}>
                <Text style={{ color: "#151f27", fontSize: hp("4.8%"), fontWeight: '600', }}>Bienvenido</Text>
              </View>
              {/* inputs */}
              <View
                style={styles.FormView}
              // onChange={(e) => ChangeInput(e)}
              //   onSubmit={(e) => handleSubmit(e)}
              >
                <TextInput
                  value={log.mail}
                  onChangeText={(name) => handelChangeMail(name)}
                  name="mail"
                  placeholder="Dirección de Mail*"
                  style={styles.TextInput}
                  autoCapitalize='none'
                  // showSoftInputOnFocus={false}            
                  autoFocus={true}

                  returnKeyType="next"
                  onSubmitEditing={() => {
                    lastNameRef.current.focus();
                  }}
                  blurOnSubmit={false}

                ></TextInput>

                <TextInput
                  value={log.contraseña}
                  onChangeText={(name) => handelChangePass(name)}
                  name="contraseña"
                  placeholder="Contraseña*"
                  secureTextEntry={visible}
                  style={styles.TextInput}
                  autoCapitalize='none'
                  ref={lastNameRef} onSubmitEditing={() => {
                    return console.log('done')
                  }}
                ></TextInput>
                <TouchableOpacity style={styles.btnEye} onPress={
                  () => {
                    setVisible(!visible)
                    setShow(!show)
                  }
                }>
                  <Ionicons
                    name={show === false ? 'eye-off-outline' : 'eye-outline'}
                    size={hp("3.7%")}
                    color={show === false ? "#344c40" : "#007d3c"}
                  />
                </TouchableOpacity>

                <View style={styles.preg}>
                  <Text style={styles.pregunta} onPress={() => navigation.navigate("ForgotPass")}>Olvidé la contraseña </Text>
                </View>
                <TouchableOpacity style={styles.Button} disabled={disabledSummit} onPress={handleSubmit}>
                  <Text style={styles.ButtonText} >
                    Ingresar al sistema
                  </Text>
                  <Modal
                    transparent={true}
                    animationType="fade"
                    visible={isModalVisible5}
                    nRequestClose={() => changeModalVisible5(false)}
                  >
                    <SimpleModal5
                      changeModalVisible5={changeModalVisible5}
                      setData5={setData5}
                    />
                  </Modal>
                  <Modal
                    transparent={true}
                    animationType="fade"
                    visible={isModalVisible6}
                    nRequestClose={() => changeModalVisible6(false)}
                  >
                    <SimpleModal6
                      changeModalVisible6={changeModalVisible6}
                      setData6={setData6}
                    />
                  </Modal>
                  <Modal
                    transparent={true}
                    animationType="fade"
                    visible={isModalVisible30}
                    nRequestClose={() => changeModalVisible30(false)}
                  >
                    <SimpleModal30
                      changeModalVisible30={changeModalVisible30}
                      setData30={setData30}
                    />

                  </Modal>

                </TouchableOpacity>
              </View>


              <TouchableOpacity style={styles.TextButton}
                onPress={navegar}
              >
                <Text style={styles.SingUpText}>Crear Usuario</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
      // Container End
    );
  }
};

export default Login;

const styles = StyleSheet.create({
  // brandView: {
  //   flex: 1,
  //   justifyContent: "center",
  //   alignItems: "center",
  // },
  // brandViewText: {
  //   color: "#FFC107",
  //   fontSize: 45,
  //   fontWeight: "bold",
  //   textTransform: "uppercase",
  //   // justifyContent:'flex-start'
  // },
  bottonView: {
    flex: 1,
    backgroundColor: "#ffffffff",
    bottom: hp("8%"),
    borderTopStartRadius: 50,
    borderTopEndRadius: 50,
  },
  FormView: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: hp("-2%"),
  },
  TextInput: {
    backgroundColor: "#eceef2",
    width: "90%",
    borderWidth: 1,
    borderColor: "#eceef2",
    height: hp("7.7%"),
    borderRadius: 10,
    paddingLeft: hp("2.5%"),
    marginTop: hp("2.9%"),
    color: "#161a23",
    fontSize: hp("2.3%"),
  },
  Button: {
    width: "90%",
    color: "#FFC107",
    height: hp("8%"),
    backgroundColor: "#007d3c",
    borderRadius: 10,
    borderColor: "black",
    // borderWidth:1,
    marginTop: hp("5%"),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowOpacity: 80,
    elevation: 15,
  },
  ButtonText: {
    fontWeight: "bold",
    fontSize: hp("3%"),
    color: "white",
  },
  SingUpText: {
    marginTop: hp("2%"),
    color: "#151f27",
    fontSize: hp("2.5%"),
  },
  TextButton: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginTop: hp("1%"),
  },
  preg: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: hp("2%"),
    marginBottom: hp("2%"),
  },
  pregunta: {
    color: "#007d3c",
    fontSize: hp("2.3%"),
    fontWeight: "600"
  },
  btnEye: {
    position: "absolute",
    alignSelf: "flex-end",
    marginTop: hp("15.4%"),
    paddingRight: wp("8%")
  }
});