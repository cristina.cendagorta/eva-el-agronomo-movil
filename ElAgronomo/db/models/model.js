const tablesWithIDField = {
    SyncInfo: {
        t: "real",
        error: "real",
        errMsg: "text"
    },
    // ==== admin ====
    admins: {
        id: "real",
        id_token: "text"
    },
    // ==== almacen ====
    tipo_almacenes: {
        id: "real",
        detalle_tipo_almacen: "text"
    },
    almacenes: {
        abreviatura: "text",
        activo: "boolean",
        descripcion: "text",
        detalle_tipo_almacen: "text",
        geoposicion: "text",
        id: "real",
        nobre: "text",
        observaciones: "text"
    },
    // ==== empresa ====
    cond_ivas: {
        id: "real",
        detalle_cond_iva: "text"
    },
    monedas: {
        id: "real",
        detalle_moneda: "text"
    },
    rubro_empresas: {
        id: "real",
        detalle_rubro_empresa: "text"
    },
    empresas: {
        id: "real",
        activo: "boolean",
        razon_social: "text",
        direccion_calle: "text",
        direccion_nro: "text",
        direccion_localidad: "text",
        direccion_provincia: "text",
        direccion_pais: "text",
        direccion_cod_postal: "text",
        cuit: "real",
        fecha_cierre: "text",
        cond_iva_id: "real",
        moneda_primaria_id: "real",
        moneda_secundaria_id: "real",
        rubro_empresa_id: "real",
        admin_id: "real",
        created_at: "text",
        update_at: "text",
        delete_at: "text",
        syncState: "boolean"
    },
    // ==== establecimientos ====
    zonas: {
        id: "real",
        detalle_zona: "text"
    },
    tipo_establecimientos: {
        id: "real",
        detalle_tipo_establecimiento: "text"
    },
    establecimientos: {
        id: "real",
        activo: "boolean",
        nombre: "text",
        abreviatura: "text",
        direccion: "text",
        localidad: "text",
        provincia: "text",
        pais: "text",
        geoposicion: "text",
        observaciones: "text",
        contacto: "text",
        zona_id: "real",
        empresa_id: "real",
        establecimiento_tipo_id: "real",
        almacenes: "object"
    },
    // ==== facturacion ====
    emisor_tarjetas: {
        id: "real",
        detalle_emisor_tarjeta: "text"
    },
    facturaciones: {
        id: "real",
        activo: "boolean",
        nro_tarjeta: "real",
        vto_fecha: "text",
        cod_verificacion: "real",
        fecha_alta: "text",
        fecha_baja: "text",
        tarjeta_emisor_id: "real"
    },
    // ==== insumos ====
    tareas: {
        id: "real",
        detalle_tarea: "text"
    },
    unidades: {
        id: "real",
        abr: "text",
        español: "text",
        ingles: "text",
        portugues: "text"
    },
    familias: {
        id: "real",
        detalle_familia: "text"
    },
    subfamilias: {
        id: "real",
        detalle_subfamilia: "text"
    },
    rubro_insumos: {
        id: "real",
        detalle_rubro_insumo: "text"
    },
    tipo_erogaciones: {
        id: "real",
        nombre_tipo_erogacion: "text",
        abreviatura_tipo_erogacion: "text"
    },
    tipo_movimiento_insumos: {
        id: "real",
        detalle_tipo_movimiento_insumo: "text"
    },
    encabezado_movimiento: {
        almacen_destino: "real",
        destino_almacen_id: "real",
        detalle_tipo_movimiento_insumo: "text",
        fecha_real: "text",
        fecha_valor: "text",
        id: "real",
        nombre_almacen_origen: "text",
        nro_movimiento: "text",
        orden_de_compra: "text",
        origen_almacen_id: "real",
        tipo_movimiento_id: "real"
    },
    insumos: {
        abr: "text",
        abreviatura: "text",
        abreviatura_tipo_erogacion: "text",
        activo: "boolean",
        codigo_externo: "text",
        detalle_familia: "text",
        detalle_rubro_insumo: "text",
        detalle_subfamilia: "text",
        detalle_tarea: "text",
        id: "real",
        lote_control: "boolean",
        nombre: "text",
        nombre_tipo_erogacion: "text",
        reposicion_alerta: "boolean",
        reposicion_alerta_email: "text",
        reposicion_cantidad: "real",
        reposicion_control: "boolean",
        vencimiento_control: "boolean",
        created_at: "text",
        update_at: "text",
        delete_at: "text",
        syncState: "boolean", //esto no se envia a la base de datos remota
        syncImgSt: "boolean" //esto no se envia a la base de datos remota
    },
    movimiento_detalle: {
        id: "real",
        insumo_id: "real",
        cantidad: "real",
        unidad_id: "real",
        nro_lote: "text",
        fecha_vencimiento: "text",
        precio_unitario: "text",
        observaciones: "text",
        encabezado_movimiento_id: "real", // ******
        precio_total: "real"
    },
    stocks: {
        id: "real",
        cantidad: "real",
        detalle: "text",
        insumo_id: "real",
        almacen_id: "real",
        nro_lote: "text",
        fecha_vencimiento: "text",
        unidad_id: "real",
        precio_unitario: "real",
        precio_total: "real",
        created_at: "text",
        update_at: "text",
        delete_at: "text",
    },
    // ==== lote ====
    lotes:{
        id: "real",
        activo: "boolean",
        codigo: "text",
        poligono: "text",
        superficie: "real",
        superficie_calculada: "real",
        establecimiento_id: "real"
    },
    // ==== tablas relacionales ====
    inquilinos:{
        id: "real",
        nombre: "text",
        empresas: "text",
        usuarios: "text",
        accesos_por_rol: "text"
    },
    establecimiento_almacenes: {
        almacen_id: "real", // ***
        establecimiento_id: "real" // ***
    },
    // ==== usuario ====
    roles: {
        id: "real",
        detalle_rol: "text"
    },
    log_auditoria_usuarios: {
        id: "real",
        fecha: "text",
        criticidad: "text",
        detalle: "text",
        usuario_id: "real"
    },
    usuarios: {
        id: "real",
        nombre: "text",
        apellido: "text",
        dni: "real",
        email: "text",
        activo: "boolean",
        fecha_alta: "text",
        fecha_baja: "text",
        rol_id: "real",
        empresa_id: "real",
    },
    // ==== valuaciones ====
    insumos_valorizacion: {
        id: "real",
        cantidad: "real",
        precio_unitario: "real",
        precio_total: "real",
        almacen_id: "real", // **
        movimiento: "text",
        tipo_movimiento_id: "real",
        insumo_id: "real"
    },
    tipo_metodo_valorizacion: {
        id: "real",
        abreviatura: "text",
        tipo: "text"
    },
    tipo_valorizacion_empresas: {
        id: "real",
        empresa_id: "real",
        metodo_id: "real",
        config: "boolean"
    },
    historicos_precio_segun_criterio: {
        id: "real",
        fecha: "text",
        precio: "real",
        insumo_id: "real"
    }
}

export { tablesWithIDField };